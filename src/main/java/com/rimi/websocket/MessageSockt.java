package com.rimi.websocket;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.gson.Gson;
import com.rimi.bean.CommonDate;
import com.rimi.bean.Message;
import com.rimi.bean.User;
import com.rimi.bean.UserFriend;
import com.rimi.service.impl.MessageServiceImpl;
import com.rimi.service.impl.UserFriendServiceImpl;
import com.rimi.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @ServerEndpoint 可以把当前类变成websocket服务类
 */

@ServerEndpoint("/client/{userno}")
@Component
public class MessageSockt {
    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;
    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。若要实现服务端与单一客户端通信的话，可以使用Map来存放，其中Key可以为用户标识
    private static ConcurrentHashMap<String, MessageSockt> webSocketSet = new ConcurrentHashMap<String, MessageSockt>();
    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session WebSocketsession;
    //当前发消息的人员编号
    private String userno = "";
    //记录消息
    List<Message> messagelist = new ArrayList<Message>();
    //gson
    private Gson gson = new Gson();
    //实现接口
    private static MessageServiceImpl messageServiceImpl;

    private static  UserServiceImpl userServiceImpl;

    private static UserFriendServiceImpl userFriendServiceImpl;

    @Autowired
    public  void setUserFriendServiceImpl(UserFriendServiceImpl userFriendServiceImpl) {
        this.userFriendServiceImpl = userFriendServiceImpl;
    }

    @Autowired
    public void setMessageServiceImpl(MessageServiceImpl messageServiceImpl) {
        this.messageServiceImpl = messageServiceImpl;
    }
    @Autowired
    public void setUserServiceImpl(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    public  MessageServiceImpl getMessageServiceImpl() {
        return messageServiceImpl;
    }

    public  UserServiceImpl getUserServiceImpl() {
        return userServiceImpl;
    }


    /**
     * 连接建立成功调用的方法
     * @param WebSocketsession 可选的参数。session为与某个客户端的连接会话，需要通过它来给客户端发送数据
     */
    @OnOpen
    public void onOpen(@PathParam(value = "userno") String param, Session WebSocketsession, EndpointConfig config) {
        userno = param;//接收到发送消息的人员编号
        this.WebSocketsession = WebSocketsession;
        webSocketSet.put(param, this);//加入map中
        addOnlineCount();           //在线数加1
        System.out.println("有新连接加入！当前在线人数为" + getOnlineCount());
    }


    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        if (!userno.equals("")) {
            webSocketSet.remove(userno);  //从set中删除
            subOnlineCount();           //在线数减1
            System.out.println("有一连接关闭！当前在线人数为" + getOnlineCount());
        }
    }


    /**
     * 给指定的人发送消息
     *
     * @param message
     */
    @OnMessage
    public void sendToUser(String message) {
        JSONObject jsonobject = JSONObject.parseObject(message);
        String msgId = jsonobject.getString("msgId");
        String notry = jsonobject.getString("notry");
        String msgSendTime = jsonobject.getString("msgSendTime");
        String msgContext = jsonobject.getString("msgContext");
        String msgSendId = jsonobject.getString("msgSendId");
        String msgReciveId = jsonobject.getString("msgReciveId");
        Message message1 = null;
        if (notry.equals("消息")){
             message1 = new Message(Integer.parseInt(msgId),notry,msgSendTime,msgContext,Integer.parseInt(msgSendId),Integer.parseInt(msgReciveId));
            messagelist.add(message1);
            if (messagelist.size() >= 3){
                messageServiceImpl.insertBatch(messagelist);
                messagelist.clear();
            }
            try {
                if (msgReciveId != null){
                    webSocketSet.get(msgReciveId).sendMessage(message);
                } else {
                    System.out.println("当前用户不在线");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if (notry.equals("推送")){
            List<UserFriend> userFriends = userFriendServiceImpl.selectList(new EntityWrapper<UserFriend>().eq("user_id",Integer.parseInt(msgSendId)));
            for (UserFriend user1 : userFriends) {
                for (String key : webSocketSet.keySet()) {
                    try {
                        //判断接收用户是否是当前发消息的用户
                        if (!userno.equals(key)&&String .valueOf(user1.getFriendId()).equals(key)) {
                            message1 = new Message(Integer.parseInt(msgId),notry,msgSendTime,msgContext,Integer.parseInt(msgSendId),Integer.parseInt(key));
                            String s = gson.toJson(message1);
                            webSocketSet.get(key).sendMessage(s);
                            System.out.println("key = " + key);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else{
            try {
                if (msgReciveId != null){
                    webSocketSet.get(msgReciveId).sendMessage(message);
                } else {
                    System.out.println("当前用户不在线,无法接收到消息");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    /**
     * 发生错误时调用
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        System.out.println("发生错误");
        error.printStackTrace();
    }


    /**
     * 这个方法与上面几个方法不一样。没有用注解，是根据自己需要添加的方法。
     *
     * @param message
     * @throws IOException
     */
    public void sendMessage(String message) throws IOException {
        this.WebSocketsession.getBasicRemote().sendText(message);
    }


    public static synchronized int getOnlineCount() {
        return onlineCount;
    }


    public static synchronized void addOnlineCount() {
        MessageSockt.onlineCount++;
    }


    public static synchronized void subOnlineCount() {
        MessageSockt.onlineCount--;
    }

}