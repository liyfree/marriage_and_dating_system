package com.rimi.util;

import com.rimi.bean.Email;
import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;

public class MailUtil {

    public static void sendMail(final Email email) throws GeneralSecurityException, MessagingException, UnsupportedEncodingException {

        Properties props = new Properties();
        // 开启debug调试
        //props.setProperty("mail.debug", "true");
        // 发送服务器需要身份验证
        props.setProperty("mail.smtp.auth", "true");
        // 设置邮件服务器主机名
        props.setProperty("mail.host", "smtp.qq.com");
        // 发送邮件协议名称
        props.setProperty("mail.transport.protocol", "smtp");

        // QQ邮箱需要下面这段代码，使用SSL加密
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.ssl.socketFactory", sf);

        // 由发送者发起会话,并创建认证（发送者邮箱的帐号、授权码）
        Session session = Session.getDefaultInstance(props, new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email.getFromAddress(), email.getLicenseCode());
            }
        });

        // 3.根据会话创建邮件消息
        Message message = new MimeMessage(session);
        // 设置发送者地址,昵称，编码
        message.setFrom(new InternetAddress(email.getFromAddress(), "RIMI", "UTF-8"));

        // 设置接收者地址
        message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(email.getToAddress()));
        // 设置邮件主题
        message.setSubject(email.getSubject());

        // 6. 设置显示的发件时间
        //message.setSentDate(new Date());

        //Content: 邮件正文（可以使用html标签）
        String content = email.getText();
        message.setContent(content, "text/html;charset=UTF-8");
        // 发送纯文本格式
        //message.setText(email.getText());

        // 4.发送邮件
        Transport.send(message);

    }


}
