package com.rimi.util;

public class CodeUtil {

    /**
     * @return 4位验证码
     */
    public static String getCode() {

        // 去掉0，1 i，l,I,L
        String[] codes = {"2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e"
                , "f", "g", "h", "j", "k", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x"
                , "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "M", "N", "P", "Q", "R"
                , "S", "T", "U", "V", "W", "X", "Y", "Z"};
        String code = "";
        //随机生成一个1位0-codes.length数字
        for (int i = 0; i < 4; i++) {
            int index = (int) (Math.random() * 55);
            code += codes[index];
        }

        return code;
    }


}
