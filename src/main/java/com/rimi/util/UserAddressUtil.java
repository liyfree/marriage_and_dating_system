package com.rimi.util;


import com.rimi.bean.UserAddress;

/**
 * 操作用户添加的地址
 */
public interface UserAddressUtil {

    /**
     * 拼接地址对象获取用户地址
     * @param userAddress  前端传递的地址对象
     * @return 用户地址
     */
    String getAddress(UserAddress userAddress);


    /**
     * 将用户的地址字段转化为地址对象
     * @param userAddress 地址字段
     * @return
     */
    UserAddress getUserAddress(String userAddress);




}
