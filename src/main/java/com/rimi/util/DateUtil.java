package com.rimi.util;

import java.util.Date;

/**
 * 操作时间的工具
 */
public interface DateUtil {


    /**
     * 格式化日期为字符串
     *
     * @param date 要转化的日期
     * @return 转化后的字符串
     */
    String dateformat(Date date);


    /**
     * 根据生日获取年龄
     *
     * @param birthDate 生日
     * @return 年龄
     */
    Integer getAgeByDate(Date birthDate);

    /**
     * 根据字符串获取出生日期
     *
     * @param str 获取的日期的字符串
     * @return 日期
     */
    Date getDateByStr(String str);


    /**
     * 根据字符串值获取年份
     * @param s 字符串类型的出生年份
     * @return 转化后的date类型的时间
     */
    Date getDateYearByStr(String s);
}
