package com.rimi.util.impl;

import com.rimi.util.DateUtil;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateUtilImpl implements DateUtil {


    /**
     * 格式化日期为字符串
     * @param date 要转化的日期
     * @return 转化后的字符串
     */
    @Override
    public String dateformat(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(date);
    }


    /**
     * 根据生日获取年龄
     * @param birthDate 生日
     * @return 年龄
     */
    @Override
    public Integer getAgeByDate(Date birthDate) {
        if (birthDate != null && !"".equals(birthDate)) {// 添加出生时间才会获取年龄值
            // 出生年月时间格式化
            String birth = dateformat(birthDate);
            birth= birth.replace("-","");
            // 当前时间格式化
            String nowDate = dateformat(new Date());
            nowDate=nowDate.replace("-","");
            // 将出生时间转化为int 类型
            Integer birthNum = Integer.parseInt(birth);
            // 将当前时间转换为 int类型
            Integer nowNum = Integer.parseInt(nowDate);
            // 两个时间差为年龄
            Integer age = nowNum - birthNum;
            System.out.println(birthDate);
            System.out.println(birth);
            // 返回年龄值
            return age;
        } else {// 未添加年龄则返回空值
            return null;
        }
    }


    /**
     * 根据字符串获取出生日期
     * @param str 获取的日期的字符串
     * @return 日期
     */
    @Override
    public Date getDateByStr(String str) {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("字符串转换时间失败");
        }
        return date;
    }

    /**
     * 根据字符串值获取年份
     * @param s 字符串类型的出生年份
     * @return 转化后的date类型的时间
     */
    @Override
    public Date getDateYearByStr(String s){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy");
        Date date = null;
        try {
            date = sdf.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("字符串转换年份失败");
        }
        return date;
    }

}
