package com.rimi.util.impl;

import com.rimi.bean.UserAddress;
import com.rimi.util.UserAddressUtil;
import org.springframework.stereotype.Component;

@Component
public class UserAddressUtilImpl implements UserAddressUtil {

    /**
     * 拼接地址对象，获取用户自地址
     *
     * @param userAddress 前端传递的地址对象
     * @return 用户地址字段
     */
    @Override
    public String getAddress(UserAddress userAddress) {
        String userLocation = "";
        String s_province = userAddress.getS_province();
        String s_city = userAddress.getS_city();
        String s_county = userAddress.getS_county();
        if (s_province != null && !"".equals(s_province)) {
            userLocation = userLocation + s_province;
        }
        if (s_city != null && !"".equals(s_city)) {
            userLocation = userLocation + "," + s_city;
        }

        if (s_county != null && !"".equals(s_county)) {
            userLocation = userLocation + "," + s_county;
        }
        return userLocation;
    }


    /**
     * 将用户的地址字段转化为地址对象
     *
     * @param userAddress 地址字段
     * @return
     */
    @Override
    public UserAddress getUserAddress(String userAddress) {
        UserAddress userAdd = new UserAddress();
        if (userAddress != null && !"".equals(userAddress)) {
            String[] address = userAddress.split(",");
            userAdd.setS_province(address[0]);
            userAdd.setS_city(address[1]);
            if (address.length == 3) {
                userAdd.setS_county(address[2]);
            }

        } else {
            userAdd.setS_province("省份");
            userAdd.setS_city("地级市");
            userAdd.setS_county("县级市");
        }
        return userAdd;
    }


}
