package com.rimi.generator;

import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class AutoGenerator {

    public static void main(String[] args) {
        //new AutoGenerator().generateCode();
    }

    /**
     * 根据数据库表自动创建对象
     */
    public void generateCode() {
        /* 指定生成的父级包名 */
        String packageName = "com.rimi";
        boolean serviceNameStartWithI = false;//user -> UserService, 设置成true: user -> IUserService
        // 指定逆向生成的表名
        generateByTables(serviceNameStartWithI, packageName, "ballSport","bodyBuilding","foodHabit","interest"
                ,"leisureStyle","lifeStyle","likeUser","menu","message","outdoorSport"
                ,"pet","popularity","role","roleMenu","status","travel","user","userFriend"
                ,"userImage","userInterest","visit");
    }

    private void generateByTables(boolean serviceNameStartWithI, String packageName, String... tableNames) {
        GlobalConfig config = new GlobalConfig();
        /* 链接数据库的参数 */
        String dbUrl = "jdbc:mysql://localhost:3306/hunlian";
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setUrl(dbUrl)
                .setUsername("root")
                .setPassword("123456")
                .setDriverName("com.mysql.jdbc.Driver");
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig
                .setCapitalMode(true)
                .setEntityLombokModel(false)
                .setDbColumnUnderline(false)
                .setNaming(NamingStrategy.underline_to_camel)
                .setInclude(tableNames);//修改替换成你需要的表名，多个表名传数组
        config.setActiveRecord(false)
                .setAuthor("ly")
                .setOutputDir(System.getProperty("user.dir")+"/src/main/java")
                .setFileOverride(true);
        if (!serviceNameStartWithI) {
            config.setServiceName("%sService");
            config.setMapperName("%sDao");
        }
        new com.baomidou.mybatisplus.generator.AutoGenerator().setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(// 生成的文件路径
                        new PackageConfig()
                                .setParent(packageName)
                                .setController("controller")
                                .setEntity("bean")
                                .setMapper("dao")
                                .setXml("mapper")
                                .setService("service")
                                .setServiceImpl("service.impl")

                ).execute();
    }
}
