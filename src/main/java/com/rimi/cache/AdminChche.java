package com.rimi.cache;

import com.alibaba.fastjson.JSON;
import com.rimi.bean.User;
import com.rimi.bean.UserAndMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.List;

public class AdminChche {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    /**
     * 增加user集合到缓存
     */
    public void addUserList(String key, List<User> list){
        ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
        valueOperations.set(key, JSON.toJSONString(list));
    }

    /***
     * 缓存历史消息
     * @param key
     * @param list
     */
    public void  addMessageList(String key, List<UserAndMessage> list){
        ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
        valueOperations.set(key, JSON.toJSONString(list));
    }

}
