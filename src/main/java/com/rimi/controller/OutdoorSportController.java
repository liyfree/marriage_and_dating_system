package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.LifeStyle;
import com.rimi.bean.OutdoorSport;
import com.rimi.bean.User;
import com.rimi.bean.UserInterest;
import com.rimi.service.OutdoorSportService;
import com.rimi.service.UserInterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/outdoorSport")
public class OutdoorSportController {
    @Autowired
    private OutdoorSportService outdoorSportService;

    @Autowired
    private UserInterestService userInterestService;

    /**
     * 获取户外运动列表
     * @param request
     * @return
     */
    @RequestMapping("/getOutdoorSpotrList")
    public String getOutdoorSport(HttpServletRequest request) {
        //获取登录用户
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 获取用户的兴趣爱好对象
        UserInterest userInterest = userInterestService.selectOne(new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 判断对象是否存在，不存在则添加
        if (userInterest == null) {
            userInterestService.insertUserInterest(loginUser,userInterest );
        }
        // 将用户的生活方式字段进行截取成数组
        String[] OutdoorSport = userInterestService.getUserOutdoor(userInterest.getUserOutdoor());
        // 获取数据库兴趣列表
        List<OutdoorSport> outdoorSports = outdoorSportService.selectList(null);
        // 将选中的数据添加选中属性
        if (OutdoorSport != null) {
            for (OutdoorSport o : outdoorSports) {
                for (String s : OutdoorSport) {
                    if ( !"".equals(s) && o.getId() == Integer.parseInt(s)){
                        o.setChecked("checked");
                    }
                }
            }
        }
        // 将修改属性后的列表传到前端页面
        request.setAttribute("outdoorsportList",outdoorSports );
        return "outdoorsport";
    }



    @RequestMapping("/addUserOurdoorSport")
    public String addUserOutdoorSport(HttpServletRequest request,@RequestParam(value = "outdoorSport", required = false) String[] outdoorSport){

        // 将数组转化为数据表字段类型
        String outdoorStr = outdoorSportService.getOutdoorSportStr(outdoorSport);
        // 将字段添加到数据表中
        boolean updateOutdoor = userInterestService.updateOutdoor(outdoorStr, (User) request.getSession().getAttribute("loginUser"));
        System.out.println(updateOutdoor);
        return "redirect:/outdoorSport/getOutdoorSpotrList";
    }
}

