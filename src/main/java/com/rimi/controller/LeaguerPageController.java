package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.LikeUser;
import com.rimi.bean.User;
import com.rimi.bean.UserImage;
import com.rimi.service.LeaguerPageService;
import com.rimi.service.LikeUserService;
import com.rimi.service.UserImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 点击查看会员详细信息
 */
@Controller
@RequestMapping("/leaguerPage")
public class LeaguerPageController {
    @Autowired
    private LeaguerPageService leaguerPageService;
    @Autowired
    private LikeUserService likeUserService;
    @Autowired
    private UserImageService userImageService;


    /**
     * 点击列表中的会员查询会员详细信息
     * @param userid 要查看的会员id
     * @return 跳转到显示会员详细信息页面
     */
    @RequestMapping("/leaguerInfo")
    public String getLeaguerInfoById(Integer userid, Model model, HttpServletRequest request){
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 根据id获取要查看的对象
        User leaguer = leaguerPageService.getLeaguerInfo(userid);
        // 将用户的出生年月转化为年龄
        Integer age = leaguerPageService.getAgeByDate(leaguer.getUserBirthday());
        // 将要查看的对象进行存储
        model.addAttribute("leaguer",leaguer);
        // 将对象的年龄进行存储
        model.addAttribute("leaguerAge",age);
        // 将择偶条件对象传到页面
        model.addAttribute("likeUser", likeUserService.selectOne(new EntityWrapper<LikeUser>().eq("user_id",userid )));

        // 查询图片列表，传递到页面
        List<UserImage> userImageList = userImageService.selectList(new EntityWrapper<UserImage>().eq("user_id", userid));
        model.addAttribute("userImageList", userImageList);
        // 获取登录会员图片
        UserImage userImage = userImageService.selectOne(new EntityWrapper<UserImage>().eq("user_id", loginUser.getUserId()));
        model.addAttribute("loginUserImage", userImage);
        // 返回访问会员详细信息页面
        return "personalshow";
    }
}
