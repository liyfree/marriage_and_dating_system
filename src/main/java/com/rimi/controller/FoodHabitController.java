package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.FoodHabit;
import com.rimi.bean.LifeStyle;
import com.rimi.bean.User;
import com.rimi.bean.UserInterest;
import com.rimi.service.FoodHabitService;
import com.rimi.service.UserInterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/foodHabit")
public class FoodHabitController {
    @Autowired
    private FoodHabitService foodHabitService;
    @Autowired
    private UserInterestService userInterestService;

    /**
     * 获取饮食爱好列表
     *
     * @param request
     * @return
     */
    @RequestMapping("/getFoodHabitList")
    public String getFoodHabitList(HttpServletRequest request) {
        //获取登录用户
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 获取用户的兴趣爱好对象
        UserInterest userInterest = userInterestService.selectOne(new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 判断对象是否存在，不存在则添加
        if (userInterest == null) {
            userInterestService.insertUserInterest(loginUser,userInterest );
        }
        // 将用户的饮食习惯字段进行截取成数组
        String[] foodHobby = userInterestService.getUserFood(userInterest.getUserFood());
        // 获取数据库兴趣列表
        List<FoodHabit> foodHobbys = foodHabitService.selectList(null);
        // 将选中的数据添加选中属性
        if (foodHobby != null) {
            for (FoodHabit foodHabit : foodHobbys) {
                for (String s : foodHobby) {
                    if ( !"".equals(s) && foodHabit.getId() == Integer.parseInt(s)){
                        foodHabit.setChecked("checked");
                    }
                }
            }
        }
        request.getSession().setAttribute("foodHabitList", foodHobbys);
        return "foodhobby";
    }


    @RequestMapping("/addUserFoodHabit")
    public String addUserFoodHabit(HttpServletRequest request, @RequestParam(value = "foodHobby", required = false) String[] foodHobby) {
        // 将数组转化为数据表字段类型
        String foodHobbyStr = foodHabitService.getFoodHobby(foodHobby);
        // 将字段添加到数据表中
        boolean updateFoodHobby = userInterestService.updateUserFood(foodHobbyStr, (User) request.getSession().getAttribute("loginUser"));
        System.out.println(updateFoodHobby);
        return "redirect:/foodHabit/getFoodHabitList";

    }
}

