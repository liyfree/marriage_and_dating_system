package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.CommonDate;
import com.rimi.bean.Popularity;
import com.rimi.bean.Visit;
import com.rimi.service.PopularityService;
import com.rimi.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/visit")
public class VisitController {

    @Autowired
    private VisitService visitService;

    @Autowired
    private PopularityService popularityService;

    @RequestMapping("/addGaun")
    public void addGuan(String userid, HttpServletResponse response) {
        String state = "5";
        String[] split = userid.split(",");
        String s = split[0];
        String s1 = split[1];
        if (!s.equals(s1)) {
            CommonDate commonDate = new CommonDate();
            String time = commonDate.getTimeString();
            Visit chatvisit = visitService.selectOne(new EntityWrapper<Visit>().eq("user_id", Integer.parseInt(s)).eq("people_id", Integer.parseInt(s1)));
            if (chatvisit != null) {
                Date dateYearByStr = commonDate.getStingtoDate(chatvisit.getTime());
                Integer gettime = commonDate.gettime(dateYearByStr);
                if (gettime >= 1) {
                    Popularity user_id = popularityService.selectOne(new EntityWrapper<Popularity>().eq("user_id", Integer.parseInt(s)));
                    if (user_id!=null){
                        boolean b1 = popularityService.updatePopu(new Popularity(Integer.parseInt(s), user_id.getNum() + chatvisit.getNum()));
                        if (b1==true) {
                            boolean b = visitService.UpdateVisit(new Visit(Integer.parseInt(s), Integer.parseInt(s1), time, 0));
                            if (b == true) {
                                state = "1";
                            }
                        }
                    }
                }else if (chatvisit.getNum() >= 5) {
                    state = "2";
                } else {
                    state = "1";
                }
            } else {
                boolean b = visitService.AddVisit(new Visit(Integer.parseInt(s), Integer.parseInt(s1), time, 0));
                if (b == true) {
                    state = "1";
                }
            }
        }
        try {
            response.getWriter().write(state);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RequestMapping("/addGaunj")
    public void addGuanj(String userid, HttpServletResponse response){
        String state = "1";
        String[] split = userid.split(",");
        String s = split[0];
        String s1 = split[1];
        CommonDate commonDate = new CommonDate();
        String time = commonDate.getTimeString();
        Visit chatvisit = visitService.selectOne(new EntityWrapper<Visit>().eq("user_id",Integer.parseInt(s)).eq("people_id",Integer.parseInt(s1)));
        boolean b = visitService.UpdateVisit(new Visit(Integer.parseInt(s), Integer.parseInt(s1), time, chatvisit.getNum()+1));
        if (b==true) {
            state = "2";
        }
        try {
            response.getWriter().write(state);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

