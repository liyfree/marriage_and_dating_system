package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.LifeStyle;
import com.rimi.bean.Pet;
import com.rimi.bean.User;
import com.rimi.bean.UserInterest;
import com.rimi.service.PetService;
import com.rimi.service.UserInterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/petHobby")
public class PetController {
    @Autowired
    private PetService petService;
    @Autowired
    private UserInterestService userInterestService;

    /**
     * 进入用户宠物爱好页面
     *
     * @param request
     * @return
     */
    @RequestMapping("/getHobbyList")
    public String getHobbyList(HttpServletRequest request) {
        //获取登录用户
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 获取用户的兴趣爱好对象
        UserInterest userInterest = userInterestService.selectOne(new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 判断对象是否存在，不存在则添加
        if (userInterest == null) {
            userInterestService.insertUserInterest(loginUser,userInterest );
        }
        // 将用户的生活方式字段进行截取成数组
        String[] pet = userInterestService.getUserPet(userInterest.getUserPet());
        // 获取数据库兴趣列表
        List<Pet> pets = petService.selectList(null);
        // 将选中的数据添加选中属性
        if (pet != null) {
            for (Pet p: pets) {
                for (String s : pet) {
                    if ( !"".equals(s) && p.getId() == Integer.parseInt(s)){
                        p.setChecked("checked");
                    }
                }
            }
        }
        // 将修改属性后的列表传到前端页面
        request.setAttribute("petList",pets );
        return "pethobby";
    }


    @RequestMapping("/addPetHobby")
    public String addPetHobby(HttpServletRequest request, @RequestParam(value = "petHobby", required = false)String[] petHobby){

        for (String s : petHobby) {
            System.out.println(s);
        }
        // 将数组转化为数据表字段类型
        String petHobbyStr = petService.getUserPetHobby(petHobby);
        // 将字段添加到数据表中
        boolean updatePet = userInterestService.updateUserPet(petHobbyStr, (User) request.getSession().getAttribute("loginUser"));
        return "redirect:/petHobby/getHobbyList";
    }


}

