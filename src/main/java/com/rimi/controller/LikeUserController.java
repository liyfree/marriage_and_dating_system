package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.LikeUser;
import com.rimi.bean.User;
import com.rimi.bean.UserAddress;
import com.rimi.service.LikeUserService;
import com.rimi.util.DateUtil;
import com.rimi.util.UserAddressUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/likeUser")
public class LikeUserController {
    @Autowired
    private LikeUserService likeUserService;
    @Autowired
    public UserAddressUtil userAddressUtil;
    @Autowired
    public DateUtil dateUtil;


    /**
     * 跳转添加择偶条件的页面
     *
     * @param request 获取session中的对象
     * @return 跳转择偶页面
     */
    @RequestMapping("/otherHalf")
    public String addOtherHlf(HttpServletRequest request) {
        //获取session 中的用户对象
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 根据用户id获取用户择偶条件的对象
        LikeUser likeUser = likeUserService.selectById(loginUser.getUserId());
        System.out.println(likeUser.getUserBirthday());
        // 判断择偶对象是否存在
        boolean hava = likeUserService.isHava(likeUser);
        // 如果不存在，就在数据表中添加一条数据
        if (hava == false) {
            likeUser = new LikeUser();
            likeUser.setUserId(loginUser.getUserId());
            likeUserService.insert(likeUser);
        }
        // 将择偶对象利用model 传递值页面
        request.getSession().setAttribute("likeUser", likeUser);
        // 将择偶标准的的户籍地址获取，并转化为地址对象
        UserAddress fromPlace = userAddressUtil.getUserAddress(likeUser.getUserFromplace());
        // 利用model对象将地址对象传递到页面
        request.getSession().setAttribute("likeUserFromPlace", fromPlace);
        // 将择偶标准的现居住地址获取，并转化为地址对象
        UserAddress location = userAddressUtil.getUserAddress(likeUser.getUserLocation());
        // 利用model对象将地址对象传递到页面
       request.getSession().setAttribute("likeUserLocation", location);
        // 跳转到择偶条件的页面
        return "otherhalf";
    }

    /**
     * 添加用户择偶条件
     * @param model 向页面传参
     * @param request 获取session对象
     * @param likeUser 择偶条件对象
     * @param likeUserBir 择偶条件出生年份
     * @param userAddress 地址对象
     * @return
     */
    @RequestMapping("/addOtherHalf")
    public String addLikeUser(Model model, HttpServletRequest request, LikeUser likeUser, String likeUserBir, UserAddress userAddress,String s_province1,String s_city1) {
        //  拼接择偶对象的户籍地址，并给对象赋值
        UserAddress likeUserFromPlace = new UserAddress();
        likeUserFromPlace.setS_province(s_province1);
        likeUserFromPlace.setS_city(s_city1);
        String address = userAddressUtil.getAddress(likeUserFromPlace);
        likeUser.setUserFromplace(address);

        System.out.println(likeUserBir);
        // 将用户的生日由String类型转化为date类型,并赋值
        Date dateByStr = dateUtil.getDateYearByStr(likeUserBir);
        likeUser.setUserBirthday(dateByStr);
        // 将择偶对象的现居住地转化为string类型
        String likeUserLocation= userAddressUtil.getAddress(userAddress);
        // 将现居住地址添加到择偶对象中
        likeUser.setUserLocation(likeUserLocation);
        // 将所有数据接收完成后将信息添加到表中
        boolean isok = likeUserService.update(likeUser, new EntityWrapper<LikeUser>().eq("user_id", likeUser.getUserId()));
        System.out.println(isok);
        // 获取改后的对象
        LikeUser likeUser1 = likeUserService.selectById(likeUser.getUserId());
        // 将这个对象通过model传递到页面
        request.getSession().setAttribute("likeUser", likeUser1);
        // 将现居住地对象传递到页面
        request.getSession().setAttribute("likeUserLocation", userAddress);
        // 将户籍地址对象通过model传递到页面
        request.getSession().setAttribute("likeUserFromPlace", likeUserFromPlace);
        // 跳转页面
        return "otherhalf";
    }


}

