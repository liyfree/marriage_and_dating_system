package com.rimi.controller;

import com.google.gson.Gson;
import com.rimi.bean.*;
import com.rimi.service.UserFriendService;
import com.rimi.service.UserImageService;
import com.rimi.service.UserService;
import com.rimi.websocket.MessageSockt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/userFriend")
public class UserFriendController {

    @Autowired
    private UserFriendService userFriendService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserImageService userImageService;
    private  Gson gson = new Gson();
    /**
     * 申请好友
     * @return
     *  true 为申请好友成功
     *   1   申请失败
     *  2   申请成功
     */
    @RequestMapping("/AddFriend")
    public void addUserFriend(String userid,HttpServletResponse response){
        String[] split = userid.split(",");
        String userid1 = split[0];
        String userid2 = split[1];
        //设置状态初始值
        String state = "1";
        //将信息存储到数据库中
        boolean b = userFriendService.addUserFriend(new UserFriend(Integer.parseInt(userid1),Integer.parseInt(userid2),1));
        // 如果储存成功便更改状态值
        if (b == true){
            MessageSockt  messageSock = new MessageSockt();
            CommonDate commonDate = new CommonDate();
            User user = userService.selectById(Integer.parseInt(userid2));
            String messages = user.getUserRealname()+"申请你为好友";
            Message message = new Message(0,"通知",commonDate.getTimeString(),messages,Integer.parseInt(userid2),Integer.parseInt(userid1));

            String s = gson.toJson(message);
            messageSock.sendToUser(s);
            state = "2";
        }
        try {
            //将状态值返回到页面进行判断
            response.getWriter().write(state);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /***
     * 查询好友是否存在
     *
     * @return
     *   1   正在申请对方为好友
     *   2   已为好友
     *   3   对方正在添加你为好友
     *   4   不是好友
     */
    @RequestMapping("/FriendHave")
    public void LookFriend(String userid,HttpServletResponse response) {
        String state = "4";
        String[] split = userid.split(",");
        String s = split[0];
        String s1 = split[1];
        if (!s.equals(s1)) {
            List<UserFriend> list = userFriendService.UserFriendHave(Integer.parseInt(s1));
            if (list.size() > 0) {
                for (UserFriend userFriend1 : list) {
                    if (userFriend1.getFriendId() == Integer.parseInt(s)) {
                        switch (userFriend1.getRelation()) {
                            case 1:
                                state = "1";
                                break;
                            case 2:
                                state = "2";
                                break;
                            case 3:
                                state = "3";
                                break;
                            case 4:
                                state = "4";
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                }
            }
        }else {
            state ="5";
        }
        try {
            response.getWriter().write(state);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /***
     * 查询好友列表
     * @param userId
     * @return 好友对象
     */
    @RequestMapping("/FriendList")
    @ResponseBody
    public void FriendList(String userId, HttpServletResponse response, HttpServletRequest request){
        Integer userIds = Integer.valueOf(userId);
        //根据id查询好友的id值
        List<UserFriend> userFriends = userFriendService.UserFriendlist(userIds);
        //创建一个UserList集合储存好友User
        List<UserandImage> userImages = new ArrayList<>();
        List<User> onlineList = (List<User>) request.getServletContext().getAttribute("onlineList");
        int a = 0;
        for (UserFriend userFriend : userFriends) {
            User user = userService.selectById(userFriend.getFriendId());
            UserImage userImage = userImageService.ChatImage(user.getUserId());
            UserandImage userandImage = null;
            for (User user1 : onlineList) {
                if (user1.getUserId() == userFriend.getFriendId()) {
                    a=1;
                    break;
                }
            }
            userandImage = new UserandImage(user.getUserId(), user.getUserRealname(), userImage.getImageUrl(), a);
            userImages.add(userandImage);
        }
        Gson gson = new Gson();
        String s1 = gson.toJson(userImages);
        response.setCharacterEncoding("UTF-8");
        try {
            response.getWriter().write(s1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***
     * 查询好友申请信息
     * @param userId
     * @return
     */
    @RequestMapping("/FriendChat")
    @ResponseBody()
    public void FriendChat(String userId,HttpServletResponse response) {
        Integer userIds = Integer.valueOf(userId);
        //根据id查询好友的id值
        List<UserFriend> userFriends = userFriendService.UserFriendChat(userIds);
        //创建一个UserList集合储存好友User
        List<UserandImage> userImages = new ArrayList<>();
        for (UserFriend u : userFriends) {
            User users = userService.selectById(u.getFriendId());
            UserImage userImage = userImageService.ChatImage(users.getUserId());
            UserandImage userandImage = new UserandImage(users.getUserId(),users.getUserRealname(),userImage.getImageUrl(),0);
            userImages.add(userandImage);
        }
        Gson gson = new Gson();
        String s = gson.toJson(userImages);
        response.setCharacterEncoding("UTF-8");
        try {
            response.getWriter().write(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /***
     * 初始化聊天窗口
     * @return 转发index.jsp
     */

    @RequestMapping("/UpChat")
    public String UpChat(){
        return "index2";
    }
    /**
     * 确认好友关系
     * @return
     * 1 同意添加好友
     * 2 拒绝好友
     */
    @RequestMapping("/FriendAnd")
    public void  FriendAnd(String s,HttpServletResponse response){
        String font = "";
        String messages ="";
        String[] split = s.split(",");
        String userid = split[0];
        String friendid = split[1];
        String state = split[2];
        System.out.println(state);
        MessageSockt messageSockt = new MessageSockt();
        User user = userService.selectById(Integer.parseInt(friendid));
        CommonDate commonDate = new CommonDate();
        if (state.equals("1")){
            boolean b = userFriendService.updateFriend(new UserFriend(Integer.valueOf(userid), Integer.valueOf(friendid), 2));
            if (b==true) {
                font = "1";
            }else{
                font = "2";
            }
            messages =  user.getUserRealname()+"同意了你的好友请求";
        }else{
            boolean b = userFriendService.updateFriend(new UserFriend(Integer.valueOf(userid), Integer.valueOf(friendid), 4));
            if (b==true) {
                font = "3";
            }else{
                font = "4";
            }
            messages =  user.getUserRealname()+"拒绝了你的好友请求";

        }

        Message message = new Message(0,"通知",commonDate.getTimeString(),messages,Integer.parseInt(friendid),Integer.parseInt(userid));
        String ss = gson.toJson(message);
        messageSockt.sendToUser(ss);
        try {
            response.getWriter().write(font);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RequestMapping("/UserChat")
    public void  UserChat(String userid,HttpServletResponse response){
        response.setCharacterEncoding("UTF-8");
        System.out.println(userid);
        User user = userService.selectById(userid);
        try {
            response.getWriter().write(user.getUserRealname());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

