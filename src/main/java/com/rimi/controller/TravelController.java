package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.LifeStyle;
import com.rimi.bean.Travel;
import com.rimi.bean.User;
import com.rimi.bean.UserInterest;
import com.rimi.service.TravelService;
import com.rimi.service.UserInterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/travel")
public class TravelController {

    @Autowired
    private TravelService travelService;
    @Autowired
    private UserInterestService userInterestService;


    /**
     * 获取旅游方式的列表
     *
     * @param request
     * @return
     */
    @RequestMapping("getTravelList")
    public String getTravelStyleList(HttpServletRequest request) {
        //获取登录用户
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 获取用户的兴趣爱好对象
        UserInterest userInterest = userInterestService.selectOne(new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 判断对象是否存在，不存在则添加
        if (userInterest == null) {
            userInterestService.insertUserInterest(loginUser,userInterest );
        }
        // 将用户的生活方式字段进行截取成数组
        String[] travelBy = userInterestService.getUserTravel(userInterest.getUserTravel());
        // 获取数据库兴趣列表
        List<Travel> travelBys =travelService.selectList(null);
        // 将选中的数据添加选中属性
        if (travelBy != null) {
            for (Travel travel : travelBys) {
                for (String s : travelBy) {
                    if ( !"".equals(s) && travel.getId() == Integer.parseInt(s)){
                        travel.setChecked("checked");
                    }
                }
            }
        }
        // 将修改属性后的列表传到前端页面
        request.setAttribute("travelList",travelBys );
        return "travelby";
    }


    @RequestMapping("/addTravelList")
    public String addTravelList(HttpServletRequest request, @RequestParam(value = "travelBy", required = false) String[] travelBy){
        // 将数组转化为数据表字段类型
        String travelByStr = travelService.getTravelByStr(travelBy);
        // 将字段添加到数据表中
        boolean updateTravelStyle = userInterestService.updateUserTravel(travelByStr, (User) request.getSession().getAttribute("loginUser"));
        System.out.println(updateTravelStyle);
        return "redirect:/travel/getLifeStyleList";
    }


}

