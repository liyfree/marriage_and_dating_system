package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.BodyBuilding;
import com.rimi.bean.LifeStyle;
import com.rimi.bean.User;
import com.rimi.bean.UserInterest;
import com.rimi.service.BodyBuildingService;
import com.rimi.service.UserInterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/bodyBuilding")
public class BodyBuildingController {

    @Autowired
    private BodyBuildingService bodyBuildingService;

    @Autowired
    private UserInterestService userInterestService;

    /**
     * 获取运动健身列表
     * @param request
     * @return
     */
    @RequestMapping("/getBodyBuildList")
    public String getList(HttpServletRequest request){
        //获取登录用户
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 获取用户的兴趣爱好对象
        UserInterest userInterest = userInterestService.selectOne(new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 判断对象是否存在，不存在则添加
        if (userInterest == null) {
            userInterestService.insertUserInterest(loginUser,userInterest );
        }
        // 将用户的生活方式字段进行截取成数组
        String[] bodybuliding = userInterestService.getUserBodybuliding(userInterest.getUserBodybuliding());
        // 获取数据库兴趣列表
        List<BodyBuilding> bodyBuildings = bodyBuildingService.selectList(null);
        // 将选中的数据添加选中属性
        if (bodybuliding != null) {
            for (BodyBuilding bodyBuilding : bodyBuildings) {
                for (String s : bodybuliding) {
                    if (!"".equals(s) && bodyBuilding.getId() == Integer.parseInt(s)){
                        bodyBuilding.setChecked("checked");
                    }
                }
            }
        }
        // 将修改属性后的列表传到前端页面
        request.setAttribute("bodyBuildList",bodyBuildings );
        return "bodybuilding";
    }


    @RequestMapping("/addBodyBuild")
    public String addLifeStyle(HttpServletRequest request, @RequestParam(value = "bodyBuild", required = false) String[] bodyBuild) {
        // 将数组转化为数据表字段类型
        String bodyBuildStr = bodyBuildingService.getBodyBuildStr(bodyBuild);
        // 将字段添加到数据表中
        boolean updateBodyBuild = userInterestService.updateUserBodybuliding(bodyBuildStr,(User) request.getSession().getAttribute("loginUser") );
        System.out.println(updateBodyBuild);
        return "redirect:/bodyBuilding/getBodyBuildList";

    }



}

