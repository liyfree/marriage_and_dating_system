package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.LifeStyle;
import com.rimi.bean.User;
import com.rimi.bean.UserInterest;
import com.rimi.service.LifeStyleService;
import com.rimi.service.UserInterestService;
import com.rimi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/lifeStyle")
public class LifeStyleController {


    @Autowired
    private LifeStyleService lifeStyleService;
    @Autowired
    private UserInterestService userInterestService;

    /**
     * 进入用户生活方式页面
     *
     * @param request
     * @return
     */
    @RequestMapping("/getLifeStyleList")
    public String getIntoLifeStyle(HttpServletRequest request) {
        //获取登录用户
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 获取用户的兴趣爱好对象
        UserInterest userInterest = userInterestService.selectOne(new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 判断对象是否存在，不存在则添加
        if (userInterest == null) {
            userInterestService.insertUserInterest(loginUser,userInterest );
        }
        // 将用户的生活方式字段进行截取成数组
        String[] lifeStyle = userInterestService.getLifeStyle(userInterest.getUserLifestyle());
        // 获取数据库兴趣列表
        List<LifeStyle> lifeStyles = lifeStyleService.selectList(null);
        // 将选中的数据添加选中属性
        if (lifeStyle != null) {
            for (LifeStyle style : lifeStyles) {
                for (String s : lifeStyle) {
                    if ( !"".equals(s) && style.getId() == Integer.parseInt(s)){
                        style.setChecked("checked");
                    }
                }
            }
        }
        // 将修改属性后的列表传到前端页面
        request.setAttribute("lifestyleList",lifeStyles );
        return "lifestyle";
    }


    @RequestMapping("/addLifeStyle")
    public String addLifeStyle(HttpServletRequest request, @RequestParam(value = "lifeStyle", required = false) String[] lifeStyle) {
        // 将数组转化为数据表字段类型
        String lifeStyleStr = lifeStyleService.getLifeStyleStr(lifeStyle);
        // 将字段添加到数据表中
        boolean updateLifeStyle = userInterestService.updateLifeStyle(lifeStyleStr, (User) request.getSession().getAttribute("loginUser"));
        System.out.println(updateLifeStyle);
        return "redirect:/lifeStyle/getLifeStyleList";

    }


}

