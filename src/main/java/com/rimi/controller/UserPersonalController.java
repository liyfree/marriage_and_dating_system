package com.rimi.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.User;
import com.rimi.bean.UserAddress;

import com.rimi.service.LikeUserService;
import com.rimi.service.UserPersonalService;
import com.rimi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


/**
 * 会员个人中心页面
 */
@Controller
@RequestMapping("/userPersonal")
public class UserPersonalController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserPersonalService userPersonalService;

    /**
     * 点击进入当前登录用户个人中心
     *
     * @return 用户个人中心管理页面
     */
    @RequestMapping()
    public String getIntoPersonalCenter(HttpServletRequest request,Model model) {
        // 进去个人中心页面首先获取当前登录用户对象
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        System.out.println(loginUser);
        // 将用户的户籍地址字段转化为地址对象
        UserAddress userAddress = userPersonalService.getUserAddress(loginUser.getUserFromplace());
        // 将户籍地址对象通过model传递到页面
        model.addAttribute("loginUserFromPlace", userAddress);
        // 跳转到添加用户个人基本信息页面
        return "personalcenter";
    }


    /**
     * 添加用户基本信息
     *
     * @param model       传参对象
     * @param user        对象基本信息
     * @param birthday    用户生日，字符串对象，用户转换为date类型
     * @param userAddress 用户户籍地址对象
     * @param request     获取session对象
     * @return 跳转到添加用户联系信息页面
     */
    @RequestMapping("/addBasicMsg")
    public String insertBasicMsg(Model model, User user, String birthday, UserAddress userAddress, HttpServletRequest request) {
        // 将转化后的地址赋值给对象的户籍地址字段
        user.setUserFromplace(userPersonalService.getAddress(userAddress));
        // 将转化后的时间赋值给用户生日字段
        user.setUserBirthday(userPersonalService.getDateByStr(birthday));
        // 将用户对象的属性进行修改
        boolean update = userService.update(user, new EntityWrapper<User>().eq("user_id", user.getUserId()));
        System.out.println(update);
        // 获取修改后的对象
        User updateUser = userService.selectOne(new EntityWrapper<User>().eq("user_id", user.getUserId()));
        // 修改对象后，更新session中存的登录对象
        request.getSession().setAttribute("loginUser", updateUser);
        // 将用户的户籍地址通过model传递到页面
        request.getSession().setAttribute("loginUserFromPlace", userAddress);
        // 跳转至添加用户联系信息页面
        return "contactuser";
    }

    /**
     * 跳转添加用户联系方式页面
     *
     * @param request 获取session
     * @return 添加用户联系方式界面
     */
    @RequestMapping("/addcontactPage")
    public String getIntoContactPage(HttpServletRequest request,Model model) {
        // 获取session中的登录对象
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 将用户的所在地转化为地址对象
        UserAddress userLocation = userPersonalService.getUserAddress(loginUser.getUserLocation());
        // 将用户的现居住地通过model传递到页面
        request.getSession().setAttribute("userLocation", userLocation);
        // 跳转到添加用户的联系方式页面
        return "contactuser";
    }


    /**
     * 添加会员的联系方式
     *
     * @param model
     * @return 跳转添加联系方式页面
     */
    @RequestMapping("/addContact")
    public String addContact(Model model, User user, HttpServletRequest request, UserAddress userAddress) {

        System.out.println(user);
        // 根据页面传的用户现居住地对象获取用户的居住地址
        String address = userPersonalService.getAddress(userAddress);
        // 给要操作的用户将现居住地的内容赋值
        user.setUserLocation(address);
        // 添加或修改用户的联系信息
        boolean userId = userService.update(user, new EntityWrapper<User>().eq("user_id", user.getUserId()));
        // 判断是否修改成功
        System.out.println(userId);
        // 获取修改或添加后的用户对象
        User user1 = userService.selectOne(new EntityWrapper<User>().eq("user_id", user.getUserId()));
        // 将session中的登录对象信息
        request.getSession().setAttribute("loginUser", user1);
        // 更新用户现居住地,通过model传递到页面
        request.getSession().setAttribute("userLocation", userAddress);
        // 跳转到用户择偶条件的页面
        return "otherhalf";
    }




    /**
     * 显示用户照片
     *
     * @return
     */
    @RequestMapping("/image")
    public String userImage() {
        return "imageshow";
    }


    /**
     * 进入用户上传照片页面
     *
     * @return 上传照片页面
     */
    @RequestMapping("/uploadPicture")
    public String getIntoUploadPic() {
        return "imageupload";
    }


}




