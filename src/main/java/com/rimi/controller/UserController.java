package com.rimi.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.gson.Gson;
import com.rimi.bean.*;
import com.rimi.service.LikeUserService;
import com.rimi.service.UserImageService;
import com.rimi.bean.*;
import com.rimi.service.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.LifeStyle;
import com.rimi.bean.User;
import com.rimi.bean.UserInformation;
import com.rimi.bean.UserPage;
import com.rimi.dao.UserDao;
import com.rimi.service.LifeStyleService;
import com.rimi.service.UserInterestService;
import com.rimi.service.UserService;
import com.rimi.util.CodeUtil;
import com.rimi.util.MD5Util;
import com.rimi.websocket.MessageSockt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private LikeUserService likeUserService;
    @Autowired
    private UserInterestService userInterestService;
    @Autowired
    private UserImageService userImageService;
    @Autowired
    private PopularityService popularityService;
    private CommonDate commonDate = new CommonDate();
    private Gson gson = new Gson();
    /**
     * 跳转登录页面
     *
     * @return
     */
    @RequestMapping("/index")
    public String index(){

        return "login";
    }

    /**
     * 跳转注册页面
     *
     * @return
     */
    @RequestMapping("/register")
    public String reg() {

        return "reg";
    }


    /**
     * 注册用户
     */
    @RequestMapping("/reg")
    public String reg(User user, HttpServletRequest request) {

        //根据账号去数据库查询id
        String id = userService.getUserIdByAccount(user.getUserAccount());
        if (id == null || "".equals(id)) {//没查询到数据向数据库中写入数据
            // 获取当前时间
            Date nowTime = new Date();
            //设置插入时间
            user.setUserAddtime(nowTime);
            //设置注册成功后登录时间为插入时间
            user.setUserLogintime(nowTime);
            // 加密密码
            String string2MD5 = MD5Util.convertMD5(user.getUserPassword());
            user.setUserPassword(string2MD5);
            //将注册用户插入到数据库
            Integer num = userService.insertUser(user);
            // 获取插入用户的id
            String insertId = userService.getUserIdByAccount(user.getUserAccount());

            //一个用户注册成功后，user表相关联的表like_user user_interest user_image要插入一条数据
            //通过ID向 like_user user_interest user_image 插入id
            int regId = Integer.parseInt(insertId);//转型
            LikeUser likeUser = new LikeUser();
            UserInterest userInterest = new UserInterest();
            UserImage userImage = new UserImage();
            Popularity popularity = new Popularity();

            likeUser.setUserId(regId);
            userInterest.setUserId(regId);
            userImage.setUserId(regId);
            userImage.setImageUrl("/images/tou.png");
            popularity.setUserId(regId);

            //控制台显示数据
            boolean b = likeUserService.insertAllColumn(likeUser);
            boolean b1 = userInterestService.insertAllColumn(userInterest);
            boolean b2 = userImageService.insertAllColumn(userImage);
            boolean b3 = popularityService.insertAllColumn(popularity);

            //System.out.println("喜欢异性类型表插入："+b);
            //System.out.println("爱好表表插入："+b1);
            //System.out.println("图片表表插入："+b2);

            if (num != 0 && b && b1 && b2) {// 写入数据库成功，
                // 将登录用户存入session
                request.getSession().setAttribute("loginUser", user);
                // 获取在线用户列表
                List<User> onlineList = (List<User>) request.getServletContext().getAttribute("onlineList");

                // 将登录用户放入在线列表中
                if (onlineList == null) {// 第一次登录
                    onlineList = new ArrayList<>();
                }
                // 1.先要判断集合中是否已经将有该用户，没有则存入集合
                if (!onlineList.contains(user)) {
                    onlineList.add(user);
                }
                // 将集合存入context
                request.getServletContext().setAttribute("onlineList", onlineList);
                // 从session中删除验证码
                request.getSession().removeAttribute("code");

                // 注册成功显示个人中心页面
                return "personalcenter";
            }
        }

        return "register";//
    }

    /**
     * 获取验证码
     */
    @RequestMapping("/getcode")
    @ResponseBody
    public String getCode(String account, final HttpServletRequest request) {

        // 获取验证码
        String code = CodeUtil.getCode();
        // 获取时间戳
        long codeStartTime = System.currentTimeMillis();
        // 存储验证码到session中
        request.getSession().setAttribute("code", code);
        request.getSession().setAttribute("codeStartTime", codeStartTime);

        // 5分钟后删除code
        final HttpSession session = request.getSession();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                session.removeAttribute("code");
                System.out.println("验证码已删除");
            }
        }, 60000);//一分钟后删除验证码


        try {
            // 向注册邮箱发送验证码
            userService.sendCodeToAccount(code, account);
            // 发送成功返回1
            return "1";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }

        return "";
    }


    /**
     * 校验验证码是否相同
     */
    @RequestMapping("/checkCode")
    @ResponseBody
    public String checkCode(String inputCode, HttpServletRequest request) {
        // 获取session中的验证码
        String code = (String) request.getSession().getAttribute("code");
        System.out.println("code=" + code);
        System.out.println("inputCode=" + inputCode);

        // 获取时间戳
        long codeEndTime = System.currentTimeMillis();
        // 从session中获取验证码生成时间
        long codeStartTime = (long) request.getSession().getAttribute("codeStartTime");

        // 验证时间是否超时
        if ((codeEndTime - codeStartTime) > 60000) {
            System.out.println("验证码已超时！");
            return "-1"; // 返回值-1表示验证码超时
        }

        if (code != null) {
            System.out.println("验证码是否相同：" + code.equals(inputCode));
            // 比较验证码是否一致
            if (code.equals(inputCode)) {
                return "1";
            }
        }
        return "";
    }
    /***
     * 首页
     */
    @RequestMapping("/chatindex")
    public String chatindex(HttpServletRequest request) {
        User user1 = (User) request.getSession().getAttribute("loginUser");
        if (user1 != null) {
            //人气会员列表
            List<IndexUser> list = userService.getPupoList();
            //根据登录用户性别推荐会员
            List<IndexUser> list1 = null;
            if (user1.getUserSex().equals("男")) {
                list1 = userService.getFlashList("女");
            }
            if (user1.getUserSex().equals("女")) {
                list1 = userService.getFlashList("男");
            }
            request.setAttribute("firstUserList", list);
            request.setAttribute("falshUserList", list1);
            return "index";
        }
        return "login";
    }
    /**
     * 登录功能
     */
    @RequestMapping("/login")
    public String login(User user, String remPs, HttpServletRequest request, HttpServletResponse response){
        if (user != null) {
            // 根据账号查询用户密码，然后解密，跟表单提交的比较
            User loginUser = userService.selectOne(
                    new EntityWrapper<User>()
                            .eq("user_account", user.getUserAccount())
            );
            String convertPs = "";
            if (loginUser != null) {
                // 登录用户密码进行解密
                convertPs = MD5Util.convertMD5(loginUser.getUserPassword());
            }
            // 获取登录用户id
            String loginId = userService.getUserIdByAccount(user.getUserAccount());

            if (convertPs.equals(user.getUserPassword())) {// 如果解密的密码和表单的密码一致，登录成功
                //修改登录时间
                User updateUser = new User();
                updateUser.setUserId(Integer.parseInt(loginId));
                updateUser.setUserLogintime(new Date());
                boolean b = userService.updateById(updateUser);//根据id修改登录时间

                // 获取在线用户列表
                List<User> onlineList = (List<User>) request.getServletContext().getAttribute("onlineList");
                // 查询到用户将用户存入session中
                request.getSession().setAttribute("loginUser", loginUser);
                // 将登录用户放入在线列表中
                if (onlineList == null) {// 第一次登录
                    onlineList = new ArrayList<>();
                }
                // 1.先要判断集合中是否已经将有该用户，没有则存入集合
                boolean a = true;
                for (User user1 : onlineList) {
                    if (user1.getUserId()==loginUser.getUserId()) {
                        System.out.println(user1.getUserId()+"-----------"+loginUser.getUserId());
                        a = false;
                        break;
                    }
                }
                if (a == true){
                    System.out.println("555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555");
                    onlineList.add(loginUser);
                    MessageSockt messageSockt = new MessageSockt();
                    Message message = new Message(0,"推送",commonDate.getTimeString(),"你的好友"+loginUser.getUserRealname()+"已上线",loginUser.getUserId(),0);
                    String s = gson.toJson(message);
                    messageSockt.sendToUser(s);
                }
                // 将集合存入context
                request.getServletContext().setAttribute("onlineList", onlineList);
                //是否记住密码
                if ("y".equals(remPs)) {// 记住密码
                    // 将账号密码存入cookie
                    Cookie accCookie = new Cookie("accCookie", user.getUserAccount());
                    Cookie psCookie = new Cookie("psCookie", user.getUserPassword());

                    // 设置cookie存活时间1天：秒
                    accCookie.setMaxAge(60 * 60 * 24);
                    psCookie.setMaxAge(60 * 60 * 24);

                    //将cooike响应给浏览器
                    response.addCookie(accCookie);
                    response.addCookie(psCookie);
                } else {//取消记住密码
                    // 删除cookie中保存的账号密码
                    Cookie accCookie = new Cookie("accCookie", "");
                    Cookie psCookie = new Cookie("psCookie", "");

                    // 设置cookie存活时间1天：秒
                    accCookie.setMaxAge(0);
                    psCookie.setMaxAge(0);

                    //将cooike响应给浏览器
                    response.addCookie(accCookie);
                    response.addCookie(psCookie);
                }
                //人气会员列表
                List<IndexUser> list = userService.getPupoList();
                //根据登录用户性别推荐会员
                List<IndexUser> list1 = null;
                if (loginUser.getUserSex().equals("男")) {
                    list1 = userService.getFlashList("女");
                }
                if (loginUser.getUserSex().equals("女")) {
                    list1 = userService.getFlashList("男");
                }
                //向好友发送上线信息
                UserImage userImage = userImageService.ChatImage(loginUser.getUserId());
                request.setAttribute("firstUserList", list);
                request.setAttribute("falshUserList", list1);
                request.getSession().setAttribute("userimage", userImage);
                System.out.println("登录成功");
                return "index"; // 登录成功进入首页，失败返回登录页面
            } else {
                System.out.println("登录失败");
                return "login";
            }
        }
        return "login";
    }

    /**
     *  异步验证用户账号密码
     */
    @RequestMapping("/loginAjax")
    @ResponseBody
    public String loginAjax(User user) {

        //提交数据不为空
        if (user.getUserAccount() != null && user.getUserPassword() != null && !"".equals(user.getUserAccount()) && !"".equals(user.getUserPassword())) {

            // 根据用户名区查找密码
            String password1 = userService.getPasswordByAccount(user.getUserAccount());
            // 解密查询到的明码
            String ps = MD5Util.convertMD5(password1);
            // 判断查询到的密码和用户输入密码是否相同,
            if (ps.equals(user.getUserPassword())) {//相同返回1，不同返回-1
                return "1";
            }
        }

        return "-1";
    }


    /**
     * 校验账号是否存在
     */
    @RequestMapping("/checkAccount")
    @ResponseBody
    public String checkAccount(String account) {
        System.out.println("异步检验的账号：" + account);
        // 通过账号去数据库查询一条数据，返回用户id:1表示账号已注册，-1表示账号不存在
        String id = userService.getUserIdByAccount(account);
        System.out.println("查找到的用户id=" + id);

        if (id != null && !"".equals(id)) {
            return "1";
        }
        return "-1";

    }



    /**
     * 注销登录
     */
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request) {
        // 获取当前用户
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 当前用户移除在线列表
        List<User> onlineList = (List<User>) request.getServletContext().getAttribute("onlineList");
        //销毁session
        request.getSession().invalidate();
        if (onlineList != null) {
            for (int i=0;i<onlineList.size();i++) {
                User aBean= onlineList.get(i);
                if(aBean.getUserId()==loginUser.getUserId()) {
                    onlineList.remove(i);
                    MessageSockt messageSockt = new MessageSockt();
                    Message message = new Message(0,"推送",commonDate.getTimeString(),"你的好友"+loginUser.getUserRealname()+"已下线",loginUser.getUserId(),0);
                    String s = gson.toJson(message);
                    messageSockt.sendToUser(s);
                    break;
                }
            }
        }
        for (User user : onlineList) {
            System.out.println(user.toString());
        }
        return "redirect:/user/login"; // 返回登录页面

    }

    /**
     * 找回密码，向邮箱发送一个找回密码连接
     */
    @RequestMapping("/findPs")
    @ResponseBody
    public String findPs(String regEmail, HttpServletRequest request) {

        if (regEmail != null) {
            String regEx1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

            Pattern p;
            Matcher m;
            p = Pattern.compile(regEx1);
            m = p.matcher(regEmail);

            if (m.matches()) {
                //向注册邮箱发送找回密码链接
                userService.sendFindPsLink(regEmail);

                return "1";
            }
        }
        return "";
    }


    @RequestMapping("/preupdateps")
    public String preUpdatePs(HttpServletRequest request, String uid, String account) {

        // 获取发送连接的时间
        long sendLinkTime = Long.parseLong(uid);
        // 获取当前时间
        long nowTime = System.currentTimeMillis();
        // 获取剩余时间
        long remainTime = nowTime - sendLinkTime;

        //将要修改的账号存入session
        request.getSession().setAttribute("updateAccount", account);

        if (remainTime <= 60 * 1000) {// 1分钟内有效，跳转修改密码界面
            return "updatepassword";
        } else {// 发送连接到点击链接超过1分钟，返回登录页面
            return "login";
        }
    }

    /**
     * 快速匹配用户
     */
    @RequestMapping("/userInformation")
    public String userInformation(HttpServletRequest request,String match){
        TreeSet<UserInformation> userSexList =(TreeSet<UserInformation>) request.getSession().getAttribute("userMatchSexList");
        if("notSatisfied".equals(match)){
            System.out.println("换一个");
            if (userSexList==null||userSexList.size()<=0){
                userSexList = userService.getUserSexList(request);
            }
        }else {
            System.out.println("首页上点进来");
            userSexList = userService.getUserSexList(request);
        }
        Map<String, List<String>> userInteresMap = userInterestService.getUserInteresMap(userSexList.first().getUser().getUserId());
        UserInformation userInformation = userSexList.pollFirst();
        request.setAttribute("userInterestMap",userInteresMap);//用户爱好
        request.setAttribute("userInformation",userInformation);//用户信息及择偶意向信息跟当前登录用户匹配度
        request.getSession().setAttribute("userMatchSexList",userSexList);//当前匹配到的所有异性用户信息
        return "matching";
    }
    @RequestMapping("/updatePs")
    @ResponseBody
    public String updatePs(User user) {

        //通过账号区查询id
        String id = userService.getUserIdByAccount(user.getUserAccount());
        if (id != null && !"".equals(id)) {//有此用户
            // 密码加密
            String convertPs = MD5Util.convertMD5(user.getUserPassword());
            //创建对象封装修改条件
            User updateUser = new User();
            updateUser.setUserId(Integer.parseInt(id));//封装修改的用户id
            updateUser.setUserPassword(convertPs);//封装加密后的密码
            // 修改密码
            boolean b = userService.update(updateUser,
                    new EntityWrapper<User>()
                            .eq("user_id", updateUser.getUserId())
            );
            System.out.println("b=" + b);
            //返回值
            if (b) {
                return "1";
            } else {
                return "-1";
            }
        }
        return "";
    }

    /**
     * 搜索条件查询用户
     */
    @RequestMapping("/firstUserList")
    public String firstUserList(String cha,UserPage userPage,Integer age1,Integer age2,String city,String province,Integer height1,Integer height2, HttpServletRequest request){
        System.out.println(cha+"0---0");
        if("cha".equals(cha)){
            List<User> list = userService.getUserListByUsers(userPage, age1, age2, city, province,height1,height2,request);
            request.setAttribute("firstUserList",list);
            userPage.setNowPage(1);
            System.out.println(userPage.getAge1()+"===2"+userPage.getAge2()+"==h1"+userPage.getHeight1()+"===h2"+userPage.getHeight2());
            request.getSession().setAttribute("firstUserCondition",userPage);
            System.out.println(userPage.getInCome()+"qian");
            if (userPage.getFromplace()!=null){
                String[]  formp1 = userPage.getFromplace().split(",");
                List formp=Arrays.asList(formp1);
                request.setAttribute("sheng",formp.get(0));
                if(formp.size()>1){
                    request.setAttribute("shi",formp.get(1));
                }
            }
        }else {
            UserPage userPage1=new UserPage();
            userPage1.setMarry("不限");
            userPage1.setInCome("0-0");
            List<User> list = userService.getUserListByUsers(userPage1, 17, 70, "地级市", "省份", 0, 210, request);
            request.setAttribute("firstUserList",list);
            request.getSession().setAttribute("firstUserCondition",userPage1);
        }
        return "search";
    }

    /**
     *
     * 搜索翻页
     */
    @RequestMapping("/firstPageChange")
    public  String firstPageChange (Integer nowPage,HttpServletRequest request){
        UserPage userPage= (UserPage) request.getSession().getAttribute("firstUserCondition");
        System.out.println("页数是："+nowPage);
        if(nowPage<=1){
            userPage.setNowPage(1);
        }
        if (nowPage>=userPage.getTotalPage()) {
            userPage.setNowPage(userPage.getTotalPage());
        }
        List<User> list = userService.getUserListChange(userPage);
        request.setAttribute("firstUserList",list);
        request.getSession().setAttribute("firstUserCondition",userPage);
        return "search";
    }

}

