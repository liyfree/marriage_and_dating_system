package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.LeisureStyle;
import com.rimi.bean.LifeStyle;
import com.rimi.bean.User;
import com.rimi.bean.UserInterest;
import com.rimi.service.LeisureStyleService;
import com.rimi.service.UserInterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/leisureStyle")
public class LeisureStyleController {

    @Autowired
    private LeisureStyleService leisureStyleService;
    @Autowired
    private UserInterestService userInterestService;

    /**
     * 获取休闲方式的列表
     * @param request
     * @return
     */
    @RequestMapping("getLeisureList")
    public String getLeiSureList(HttpServletRequest request) {
        //获取登录用户
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 获取用户的兴趣爱好对象
        UserInterest userInterest = userInterestService.selectOne(new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 判断对象是否存在，不存在则添加
        if (userInterest == null) {
            userInterestService.insertUserInterest(loginUser,userInterest );
        }
        // 将用户的生活方式字段进行截取成数组
        String[] userIeisure = userInterestService.getUserIeisure(userInterest.getUserIeisure());
        // 获取数据库兴趣列表
        List<LeisureStyle> userIeisures = leisureStyleService.selectList(null);
        // 将选中的数据添加选中属性
        if (userIeisure != null) {
            for (LeisureStyle style : userIeisures) {
                for (String s : userIeisure) {
                    if ( !"".equals(s) && style.getId() == Integer.parseInt(s)){
                        style.setChecked("checked");
                    }
                }
            }
        }
        // 将修改属性后的列表传到前端页面
        request.setAttribute("leisureList",userIeisures );
        return "leisure";
    }


    @RequestMapping("/addLeiSureStyle")
    public String addLeiSureStyle(HttpServletRequest request,@RequestParam(value = "leisure", required = false) String[] leisure){
        // 将数组转化为数据表字段类型
        String leisureStr =leisureStyleService.getLeisureStyleStr(leisure);
        // 将字段添加到数据表中
        boolean updateleisureStr = userInterestService.updateIeisure(leisureStr, (User) request.getSession().getAttribute("loginUser"));
        System.out.println(updateleisureStr);
        return "redirect:/leisureStyle/getLeisureList";




    }


}

