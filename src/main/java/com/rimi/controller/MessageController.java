package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.google.gson.Gson;
import com.rimi.bean.*;
import com.rimi.service.MessageService;
import com.rimi.service.UserImageService;
import com.rimi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

   @Autowired
   private UserImageService userImageService;
    private Gson gson = new Gson();
    /***
     * 根据好友信息打开聊天框
     * @return user 好友对象
     */
    @RequestMapping("/ChatIndex")
    public void ChatIndex(String userid, HttpServletResponse response){
        User user = userService.selectById(userid);
        UserImage userImage = userImageService.ChatImage(user.getUserId());
        UserandImage userandImage = new UserandImage();
        userandImage.setUserid(user.getUserId());
        userandImage.setUsername(user.getUserRealname());
        userandImage.setImageurl(userImage.getImageUrl());
        String  s = gson.toJson(userandImage);
        response.setContentType("text/html;charset=UTF-8");
        try {
            response.getWriter().print(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RequestMapping("/ChatChat")
    public String ChatChat(){
        return "chat";
    }
    /**
     * 查询消息记录
     * @return 返回list
     */
    @RequestMapping("/MessageList")
    public void MessageList(String userid,HttpServletResponse response){
        String[] split = userid.split(",");
        String s = split[0];
        String s1 = split[1];
        String satrt = split[2];
        Message message = new Message();
        message.setMsgSendId(Integer.parseInt(s));
        message.setMsgReciveId(Integer.parseInt(s1));
        Integer integer = messageService.selectCount(message);
        double pagec = integer/10;
        Integer ceil = (int)Math.ceil(pagec)+1;
        Integer page = satrt == null ? 1: Integer.parseInt(satrt);
        if (page > ceil){
            page = ceil;
        }
        if (page <=0){
            page = 1;
        }
        CommonDate commonDate = new CommonDate();
        String day = commonDate.getDay(3);
        boolean msg_send_time = messageService.delete(new EntityWrapper<Message>().between("msg_send_time","0000-00-00 00:00:00",day));
        if (msg_send_time==true){
            //根据双方id值获取对应的聊天记录
            Page<Message> messagePage = messageService.selectPage(new Page<Message>(page, 10), new EntityWrapper<Message>()
                    .eq("msg_send_id", s)
                    .or("msg_send_id", s1));
            List<Message> records = messagePage.getRecords();
            List<UserAndMessage> lists = new ArrayList<>();
            for (Message message1 : records) {
                User user = userService.selectById(message1.getMsgSendId());
                UserImage userImage = userImageService.ChatImage(message1.getMsgSendId());
                UserAndMessage userAndMessage = new UserAndMessage();
                userAndMessage.setUsername(user.getUserRealname());
                userAndMessage.setImageurl(userImage.getImageUrl());
                userAndMessage.setMsgContext(message1.getMsgContext());
                userAndMessage.setMsgSendTime(message1.getMsgSendTime());
                userAndMessage.setMsgSendId(message1.getMsgSendId());
                userAndMessage.setPage(page);
                userAndMessage.setMsgReciveId(message1.getMsgReciveId());
                lists.add(userAndMessage);
            }
            response.setCharacterEncoding("UTF-8");
            String s3 = gson.toJson(lists);
            try {
                //将聊天记录返回到请求的页面中
                response.getWriter().write(s3);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

