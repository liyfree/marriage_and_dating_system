package com.rimi.controller;

import com.rimi.bean.Popularity;
import com.rimi.bean.User;
import com.rimi.service.PopularityService;
import com.rimi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/popularity")
public class PopularityController {
    @Autowired
    private PopularityService popularityService;

    @Autowired
    private UserService userService;
    /***
     * 获取人气排行前12的用户id
     * @return userid集合
     */
    @RequestMapping("/PopuUserList")
    public String PopularityList(){
        List<Popularity> popularities = popularityService.ChatPopList();
        List<User> list = new ArrayList<>();
        for (Popularity popularity : popularities) {
            User user = userService.selectById(popularity.getUserId());
            list.add(user);
        }
        return "index";
    }

}

