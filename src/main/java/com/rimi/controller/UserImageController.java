package com.rimi.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.User;
import com.rimi.bean.UserImage;
import com.rimi.service.UserImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Controller
@RequestMapping("/userImage")
public class UserImageController {

    @Autowired
    private UserImageService userImageService;

    /**
     * 跳转显示用户图片页面
     *
     * @param request
     * @return
     */
    @RequestMapping("/imageShow")
    public String getIntoImageShow(HttpServletRequest request) {
        // 获取session 中的userId
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 根据userId 获取Image对象列表
        List<UserImage> userImageList = userImageService.selectList(new EntityWrapper<UserImage>().eq("user_id", loginUser.getUserId()));
        System.out.println(userImageList.size());
        // 将列表用request传递到前端页面
        if (userImageList != null && userImageList.size() > 0) {
            request.setAttribute("userImageList", userImageList);
        }
        UserImage userImage = userImageList.get(0);
        // 跳转到页面
        return "imageshow";

    }


    /**
     * 添加用户的照片
     *
     * @param fileName 上传的文件
     * @param request  获取文件的存储地址
     * @param userId   用户id
     * @return
     */
    @RequestMapping("/addShowImage")
    public String showImage(@RequestParam MultipartFile fileName, HttpServletRequest request, String userId) {
        // 上传文件后获取文件的存储地址
        String fileUrl = userImageService.addUserImageUrl(request, userId, fileName);
        // 创建一个新的userImage对象，并将属性添加上去
        UserImage userImage = new UserImage();
        userImage.setUserId(Integer.parseInt(userId));
        userImage.setImageUrl(fileUrl);
        userImage.setImageId(0);
        // 将对象添加到数据表中
        boolean insert = userImageService.insert(userImage);
        // 判断是否上传成功
        System.out.println("文件是否上传成功：" + insert);
        // 上传成功，则将默认的图片地址从数据库中删除
        if (insert == true){
            // 获取图片对象
            List<UserImage> userImageList = userImageService.selectList(new EntityWrapper<UserImage>().eq("user_id", userId));
            // 遍历
            for (UserImage image : userImageList) {
                if ("/images/tou.png".equals(image.getImageUrl())){
                    userImageService.delete(new EntityWrapper<UserImage>().eq("image_id", image.getImageId()));
                }
            }
        }
        return "redirect:/userImage/imageShow";
    }


    /**
     * 获取用户图片列表并显示出来
     * @param request
     * @return 显示所有照片列表
     *
     */
    @RequestMapping("/getImageList")
    public String download(HttpServletRequest request){
        // 获取登录用户
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        // 根据用户id获取url地址
        List<UserImage> userImageList = userImageService.selectList(new EntityWrapper<UserImage>().eq("user_id", loginUser.getUserId()));
        // 将图片列表传到前端列表
        request.setAttribute("userImageList", userImageList);
       // 跳转页面
        return "updateimage";
    }


    /**
     * 删除对应图片
     * @param request
     * @param imageId 图片对象id
     * @return
     */
    @RequestMapping("/deleteImage")
    public String deleteImage(HttpServletRequest request,String imageId){
        // 根据imageId 获取图片对象
        UserImage userImage = userImageService.selectOne(new EntityWrapper<UserImage>().eq("image_id", imageId));
        // 获取要删除的对象地址
        String path = request.getServletContext().getRealPath(userImage.getImageUrl());
        System.out.println(path);
        // 获取文件
        File file = new File(path);
        // 删除文件
        file.delete();
        // 删除image对象
        boolean b = userImageService.delete(new EntityWrapper<UserImage>().eq("image_id", imageId));;
        System.out.println(b);
        return "redirect:/userImage/getImageList";
    }


}

