package com.rimi.comparator;

import com.rimi.bean.UserInformation;

import java.util.Comparator;

public class comparatorByMatching implements Comparator<UserInformation> {

    @Override
    public int compare(UserInformation o1, UserInformation o2) {
        return o2.getMatchingIndex()-o1.getMatchingIndex();
    }

}
