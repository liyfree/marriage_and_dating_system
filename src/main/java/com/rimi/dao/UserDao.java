package com.rimi.dao;

import com.rimi.bean.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.rimi.bean.UserInformation;
import com.rimi.bean.UserPage;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface UserDao extends BaseMapper<User> {

    /**
     * 通过用户账号查询用户id
     */
    String getUserIdByAccount(String account);


    /**
     * 通过用户名查找密码
     */

    String getPasswordByAccount(String account);

    /**
     * @return  根据人气降序获取会员列表
     */

    List<User> getPupoList();

    /***
     * 根据用户性别获取新用户
     */

    List<User> getFlashList(String sex);

    /**
     * @return  根据登录者性别查询异性的所有用户列表
     */
    List<User> getUserSexList(String sex);

    List<User> getUserListByUsers(UserPage userPage);
    /**
     * 修改密码
     */

    int updatePassword(User user);


    int getUserCount(UserPage userPage);

}
