package com.rimi.dao;

import com.rimi.bean.LifeStyle;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface LifeStyleDao extends BaseMapper<LifeStyle> {
}
