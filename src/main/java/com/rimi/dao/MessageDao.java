package com.rimi.dao;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.rimi.bean.Message;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface MessageDao extends BaseMapper<Message> {

    boolean addMessage(Message mess);
    Integer selectCount(Message message);
}
