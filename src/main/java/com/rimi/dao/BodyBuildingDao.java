package com.rimi.dao;

import com.rimi.bean.BodyBuilding;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface BodyBuildingDao extends BaseMapper<BodyBuilding> {

}
