package com.rimi.dao;

import com.rimi.bean.UserFriend;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface UserFriendDao extends BaseMapper<UserFriend> {

    UserFriend UserFriendOne(UserFriend userFriend);

    List<UserFriend> UserFriendHave(Integer UserId);

    boolean addUserFriend(UserFriend userFriend);

    List<UserFriend> UserFriendlist(Integer UserId);

    List<UserFriend> UserFriendChat(Integer userIds);

    boolean updateFriend(UserFriend userFriend);
}
