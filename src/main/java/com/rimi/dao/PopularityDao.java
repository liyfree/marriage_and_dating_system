package com.rimi.dao;

import com.rimi.bean.Popularity;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface PopularityDao extends BaseMapper<Popularity> {

    List<Popularity> ChatPopList();

    boolean updatePopu(Popularity popularity);

}
