package com.rimi.dao;

import com.rimi.bean.UserInterest;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.io.Serializable;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface UserInterestDao extends BaseMapper<UserInterest> {

//    UserInterest getUserInterestById(int id);
}
