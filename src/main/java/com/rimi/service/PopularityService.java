package com.rimi.service;

import com.rimi.bean.Popularity;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface PopularityService extends IService<Popularity> {

    List<Popularity> ChatPopList();

    boolean updatePopu(Popularity popularity);
}
