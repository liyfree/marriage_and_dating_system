package com.rimi.service;


import com.rimi.bean.User;

import java.util.List;

/**
 *  index.jsp页面中人气女会员和近期熟男列表
 */
public interface IndexLeaguerListService {

    /**
     * @return 根据注册时间获取男会员列表
     */
    List<User> getMaleLeaguerList();


    /**
     * @return 根据人气值获取会员列表
     */
    List<User> getPupoList();

    String getUserImageUrl(Integer userId);


}
