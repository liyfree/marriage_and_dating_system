package com.rimi.service.impl;


import com.rimi.bean.User;
import com.rimi.dao.UserDao;
import com.rimi.service.LeaguerPageService;
import com.rimi.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


/**
 * 用户查看会员信息实现类
 */
@Service
public class LeaguerPageServiceImpl implements LeaguerPageService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private DateUtil dateUtil;


    /**
     * @param userid 要查看详细信息的会员id
     * @return 被查看的会员对象
     */
    @Override
    public User getLeaguerInfo(Integer userid) {
        return userDao.selectById(userid);
    }



    /**
     * 根据生日获取年龄
     * @param birthDate 生日
     * @return 年龄
     */
    @Override
    public Integer getAgeByDate(Date birthDate) {
        return dateUtil.getAgeByDate(birthDate);
    }
}
