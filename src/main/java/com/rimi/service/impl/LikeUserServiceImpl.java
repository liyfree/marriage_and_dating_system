package com.rimi.service.impl;

import com.rimi.bean.LikeUser;
import com.rimi.dao.LikeUserDao;
import com.rimi.service.LikeUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class LikeUserServiceImpl extends ServiceImpl<LikeUserDao, LikeUser> implements LikeUserService {

    /**
     * 判断择偶表中是否存值这个对象的择偶条件
     * @param likeUser 通过登录用户的id查询到的择偶条件对象
     * @return 是否存在，false 表示不存在，true 表示存在
     */
    @Override
    public boolean isHava(LikeUser likeUser) {
        boolean isok = false;
        if (likeUser !=null && likeUser.getUserId()!= null && likeUser.getUserId()!= 0){
            isok = true;
        }
        return isok;
    }
}
