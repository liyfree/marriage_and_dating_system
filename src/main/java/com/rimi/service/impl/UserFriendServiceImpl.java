package com.rimi.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.rimi.bean.UserFriend;
import com.rimi.dao.UserFriendDao;
import com.rimi.service.UserFriendService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.rimi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class UserFriendServiceImpl extends ServiceImpl<UserFriendDao, UserFriend> implements UserFriendService {
    @Autowired
    private UserFriendDao userFriendDao;

    @Override
    public UserFriend UserFriendOne(UserFriend userFriend) {
        UserFriend userFriend1 = userFriendDao.UserFriendOne(userFriend);
        return userFriend1;
    }

    @Override
    public List<UserFriend> UserFriendHave(Integer UserId) {
        List<UserFriend> list = userFriendDao.UserFriendHave(UserId);
        return list;
    }

    @Override
    public boolean addUserFriend(UserFriend userFriend) {
        boolean b = userFriendDao.addUserFriend(userFriend);
        return b;
    }


    @Override
    public List<UserFriend> UserFriendlist(Integer UserId) {
        List<UserFriend> userFriends = userFriendDao.UserFriendlist(UserId);
        return userFriends;
    }

    @Override
    public List<UserFriend> UserFriendChat(Integer userIds) {
        List<UserFriend> list = userFriendDao.UserFriendChat(userIds);
        return list;
    }

    @Override
    public boolean updateFriend(UserFriend userFriend) {
        boolean l = userFriendDao.updateFriend(userFriend);
        return l;
    }


}
