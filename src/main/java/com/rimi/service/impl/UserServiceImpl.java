package com.rimi.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.rimi.bean.Email;
import com.rimi.bean.User;
import com.rimi.bean.*;
import com.rimi.comparator.comparatorByMatching;
import com.rimi.dao.LikeUserDao;
import com.rimi.dao.UserDao;
import com.rimi.service.UserImageService;
import com.rimi.service.UserService;
import com.rimi.util.MailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private LikeUserDao likeUserDao;
   @Autowired
   private UserImageService userImageService;
    @Override
    public void test(User user) {
        Integer integer = userDao.updateById(user);
        System.out.println(integer.toString());
    }

    /**
     * 通过用户账号查询用户id
     */
    @Override
    public String getUserIdByAccount(String account) {

        return userDao.getUserIdByAccount(account);
    }

    @Override
    public Integer insertUser(User user) {
        Integer insert = userDao.insert(user);
        return insert;
    }

    @Override
    public String getPasswordByAccount(String account) {
        String password = userDao.getPasswordByAccount(account);
        return password;
    }

    @Override
    public User getUser(User user) {
        return userDao.selectOne(user);
    }
    /***
     * 通过用户id查询用户对象
     * @return  User对象
     */


    @Override
    public void sendCodeToAccount(String code, String account) throws UnsupportedEncodingException, GeneralSecurityException, MessagingException {

        // 创建邮件对象
        Email email = new Email();
        // 设置发送者邮箱
        email.setFromAddress("liyfree@qq.com");
        // 设置接收者邮箱
        email.setToAddress(account);
        // 设置邮件主题
        email.setSubject("缘来是你婚恋网验证码");
        // 设置邮件内容，可以使用html标签
        email.setText("<h3>欢迎您注册缘来是你婚恋网，您本次的注册验证码是：<font color='red'>" + code + "</font>  ,1分钟内有效,请尽快完成注册！</h3>");
        // 设置邮箱授权码，需要去qq邮箱获取
        email.setLicenseCode("rmgcmqfewhxcbdbd");

        MailUtil.sendMail(email);

        System.out.println("邮件发送成功！");


    }

    @Override
    public void sendFindPsLink(String account) {
        // 创建邮件对象
        Email email = new Email();
        // 设置发送者邮箱
        email.setFromAddress("liyfree@qq.com");
        // 设置接收者邮箱01    1qw2123232adfa
        email.setToAddress(account);
        // 设置邮件主题
        email.setSubject("缘来是你婚恋网找回密码地址");
        // 设置邮件内容，可以使用html标签
        email.setText("<h3>点击超链接找回密码，1分钟内有效</h3><a>192.168.0.108:8080/user/preupdateps?uid=" + System.currentTimeMillis() + "&account=" + account + "</a>");
        // 设置邮箱授权码，需要去qq邮箱获取
        email.setLicenseCode("rmgcmqfewhxcbdbd");

        try {
            MailUtil.sendMail(email);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        System.out.println("邮件发送成功！");
    }

    @Override
    public List<IndexUser> getPupoList() {
        List<User> maleList = userDao.getPupoList();
        List<IndexUser> lists = ChuliLisst(maleList);
        return lists;
    }
    @Override
    public List<IndexUser> getFlashList(String sex) {
        List<User> flashList = userDao.getFlashList(sex);
        List<IndexUser> list = ChuliLisst(flashList);
        return list ;
    }
    private List<IndexUser>  ChuliLisst(List<User> list){
        List<IndexUser> lists = new ArrayList<>();
        for (User user1 : list) {
            UserImage userImage = userImageService.ChatImage(user1.getUserId());
            IndexUser indexUser =null;
            if(userImage!=null) {
                indexUser = new IndexUser(user1.getUserId(), user1.getUserBirthday(), user1.getUserHeight(), user1.getUserEnucation(), user1.getUserHouse(), user1.getUserMarry(), userImage.getImageUrl());
            }else {
                indexUser = new IndexUser(user1.getUserId(), user1.getUserBirthday(), user1.getUserHeight(), user1.getUserEnucation(), user1.getUserHouse(), user1.getUserMarry(),"/images/tou.png");
            }
            lists.add(indexUser);
        }
        return lists;
    }


    /**
     * 查找满足登录用户择偶条件9条以上的异性列表
     */
    @Override
    public TreeSet<UserInformation> getUserSexList(HttpServletRequest request) {
        User user=(User)request.getSession().getAttribute("loginUser");
        System.out.println("登录用户：===="+user);
        List<User> userSexList = userDao.getUserSexList(user.getUserSex());//查询所有异性用户信息
        for (User user1 : userSexList) {
            System.out.println(user1);
        }
        LikeUser likeUser=new LikeUser();
        likeUser.setUserId(user.getUserId());
        LikeUser likeUser1 = likeUserDao.selectById(user.getUserId());//获取登陆者对异性的要求
        TreeSet<UserInformation> objects = new TreeSet<>(new comparatorByMatching());
        for (User user1 : userSexList) {//遍历异性用户并且查看与登陆者的要求相似度

            int num = getMatching(likeUser1, user1);
            if (num>=0){//如果满足条件大于0条，就存入集合
                LikeUser likeUser2 = likeUserDao.selectById(user1.getUserId());
                num+= getMatching(likeUser2, user);
                int num2=num*100/38;
                objects.add(new UserInformation(user1,likeUser2,num2));
            }
        }
        return objects;
    }

    /**
     * 比较用户对于另一个用户的择偶匹配度
     */
    @Override
    public int getMatching(LikeUser likeUser,User user){
        System.out.println("拿到的用户==="+user);
        int num=0;
        if(likeUser==null){
            return 0;
        }else if (user==null||likeUser==null){
            return 19;
        }
        String[] strlike={likeUser.getUserMajor(),likeUser.getUserCar(),likeUser.getUserChild(),likeUser.getUserBlood(),likeUser.getUserDrink(),likeUser.getUserEnucation(),
                likeUser.getUserFromplace(),likeUser.getUserHouse(),likeUser.getUserIncome(),likeUser.getUserLocation(),likeUser.getUserMarry(),likeUser.getUserSmoke(),
                likeUser.getUserNation(),likeUser.getUserParent(),likeUser.getUserWangchild(),likeUser.getUserSchool()};

        String [] struser={user.getUserMajor(),user.getUserCar(),user.getUserChild(),user.getUserBlood(),user.getUserDrink(),user.getUserEnucation(),
                user.getUserFromplace(),user.getUserHouse(),user.getUserIncome(),user.getUserLocation(),user.getUserMarry(),user.getUserSmoke(),
                user.getUserNation(),user.getUserParent(),user.getUserWangchild(),user.getUserSchool()};

        for (int i = 0; i < strlike.length; i++) {//判定数组中的条件是否符合要求
            if (strlike[i]!=null&&!"".equals(strlike[i])&&!"不限".equals(strlike[i])){
                if (strlike[i].equals(struser[i])){
                    num+=1;
                }
            }else {
                num+=1;
            }
        }
        if(likeUser.getUserHeight()!=null&&likeUser.getUserHeight()>0){//判定身高是否符合要求
            System.out.println(user+"8888888888");
            System.out.println(likeUser+"==3423424");
            System.out.println(user.getUserHeight()+"=======");
            if(user.getUserHeight()!=null){
                if(user.getUserHeight()>0){
                    if(likeUser.getUserHeight()==user.getUserHeight()){
                        num+=1;
                    }
                }
            }
        }else {
            num+=1;
        } if(likeUser.getUserWeight()!=null&&likeUser.getUserWeight()>0){//判定体重是否符合要求
            if(user.getUserWeight()!=null&&user.getUserWeight()>0){
                if(likeUser.getUserWeight()==user.getUserWeight()){
                    num+=1;
                }
            }
        }else {
            num+=1;
        }
        //判定年龄是否符合要求
        if(likeUser.getUserBirthday()!=null&&!"".equals(likeUser.getUserBirthday())){
            if (user.getUserBirthday()!=null&&!"".equals(likeUser.getUserBirthday())){
                if (likeUser.getUserBirthday().getYear()==user.getUserBirthday().getYear()||user.getUserBirthday().getYear()==0){
                    num+=1;
                }
            }
        }else {
            num+=1;
        }
        return num;
    }

    @Override
    public List<User> getUserListByUsers(UserPage userPage,Integer age1,Integer age2,String city,String province,Integer height1,Integer height2,HttpServletRequest request) {
        UserPage userPage1=userPage;
        userPage1.setHeight1(height1);
        userPage1.setHeight2(height2);
        Calendar cal=Calendar.getInstance();
        int year1=cal.get(Calendar.YEAR);
        userPage1.setAge1((year1-age1)+"-"+cal.get(Calendar.MONTH)+"-"+cal.get(Calendar.DATE));
        userPage1.setAge2((year1-age2)+"-"+cal.get(Calendar.MONTH)+"-"+cal.get(Calendar.DATE));
        System.out.println(userPage1.getAge1()+"====----");
        String location=null;
        if (province!=null&&!"".equals(province)&&!"省份".equals(province)){
            location=province;
            if (city!=null&&!"".equals(city)&&!"地级市".equals(city)){
                location+=","+city;
            }
        }
        userPage1.setFromplace(location);
        User user=(User)request.getSession().getAttribute("loginUser");
        userPage1.setSex(user.getUserSex());
        userPage1.setCount(userDao.getUserCount(userPage1));
        List<User> list = userDao.getUserListByUsers(userPage1);
        return list;
    }

    @Override
    public List<User> getUserListChange(UserPage userPage){
        System.out.println(userPage.getStartPage());
        return userDao.getUserListByUsers(userPage);
    }

}
