package com.rimi.service.impl;

import com.rimi.bean.Popularity;
import com.rimi.dao.PopularityDao;
import com.rimi.service.PopularityService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class PopularityServiceImpl extends ServiceImpl<PopularityDao, Popularity> implements PopularityService {

    @Autowired
    private PopularityDao popularityDao;

    @Override
    public List<Popularity> ChatPopList() {
        List<Popularity> popularities = popularityDao.ChatPopList();
        return popularities;
    }

    @Override
    public boolean updatePopu(Popularity popularity) {
        boolean b = popularityDao.updatePopu(popularity);
        return b;
    }
}
