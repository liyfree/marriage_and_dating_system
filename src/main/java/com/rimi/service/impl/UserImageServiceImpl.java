package com.rimi.service.impl;

import com.rimi.bean.UserImage;
import com.rimi.dao.UserImageDao;
import com.rimi.service.UserImageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class UserImageServiceImpl extends ServiceImpl<UserImageDao, UserImage> implements UserImageService {
    @Autowired
    private UserImageDao userImageDao;

    /**
     * 上传用户图片，并获取存储图片的地址
     * @param request
     * @param userId 用户id ，根据用户id添加一个文件夹
     * @param fileName 上传的图片
     * @return 用户存储图片的地址
     */
    @Override
    public String addUserImageUrl(HttpServletRequest request, String userId, MultipartFile fileName) {
        // 获取存储照片的文件夹
        String filePath = request.getServletContext().getRealPath("userImages/" + userId + "/");
        System.out.println(filePath);

        // 获取文件名
        String name = fileName.getOriginalFilename();
        System.out.println("===============================" + name);
        //获取文件夹
        File file = new File(filePath);
        // 如果文件夹不存在，则创建一个新的文件夹
        if (!file.exists()) {
            file.mkdirs();
        }
        // 在本地创建文件
        try {
            fileName.transferTo(new File(filePath, name));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("创建本地图片失败");
        }
        return "/userImages/" + userId + "/"+name;
    }



    @Override
    public UserImage ChatImage(Integer userId) {
        UserImage userImage = userImageDao.ChatImage(userId);
        return userImage;
    }
}
