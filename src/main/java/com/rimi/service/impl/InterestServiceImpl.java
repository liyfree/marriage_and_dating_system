package com.rimi.service.impl;

import com.rimi.bean.Interest;
import com.rimi.dao.InterestDao;
import com.rimi.service.InterestService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class InterestServiceImpl extends ServiceImpl<InterestDao, Interest> implements InterestService {

}
