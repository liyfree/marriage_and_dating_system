package com.rimi.service.impl;

import com.rimi.bean.LeisureStyle;
import com.rimi.dao.LeisureStyleDao;
import com.rimi.service.LeisureStyleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class LeisureStyleServiceImpl extends ServiceImpl<LeisureStyleDao, LeisureStyle> implements LeisureStyleService {

    @Override
    public String getLeisureStyleStr(String[] leisureStyles) {
        String lleisureStyleStr = "";
        for (String s : leisureStyles) {
            lleisureStyleStr = lleisureStyleStr + "," + s;
        }
        return lleisureStyleStr;
    }
}
