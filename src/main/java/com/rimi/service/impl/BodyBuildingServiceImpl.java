package com.rimi.service.impl;

import com.rimi.bean.BodyBuilding;
import com.rimi.dao.BodyBuildingDao;
import com.rimi.service.BodyBuildingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class BodyBuildingServiceImpl extends ServiceImpl<BodyBuildingDao, BodyBuilding> implements BodyBuildingService {

    /**
     * 将页面传递的复选框数组参数转化为数据表字段
     * @param bodyBuild 页面传的选择数组
     * @return
     */
    @Override
    public String getBodyBuildStr(String[] bodyBuild) {
        String bodyBuildStr = "";
        for (String s : bodyBuild) {
            bodyBuildStr = bodyBuildStr + "," + s;
        }
        return bodyBuildStr;
    }
}
