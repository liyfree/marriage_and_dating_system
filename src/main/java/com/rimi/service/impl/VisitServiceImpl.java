package com.rimi.service.impl;

import com.rimi.bean.Visit;
import com.rimi.dao.VisitDao;
import com.rimi.service.VisitService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class VisitServiceImpl extends ServiceImpl<VisitDao, Visit> implements VisitService {
    @Autowired
    private VisitDao visitDao;

    @Override
    public boolean AddVisit(Visit visit) {
        boolean b = visitDao.AddVisit(visit);
        return b;
    }


    @Override
    public boolean UpdateVisit(Visit visit) {
        boolean b = visitDao.UpdateVisit(visit);
        return b;
    }
}
