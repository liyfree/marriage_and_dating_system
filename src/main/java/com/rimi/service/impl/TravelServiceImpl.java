package com.rimi.service.impl;

import com.rimi.bean.Travel;
import com.rimi.dao.TravelDao;
import com.rimi.service.TravelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class TravelServiceImpl extends ServiceImpl<TravelDao, Travel> implements TravelService {

    @Override
    public String getTravelByStr(String[] travelBy) {
        String travelByStr = "";
        for (String s : travelBy) {
            travelByStr = travelByStr + "," + s;
        }
        return travelByStr;
    }
}
