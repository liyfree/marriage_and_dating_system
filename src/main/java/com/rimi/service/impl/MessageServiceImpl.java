package com.rimi.service.impl;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.rimi.bean.Message;
import com.rimi.dao.MessageDao;
import com.rimi.service.MessageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageDao, Message> implements MessageService {

    @Autowired
    private MessageDao messageDao;

    @Override
    public boolean addMessage(Message mess) {
        boolean messs = messageDao.addMessage(mess);
        return messs;
    }

    @Override
    public Integer selectCount(Message message) {
        Integer integer = messageDao.selectCount(message);
        return integer;
    }

}
