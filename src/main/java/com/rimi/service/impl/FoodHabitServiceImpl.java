package com.rimi.service.impl;

import com.rimi.bean.FoodHabit;
import com.rimi.dao.FoodHabitDao;
import com.rimi.service.FoodHabitService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class FoodHabitServiceImpl extends ServiceImpl<FoodHabitDao, FoodHabit> implements FoodHabitService {

    @Override
    public String getFoodHobby(String[] foodHobby) {
        String foodHobbyStr = "";
        for (String s : foodHobby) {
            foodHobbyStr = foodHobbyStr + "," + s;
        }
        return foodHobbyStr;
    }
}
