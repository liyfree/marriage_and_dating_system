package com.rimi.service.impl;

import com.rimi.bean.Pet;
import com.rimi.dao.PetDao;
import com.rimi.service.PetService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class PetServiceImpl extends ServiceImpl<PetDao, Pet> implements PetService {

    @Override
    public String getUserPetHobby(String[] petHobbys) {
        String petHobbyStr = "";
        for (String s : petHobbys) {
            petHobbyStr = petHobbyStr + "," + s;
        }
        return petHobbyStr;

    }
}
