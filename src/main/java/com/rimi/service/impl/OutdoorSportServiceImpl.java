package com.rimi.service.impl;

import com.rimi.bean.OutdoorSport;
import com.rimi.dao.OutdoorSportDao;
import com.rimi.service.OutdoorSportService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class OutdoorSportServiceImpl extends ServiceImpl<OutdoorSportDao, OutdoorSport> implements OutdoorSportService {

    /**
     * 将页面传递的复选框数组参数转化为数据表字段
     * @param outdoorSports 页面传的选择数组
     * @return
     */
    @Override
    public String getOutdoorSportStr(String[] outdoorSports) {
        String outdoorSportStr = "";
        for (String s : outdoorSports) {
            outdoorSportStr = outdoorSportStr + "," + s;
        }
        return outdoorSportStr;
    }
}
