package com.rimi.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rimi.bean.*;
import com.rimi.dao.UserInterestDao;

import com.rimi.dao.*;
import com.rimi.service.UserInterestService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service()
public class UserInterestServiceImpl extends ServiceImpl<UserInterestDao, UserInterest> implements UserInterestService {
    @Autowired
    private UserInterestDao userInterestDao;
    @Autowired
    private LifeStyleDao lifeStyleDao;
    @Autowired
    private BodyBuildingDao bodyBuildingDao;
    @Autowired
    private FoodHabitDao foodHabitDao;
    @Autowired
    private TravelDao travelDao;
    @Autowired
    private PetDao petDao;
    @Autowired
    private LeisureStyleDao leisureStyleDao;
    @Autowired
    private OutdoorSportDao outdoorSportDao;


    /**
     *
     * @param userId 用户ID
     * @return 返回一个用户爱好map，key=爱好对应的大类型，value=具体爱好的集合
     */
    @Override
    public Map<String,List<String>> getUserInteresMap(Integer userId) {
        Map<String,List<String>> map=new TreeMap<>();
        UserInterest userIn=new UserInterest();
        userIn.setUserId(userId);
        UserInterest userInterest = userInterestDao.selectOne(userIn);
        System.out.println(userInterest+"看看是不是空");
        if (userInterest!=null){
            String [] str={
                    userInterest.getUserLifestyle(),//生活方式
                    userInterest.getUserBodybuliding(),//运动健身
                    userInterest.getUserFood(),//饮食习惯
                    userInterest.getUserIeisure(),//休闲方式
                    userInterest.getUserOutdoor(),//户外运动
                    userInterest.getUserPet(),//宠物
                    userInterest.getUserTravel(),//旅行方式
            };
            String [] str1={"生活方式","运动健身","饮食习惯","休闲方式","户外运动","喜爱宠物","旅行方式"};
            for (int i = 0; i <str.length ; i++) {
                String[] split =null;
                if (str[i]!=null&&!"".equals(str[i])){
                    split = str[i].split(",");
                    List<String> strings = Arrays.asList(split);
                    if (strings!=null){
                        ArrayList<String> list=new ArrayList<>();
                        list.clear();
                        switch (i){
                            case 0:
                                List<LifeStyle> lifeStyles = lifeStyleDao.selectBatchIds(strings);
                                for (LifeStyle lifeStyle : lifeStyles) {
                                    list.add(lifeStyle.getLifeName());
                                }
                                break;
                            case 1:
                                List<BodyBuilding> bodyBuildings = bodyBuildingDao.selectBatchIds(strings);
                                for (BodyBuilding bodyBuilding : bodyBuildings) {
                                    list.add(bodyBuilding.getName());
                                }
                                break;
                            case 2:
                                List<FoodHabit> foodHabits = foodHabitDao.selectBatchIds(strings);
                                for (FoodHabit foodHabit : foodHabits) {
                                    list.add(foodHabit.getFoodName());
                                }
                                break;
                            case 3:
                                List<LeisureStyle> leisureStyles = leisureStyleDao.selectBatchIds(strings);
                                for (LeisureStyle leisureStyle : leisureStyles) {
                                    list.add(leisureStyle.getLeisureName());
                                }
                                break;
                            case 4:
                                List<OutdoorSport> outdoorSports = outdoorSportDao.selectBatchIds(strings);
                                for (OutdoorSport outdoorSport : outdoorSports) {
                                    list.add(outdoorSport.getOutdoorName());
                                }
                                break;
                            case 5:
                                List<Pet> pets = petDao.selectBatchIds(strings);
                                for (Pet pet : pets) {
                                    list.add(pet.getPetName());
                                }
                                break;
                            case 6:
                                List<Travel> travels = travelDao.selectBatchIds(strings);
                                for (Travel travel : travels) {
                                    list.add(travel.getTravelName());
                                }
                                break;
                        }
                        map.put(str1[i],list);
                    }
                }
            }

        }

        return map;
    }



    /**
     * 根据用户id 获取用户的兴趣对象
     *
     * @param loginUser 登录用户
     */
    @Override
    public void insertUserInterest(User loginUser, UserInterest userInterest) {
        userInterest = new UserInterest();
//        userInterest.setUserId(loginUser.getUserId());
        userInterest.setId(0);
        userInterestDao.insert(userInterest);
    }

    /**
     * @param userLifeStyle 用户生活方式字段
     * @return
     */
    @Override
    public String[] getLifeStyle(String userLifeStyle) {
        if (userLifeStyle != null) {
            return userLifeStyle.split(",");
        }else {
            return  null;
        }
    }

    /**
     * 将选择的生活方式字段添加到数据库中
     *
     * @param lifeStyle 生活方式字段
     * @param loginUser 登录用户
     * @return 是否修改成功
     */
    @Override
    public boolean updateLifeStyle(String lifeStyle, User loginUser) {
        // 创建一个兴趣对象
        UserInterest userInterest = new UserInterest();
        // 给对象赋值
//        userInterest.setUserId(loginUser.getUserId());
        userInterest.setUserLifestyle(lifeStyle);
        // 将数据添加到数据库中
        Integer update = userInterestDao.update(userInterest, new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 定义返回值
        boolean isok = false;
        if (update > 0) {// 修改行数大于0
            isok = true;
        }
        return isok;
    }

    /**
     * @param userBodybuliding 用户运动健身字段
     * @return
     */
    @Override
    public String[] getUserBodybuliding(String userBodybuliding) {
        if (userBodybuliding != null){
            return userBodybuliding.split(",");
        }else {
            return null;
        }
    }

    /**
     * 修改用户运动健身字段
     *
     * @param userBodybuliding 运动健身字段
     * @param loginUser        登录用户
     * @return
     */
    @Override
    public boolean updateUserBodybuliding(String userBodybuliding, User loginUser) {
        // 创建一个兴趣对象
        UserInterest userInterest = new UserInterest();
        // 给对象赋值
//        userInterest.setUserId(loginUser.getUserId());
        userInterest.setUserBodybuliding(userBodybuliding);
        // 将数据添加到数据库中
        Integer update = userInterestDao.update(userInterest, new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 定义返回值
        boolean isok = false;
        if (update > 0) {// 修改行数大于0
            isok = true;
        }
        return isok;
    }

    /**
     * @param userFood 用户饮食习惯字段
     * @return
     */
    @Override
    public String[] getUserFood(String userFood) {
        if (userFood != null){
            return userFood.split(",");
        }else {
            return null;
        }

    }

    /**
     * 修改用户饮食爱好
     *
     * @param userFood  饮食爱好
     * @param loginUser 登录用户
     * @return
     */
    @Override
    public boolean updateUserFood(String userFood, User loginUser) {
        // 创建一个兴趣对象
        UserInterest userInterest = new UserInterest();
        // 给对象赋值
//        userInterest.setUserId(loginUser.getUserId());
        userInterest.setUserFood(userFood);
        // 将数据添加到数据库中
        Integer update = userInterestDao.update(userInterest, new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 定义返回值
        boolean isok = false;
        if (update > 0) {// 修改行数大于0
            isok = true;
        }
        return isok;
    }

    /**
     * @param userIeisure 用户休闲方式字段
     * @return
     */
    @Override
    public String[] getUserIeisure(String userIeisure) {
        if (userIeisure != null){
            return userIeisure.split(",");
        }else {
            return null;
        }

    }

    /**
     * 修改用户休闲方式
     *
     * @param userIeisure 休闲方式
     * @param loginUser   登录用户
     * @return
     */
    @Override
    public boolean updateIeisure(String userIeisure, User loginUser) {
        // 创建一个兴趣对象
        UserInterest userInterest = new UserInterest();
        // 给对象赋值
//        userInterest.setUserId(loginUser.getUserId());
        userInterest.setUserIeisure(userIeisure);
        // 将数据添加到数据库中
        Integer update = userInterestDao.update(userInterest, new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 定义返回值
        boolean isok = false;
        if (update > 0) {// 修改行数大于0
            isok = true;
        }
        return isok;
    }

    /**
     * @param userOutdoor 用户户外运动字段
     * @return
     */
    @Override
    public String[] getUserOutdoor(String userOutdoor) {
        if (userOutdoor != null){
            return userOutdoor.split(",");
        }else {
            return null;
        }
    }


    /**
     * 修改用户户外运动方式
     *
     * @param userOutdoor 户外运动
     * @param loginUser   登录用户
     * @return
     */
    @Override
    public boolean updateOutdoor(String userOutdoor, User loginUser) {
        // 创建一个兴趣对象
        UserInterest userInterest = new UserInterest();
        // 给对象赋值
//        userInterest.setUserId(loginUser.getUserId());
        userInterest.setUserOutdoor(userOutdoor);
        // 将数据添加到数据库中
        Integer update = userInterestDao.update(userInterest, new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 定义返回值
        boolean isok = false;
        if (update > 0) {// 修改行数大于0
            isok = true;
        }
        return isok;
    }


    /**
     * @param userPet 用户宠物爱好字段
     * @return
     */
    @Override
    public String[] getUserPet(String userPet) {
        if (userPet != null){
            return userPet.split(",");
        }else {
            return null;
        }
    }

    /**
     * 修改用户宠物喜好
     *
     * @param userPet   爱好宠物
     * @param loginUser 登录用户
     * @return
     */
    @Override
    public boolean updateUserPet(String userPet, User loginUser) {
        // 创建一个兴趣对象
        UserInterest userInterest = new UserInterest();
        // 给对象赋值
//        userInterest.setUserId(loginUser.getUserId());
        userInterest.setUserPet(userPet);
        // 将数据添加到数据库中
        Integer update = userInterestDao.update(userInterest, new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 定义返回值
        boolean isok = false;
        if (update > 0) {// 修改行数大于0
            isok = true;
        }
        return isok;
    }

    /**
     * @param userTravel 用户旅行方式字段
     * @return
     */
    @Override
    public String[] getUserTravel(String userTravel) {
        if (userTravel != null){
            return userTravel.split(",");
        }else {
            return null;
        }
    }

    /**
     * 修改旅行方式
     *
     * @param userTravel 旅行方式
     * @param loginUser  登录用户
     * @return
     */
    @Override
    public boolean updateUserTravel(String userTravel, User loginUser) {

        // 创建一个兴趣对象
        UserInterest userInterest = new UserInterest();
        // 给对象赋值
//        userInterest.setUserId(loginUser.getUserId());
        userInterest.setUserTravel(userTravel);
        // 将数据添加到数据库中
        Integer update = userInterestDao.update(userInterest, new EntityWrapper<UserInterest>().eq("user_id", loginUser.getUserId()));
        // 定义返回值
        boolean isok = false;
        if (update > 0) {// 修改行数大于0
            isok = true;
        }
        return isok;

    }


}
