package com.rimi.service.impl;

import com.rimi.bean.User;
import com.rimi.bean.UserAddress;
import com.rimi.util.DateUtil;
import com.rimi.util.UserAddressUtil;
import com.rimi.dao.UserDao;
import com.rimi.service.UserPersonalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserPersonalServiceImpl implements UserPersonalService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserAddressUtil userAddressDao;
    @Autowired
    private DateUtil dateUtil;


    /**
     * 点击个人中心 或 用户头像进入用户个人中心
     *
     * @param userId 当前登录用户的Id
     * @return 当前登录用户
     */
    @Override
    public User getUserPersonalMsg(Integer userId) {
        return userDao.selectById(userId);
    }


    /**
     * 拼接地址对象，获取地址
     * @param userAddress 页面传递地址对象
     * @return 用户地址字段
     */
    @Override
    public String getAddress(UserAddress userAddress) {
        return userAddressDao.getAddress(userAddress);
    }

    /**
     * 根据字符串获取出生日期
     * @param str 获取的日期的字符串
     * @return 日期
     */
    @Override
    public Date getDateByStr(String str) {
        return dateUtil.getDateByStr(str);
    }



    /**
     * 根据用户地址字段转化为用户地址对象
     * @param address 用户地址字段
     * @return 地址对象
     */
    @Override
    public UserAddress getUserAddress(String address) {
        return userAddressDao.getUserAddress(address);
    }
}
