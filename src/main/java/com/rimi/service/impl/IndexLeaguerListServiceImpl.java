package com.rimi.service.impl;

import com.rimi.bean.User;
import com.rimi.dao.UserDao;
import com.rimi.dao.UserImageDao;
import com.rimi.service.IndexLeaguerListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 获取index.jsp页面中人气女会员和近期熟男列表
 */
@Service
public class IndexLeaguerListServiceImpl implements IndexLeaguerListService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserImageDao userImageDao;


    @Override
    public List<User> getMaleLeaguerList() {
        return null;
    }

    /**
     * @return 根据人气值降序获取会员列表
     */
    @Override
    public List<User> getPupoList() {
        return userDao.getPupoList();
    }


    /**
     * 根据会员的 Id 获取用户的头像信息
     * @param userId 要查看的会员的Id
     * @return 会员头像信息的地址
     */
    public String getUserImageUrl(Integer userId){
        return userImageDao.selectById(userId).getImageUrl();
    }

}
