package com.rimi.service.impl;

import com.rimi.bean.LifeStyle;
import com.rimi.dao.LifeStyleDao;
import com.rimi.service.LifeStyleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class LifeStyleServiceImpl extends ServiceImpl<LifeStyleDao, LifeStyle> implements LifeStyleService {

    /**
     * @param lifeStyles 页面传的选择数组
     * @return
     */
    @Override
    public String getLifeStyleStr(String[] lifeStyles) {
        String lifeStyleStr = "";
        for (String s : lifeStyles) {
            lifeStyleStr = lifeStyleStr + "," + s;
        }
        return lifeStyleStr;
    }
}
