package com.rimi.service.impl;

import com.rimi.bean.BallSport;
import com.rimi.dao.BallSportDao;
import com.rimi.service.BallSportService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class BallSportServiceImpl extends ServiceImpl<BallSportDao, BallSport> implements BallSportService {

}
