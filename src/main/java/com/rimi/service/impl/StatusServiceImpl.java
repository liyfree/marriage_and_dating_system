package com.rimi.service.impl;

import com.rimi.bean.Status;
import com.rimi.dao.StatusDao;
import com.rimi.service.StatusService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
@Service
public class StatusServiceImpl extends ServiceImpl<StatusDao, Status> implements StatusService {

}
