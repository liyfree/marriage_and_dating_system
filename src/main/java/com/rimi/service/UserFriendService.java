package com.rimi.service;

import com.rimi.bean.UserFriend;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface UserFriendService extends IService<UserFriend> {

    UserFriend UserFriendOne(UserFriend userFriend);

    List<UserFriend> UserFriendHave(Integer UserId);

    boolean addUserFriend(UserFriend userFriend);

    List<UserFriend> UserFriendlist(Integer UserId);

    List<UserFriend> UserFriendChat(Integer userIds);

    boolean updateFriend(UserFriend userFriend);
}
