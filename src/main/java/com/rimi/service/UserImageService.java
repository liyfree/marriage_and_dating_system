package com.rimi.service;

import com.rimi.bean.UserImage;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface UserImageService extends IService<UserImage> {

    /**
     * 上传用户图片，并获取存储图片的地址
     * @param request
     * @param userId
     * @param fileName
     * @return
     */
    String addUserImageUrl(HttpServletRequest request, String userId, @RequestParam MultipartFile fileName);

    UserImage ChatImage(Integer userId);
}
