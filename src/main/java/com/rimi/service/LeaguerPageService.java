package com.rimi.service;


import com.rimi.bean.User;

import java.util.Date;

/**
 * 用户查看会员信息接口
 */
public interface LeaguerPageService {


    /**
     * @param userid 要查看详细信息的会员id
     * @return 根据id获取会员对象
     */
    User getLeaguerInfo(Integer userid);

    /**
     * 根据生日获取年龄
     *
     * @param birthDate 生日
     * @return 年龄
     */
    Integer getAgeByDate(Date birthDate);




}
