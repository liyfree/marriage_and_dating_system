package com.rimi.service;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.rimi.bean.Message;
import com.baomidou.mybatisplus.service.IService;
import com.rimi.bean.User;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface MessageService extends IService<Message> {

    boolean addMessage(Message mess);
    Integer selectCount(Message message);
}
