package com.rimi.service;

import com.rimi.bean.*;
import com.baomidou.mybatisplus.service.IService;
import com.rimi.dao.UserDao;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface UserService extends IService<User> {

    void test(User user);

    /**
     * 通过用户账号查询用户id
     */
    String getUserIdByAccount(String account);


    /**
     * 向数据库插入数据
     */
    Integer insertUser(User user);

    /**
     * 根据站好查找密码
     */
    String getPasswordByAccount(String account);

    /**
     * 根据查询条件查询用户
     */
    User getUser(User user);

    /**
     * 向注册邮箱发送验证码
     */
    void sendCodeToAccount(String code, String account) throws UnsupportedEncodingException, GeneralSecurityException, MessagingException;

    /**
     * 向邮箱发送找回密码连接
     */

    void sendFindPsLink(String account);

    /***
     * 获取人气会员
     * @return
     */
    List<IndexUser> getPupoList();

    /***
     * 根据用户性别推荐新用户
     */
    List<IndexUser> getFlashList(String sex);
    /**
     * 查询满足该用户要求9个以上的所有异性列表
     */
    TreeSet<UserInformation> getUserSexList(HttpServletRequest request);

    /**
     * 查询该用户与另外一个用户的匹配度
     */
    int getMatching(LikeUser likeUser, User user);

    List<User> getUserListByUsers(UserPage userPage,Integer age1,Integer age2,String city,String province,Integer height1,Integer height2,HttpServletRequest request);

    List<User> getUserListChange(UserPage userPage);

}
