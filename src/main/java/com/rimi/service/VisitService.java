package com.rimi.service;

import com.rimi.bean.Visit;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface VisitService extends IService<Visit> {

    boolean AddVisit(Visit visit);
    boolean UpdateVisit(Visit visit);
}
