package com.rimi.service;

import com.rimi.bean.LikeUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface LikeUserService extends IService<LikeUser> {

    /**判断择偶表中是否存值这个对象的择偶条件
     * @param likeUser 通过登录用户的id查询到的择偶条件对象
     * @return 是否存在，false 表示不存在，true 表示存在
     */
    boolean isHava(LikeUser likeUser);

}
