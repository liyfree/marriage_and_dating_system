package com.rimi.service;


import com.rimi.bean.User;
import com.rimi.bean.UserAddress;

import java.util.Date;

/**
 * 用户个人中心基本资料操作
 */
public interface UserPersonalService {

    /**
     * 点击个人中心 或 用户头像进入用户个人中心
     * @param userId 当前登录用户的Id
     * @return 当前登录用户
     */
    User getUserPersonalMsg(Integer userId);


    /**
     * 拼接地址对象，获取地址
     * @param userAddress 页面传递地址对象
     * @return 用户地址字段
     */
    String getAddress(UserAddress userAddress);

    /**
     * 将字符串时间转化为时间类型
     * @param str 字符串类型的时间
     * @return 时间
     */
    Date getDateByStr(String str);


    /**
     * 根据用户地址字段转化为用户地址对象
     * @param address 用户地址字段
     * @return 地址对象
     */
    UserAddress getUserAddress(String address);


}
