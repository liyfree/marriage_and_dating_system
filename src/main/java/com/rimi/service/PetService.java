package com.rimi.service;

import com.rimi.bean.Pet;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface PetService extends IService<Pet> {

    /**
     * 将页面传递的复选框数组参数转化为数据表字段
     * @param petHobbys 页面传的选择数组
     * @return
     */
    String getUserPetHobby(String[] petHobbys);
}
