package com.rimi.service;

import com.rimi.bean.RoleMenu;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface RoleMenuService extends IService<RoleMenu> {

}
