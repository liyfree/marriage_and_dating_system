package com.rimi.service;

import com.rimi.bean.OutdoorSport;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface OutdoorSportService extends IService<OutdoorSport> {

    /**
     * 将页面传递的复选框数组参数转化为数据表字段
     * @param outdoorSports 页面传的选择数组
     * @return
     */
    String getOutdoorSportStr(String[] outdoorSports);


}
