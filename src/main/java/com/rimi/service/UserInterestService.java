package com.rimi.service;

import com.rimi.bean.User;
import com.rimi.bean.UserInterest;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public interface UserInterestService extends IService<UserInterest> {

    /**
     * 根据用户id 获取用户的兴趣对象
     * @param loginUser  登录用户
     * @param userInterest 用户兴趣对象
     */
    void insertUserInterest(User loginUser,UserInterest userInterest);


    /**
     * @param userLifeStyle 用户生活方式字段
     * @return 截取后的数组
     */
    String[] getLifeStyle(String userLifeStyle);


    /**
     * 将选择的生活方式字段添加到数据库中
     *
     * @param lifeStyle 生活方式字段
     * @param loginUser 登录用户
     * @return 是否修改成功
     */
    boolean updateLifeStyle(String lifeStyle, User loginUser);


    /**
     * @param userBodybuliding 用户运动健身字段
     * @return 截取后的数组
     */
    String[] getUserBodybuliding(String userBodybuliding);


    /**
     * 将选择的健身字段添加到数据库中
     *
     * @param userBodybuliding 运动健身字段
     * @param loginUser 登录用户
     * @return 是否修改成功
     */
    boolean updateUserBodybuliding(String userBodybuliding, User loginUser);




    /**
     * @param userFood 用户饮食习惯字段
     * @return 截取后的数组
     */
    String[] getUserFood(String userFood);


    /**
     * 将选择的饮食爱好字段添加到数据库中
     *
     * @param userFood 饮食爱好
     * @param loginUser 登录用户
     * @return 是否修改成功
     */
    boolean updateUserFood(String userFood, User loginUser);

    /**
     * @param userIeisure 用户休闲方式字段
     * @return 截取后的数组
     */
    String[] getUserIeisure(String userIeisure);




    /**
     * 将选择的休闲方式字段添加到数据库中
     *
     * @param userIeisure 休闲方式
     * @param loginUser 登录用户
     * @return 是否修改成功
     */
    boolean updateIeisure(String userIeisure, User loginUser);






    /**
     * @param userOutdoor 用户户外运动字段
     * @return 截取后的数组
     */
    String[] getUserOutdoor(String userOutdoor);



    /**
     * 将选择的户外运动字段添加到数据库中
     *
     * @param userOutdoor 户外运动
     * @param loginUser 登录用户
     * @return 是否修改成功
     */
    boolean updateOutdoor(String userOutdoor, User loginUser);





    /**
     * @param userPet 用户宠物爱好字段
     * @return 截取后的数组
     */
    String[] getUserPet(String userPet);

    /**
     * 将选择的爱好宠物字段添加到数据库中
     *
     * @param userPet 爱好宠物
     * @param loginUser 登录用户
     * @return 是否修改成功
     */
    boolean updateUserPet(String userPet, User loginUser);


    /**
     * @param userTravel 用户旅行方式字段
     * @return 截取后的数组
     */
    String[] getUserTravel(String userTravel);

    /**
     * 将选择的旅行方式字段添加到数据库中
     *
     * @param userTravel 旅行方式
     * @param loginUser 登录用户
     * @return 是否修改成功
     */
    boolean updateUserTravel(String userTravel, User loginUser);



    Map<String,List<String>> getUserInteresMap(Integer userId);
}
