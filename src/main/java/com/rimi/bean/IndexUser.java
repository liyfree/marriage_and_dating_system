package com.rimi.bean;

import java.util.Date;

public class IndexUser {

    public IndexUser() {
    }

    private Integer userId;
    private Date userBirthday;
    private Double userHeight;
    private String userEnucation;
    private String userHouse;
    private String userMarry;
    private String userImage;

    public IndexUser(Integer userId, Date userBirthday, Double userHeight, String userEnucation, String userHouse, String userMarry, String userImage) {
        this.userId = userId;
        this.userBirthday = userBirthday;
        this.userHeight = userHeight;
        this.userEnucation = userEnucation;
        this.userHouse = userHouse;
        this.userMarry = userMarry;
        this.userImage = userImage;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public Double getUserHeight() {
        return userHeight;
    }

    public void setUserHeight(Double userHeight) {
        this.userHeight = userHeight;
    }

    public String getUserEnucation() {
        return userEnucation;
    }

    public void setUserEnucation(String userEnucation) {
        this.userEnucation = userEnucation;
    }

    public String getUserHouse() {
        return userHouse;
    }

    public void setUserHouse(String userHouse) {
        this.userHouse = userHouse;
    }

    public String getUserMarry() {
        return userMarry;
    }

    public void setUserMarry(String userMarry) {
        this.userMarry = userMarry;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

}
