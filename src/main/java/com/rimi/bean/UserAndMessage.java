package com.rimi.bean;

public class UserAndMessage {



    private String msgSendTime;
    private String msgContext;
    private Integer msgSendId;

    private Integer msgReciveId;
    private String username;
    private String imageurl;

    private Integer page;
    public UserAndMessage(String msgSendTime, String msgContext, Integer msgSendId, Integer msgReciveId, String username, String imageurl, Integer page) {
        this.msgSendTime = msgSendTime;
        this.msgContext = msgContext;
        this.msgSendId = msgSendId;
        this.msgReciveId = msgReciveId;
        this.username = username;
        this.imageurl = imageurl;
        this.page = page;
    }

    public Integer getMsgReciveId() {
        return msgReciveId;
    }

    public void setMsgReciveId(Integer msgReciveId) {
        this.msgReciveId = msgReciveId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }



    public String getMsgSendTime() {
        return msgSendTime;
    }

    public void setMsgSendTime(String msgSendTime) {
        this.msgSendTime = msgSendTime;
    }

    public String getMsgContext() {
        return msgContext;
    }

    public void setMsgContext(String msgContext) {
        this.msgContext = msgContext;
    }

    public Integer getMsgSendId() {
        return msgSendId;
    }

    public void setMsgSendId(Integer msgSendId) {
        this.msgSendId = msgSendId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public UserAndMessage() {
    }

}
