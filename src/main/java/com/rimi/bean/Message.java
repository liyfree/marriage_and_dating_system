package com.rimi.bean;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;


    public Message(Integer msgId, String notry, String msgSendTime, String msgContext, Integer msgSendId, Integer msgReciveId) {
        this.msgId = msgId;
        this.notry = notry;
        this.msgSendTime = msgSendTime;
        this.msgContext = msgContext;
        this.msgSendId = msgSendId;
        this.msgReciveId = msgReciveId;
    }

    @TableId("msg_id")
    private Integer msgId;
    @TableField(exist = false)
    private String notry;
    @TableField("msg_send_time")
    private String msgSendTime;
    @TableField("msg_context")
    private String msgContext;
    @TableField("msg_send_id")
    private Integer msgSendId;
    @TableField("msg_recive_id")
    private Integer msgReciveId;

    public String getNotry() {
        return notry;
    }

    public void setNotry(String notry) {
        this.notry = notry;
    }

    public Message() {
    }

    public Integer getMsgId() {
        return msgId;
    }

    public void setMsgId(Integer msgId) {
        this.msgId = msgId;
    }

    public String getMsgSendTime() {
        return msgSendTime;
    }

    public void setMsgSendTime(String msgSendTime) {
        this.msgSendTime = msgSendTime;
    }

    public String getMsgContext() {
        return msgContext;
    }

    public void setMsgContext(String msgContext) {
        this.msgContext = msgContext;
    }

    public Integer getMsgSendId() {
        return msgSendId;
    }

    public void setMsgSendId(Integer msgSendId) {
        this.msgSendId = msgSendId;
    }

    public Integer getMsgReciveId() {
        return msgReciveId;
    }

    public void setMsgReciveId(Integer msgReciveId) {
        this.msgReciveId = msgReciveId;
    }

    @Override
    public String toString() {
        return "Message{" +
        "msgId=" + msgId +
        ", msgSendTime=" + msgSendTime +
        ", msgContext=" + msgContext +
        ", msgSendId=" + msgSendId +
        ", msgReciveId=" + msgReciveId +
        "}";
    }
}
