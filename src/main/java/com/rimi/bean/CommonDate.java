package com.rimi.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class CommonDate {
    private Integer year = Calendar.getInstance().get(Calendar.YEAR);
    private Integer month = Calendar.getInstance().get(Calendar.MONTH);
    private Integer day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    private Integer hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    private Integer minute = Calendar.getInstance().get(Calendar.MINUTE);
    private Integer second = Calendar.getInstance().get(Calendar.SECOND);
    /***
     * 获取几天前的日期
     */
    public String getDay(Integer s){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        calendar.add(Calendar.DATE, - s);    //得到前几天
        Date times = calendar.getTime();
        String time =  sdf.format(times)+" 00:00:00";
        return time;
    }
    /**
     * 获取当前时间
     */
    public Date getTime24(){
        Date date = new Date();
        return date;
    }
    /**
     * 获取格式化时间
     * @return 当前时间,24小时yyyy-MM-dd HH:mm:ss格式
     */
    public String getTimeString(){
        Date date = getTime24();
        SimpleDateFormat timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = timeStamp.format(date);
        return format;
    }
    /***
     * 格式化时间yyyy-MM-dd HH:mm:ss
     *
     */
    public String  getMysqlTime(Date date){
        SimpleDateFormat timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = timeStamp.format(date);
        return format;
    }
    /***
     * 计算与当前时间的时间差
     * @return
     */
    public Integer gettime(Date date){
        Date date2 = getTime24();
        Integer days = (int) ((date2.getTime() - date.getTime()) / (24 * 60 * 60 * 1000));
        return days;
    }
    /***
     * 字符串格式化
     * @return
     */
    public Date getStingtoDate(String time){
        SimpleDateFormat timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date =null;
        try {
            date = timeStamp.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    /**
     * 获取年龄
     * @reture 当前时间 - 出生年月
     */
    public int getAge(String year){
        if (year==null&&("").equals(year)){
            return 0;
        }
        int userYear = Integer.parseInt(year);
        return  this.year - userYear;
    }
//    /**
//     * 获取年月日字符串
//     */
//    public String getDate(Date date){
//        this.year = date.getYear();
//        this.day = date.getDay();
//        this.month = date.getMonth();
//        return year+"年"+month+"月"+day+"日";
//    }
//    /**
//     * 获取时间字符串
//     */
//    public String getDateTime(Date date){
//        this.hour = date.getHours();
//        this.minute = date.getMinutes();
//        this.second = date.getSeconds();
//        return hour+":"+minute+":"+second;
//    }
}
