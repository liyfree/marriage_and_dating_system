package com.rimi.bean;

/**
 * 用户地址对象
 */
public class UserAddress {
        // 身份
        private String s_province;
        // 市区
        private String s_city;
        // 县级市
        private String s_county;

        @Override
        public String toString() {
            return "userAddress:{ s_province :"+s_province+",s_city :"+s_city+",s_county :"+s_county+"}";
        }

        public String getS_province() {
            return s_province;
        }

        public void setS_province(String s_province) {
            this.s_province = s_province;
        }

        public String getS_city() {
            return s_city;
        }

        public void setS_city(String s_city) {
            this.s_city = s_city;
        }

        public String getS_county() {
            return s_county;
        }

        public void setS_county(String s_county) {
            this.s_county = s_county;
        }
    }


