package com.rimi.bean;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class Visit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Visit{" +
                "userId=" + userId +
                ", peopleId=" + peopleId +
                ", time=" + time +
                ", num=" + num +
                '}';
    }

    public Visit(Integer userId, Integer peopleId, String time, Integer num) {
        this.userId = userId;
        this.peopleId = peopleId;
        this.time = time;
        this.num = num;
    }

    public Visit() {
    }

    @TableField("user_id")
    private Integer userId;
    @TableField("people_id")
    private Integer peopleId;
    private String time;
    private Integer num;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Integer peopleId) {
        this.peopleId = peopleId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
