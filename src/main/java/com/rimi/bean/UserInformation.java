package com.rimi.bean;

public class UserInformation {
    private User user;
    private LikeUser likeUser;
    private int MatchingIndex;



    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LikeUser getLikeUser() {
        return likeUser;
    }

    public void setLikeUser(LikeUser likeUser) {
        this.likeUser = likeUser;
    }

    public int getMatchingIndex() {
        return MatchingIndex;
    }

    public void setMatchingIndex(int matchingIndex) {
        MatchingIndex = matchingIndex;
    }

    public UserInformation() {
    }

    public UserInformation(User user, LikeUser likeUser, int matchingIndex) {
        this.user = user;
        this.likeUser = likeUser;
        MatchingIndex = matchingIndex;
    }
}
