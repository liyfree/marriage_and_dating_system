package com.rimi.bean;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class OutdoorSport implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("outdoor_name")
    private String outdoorName;
    @TableField(exist = false)
    private String checked;


    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOutdoorName() {
        return outdoorName;
    }

    public void setOutdoorName(String outdoorName) {
        this.outdoorName = outdoorName;
    }

    @Override
    public String toString() {
        return "OutdoorSport{" +
        "id=" + id +
        ", outdoorName=" + outdoorName +
        "}";
    }
}
