package com.rimi.bean;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class LikeUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "user_id")
    private Integer userId;
    @TableField("user_blood")
    private String userBlood;
    @TableField("user_birthday")
    private Date userBirthday;
    @TableField("user_height")
    private Double userHeight;
    @TableField("user_weight")
    private Double userWeight;
    @TableField("user_nation")
    private String userNation;
    @TableField("user_fromplace")
    private String userFromplace;
    @TableField("user_school")
    private String userSchool;
    @TableField("user_major")
    private String userMajor;
    @TableField("user_enucation")
    private String userEnucation;
    @TableField("user_house")
    private String userHouse;
    @TableField("user_car")
    private String userCar;
    @TableField("user_drink")
    private String userDrink;
    @TableField("user_smoke")
    private String userSmoke;
    @TableField("user_marry")
    private String userMarry;
    @TableField("user_child")
    private String userChild;
    @TableField("user_wangchild")
    private String userWangchild;
    @TableField("user_parent")
    private String userParent;
    @TableField("user_income")
    private String userIncome;
    @TableField("user_location")
    private String userLocation;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserBlood() {
        return userBlood;
    }

    public void setUserBlood(String userBlood) {
        this.userBlood = userBlood;
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public Double getUserHeight() {
        return userHeight;
    }

    public void setUserHeight(Double userHeight) {
        this.userHeight = userHeight;
    }

    public Double getUserWeight() {
        return userWeight;
    }

    public void setUserWeight(Double userWeight) {
        this.userWeight = userWeight;
    }

    public String getUserNation() {
        return userNation;
    }

    public void setUserNation(String userNation) {
        this.userNation = userNation;
    }

    public String getUserFromplace() {
        return userFromplace;
    }

    public void setUserFromplace(String userFromplace) {
        this.userFromplace = userFromplace;
    }

    public String getUserSchool() {
        return userSchool;
    }

    public void setUserSchool(String userSchool) {
        this.userSchool = userSchool;
    }

    public String getUserMajor() {
        return userMajor;
    }

    public void setUserMajor(String userMajor) {
        this.userMajor = userMajor;
    }

    public String getUserEnucation() {
        return userEnucation;
    }

    public void setUserEnucation(String userEnucation) {
        this.userEnucation = userEnucation;
    }

    public String getUserHouse() {
        return userHouse;
    }

    public void setUserHouse(String userHouse) {
        this.userHouse = userHouse;
    }

    public String getUserCar() {
        return userCar;
    }

    public void setUserCar(String userCar) {
        this.userCar = userCar;
    }

    public String getUserDrink() {
        return userDrink;
    }

    public void setUserDrink(String userDrink) {
        this.userDrink = userDrink;
    }

    public String getUserSmoke() {
        return userSmoke;
    }

    public void setUserSmoke(String userSmoke) {
        this.userSmoke = userSmoke;
    }

    public String getUserMarry() {
        return userMarry;
    }

    public void setUserMarry(String userMarry) {
        this.userMarry = userMarry;
    }

    public String getUserChild() {
        return userChild;
    }

    public void setUserChild(String userChild) {
        this.userChild = userChild;
    }

    public String getUserWangchild() {
        return userWangchild;
    }

    public void setUserWangchild(String userWangchild) {
        this.userWangchild = userWangchild;
    }

    public String getUserParent() {
        return userParent;
    }

    public void setUserParent(String userParent) {
        this.userParent = userParent;
    }

    public String getUserIncome() {
        return userIncome;
    }

    public void setUserIncome(String userIncome) {
        this.userIncome = userIncome;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    @Override
    public String toString() {
        return "LikeUser{" +
        "userId=" + userId +
        ", userBlood=" + userBlood +
        ", userBirthday=" + userBirthday +
        ", userHeight=" + userHeight +
        ", userWeight=" + userWeight +
        ", userNation=" + userNation +
        ", userFromplace=" + userFromplace +
        ", userSchool=" + userSchool +
        ", userMajor=" + userMajor +
        ", userEnucation=" + userEnucation +
        ", userHouse=" + userHouse +
        ", userCar=" + userCar +
        ", userDrink=" + userDrink +
        ", userSmoke=" + userSmoke +
        ", userMarry=" + userMarry +
        ", userChild=" + userChild +
        ", userWangchild=" + userWangchild +
        ", userParent=" + userParent +
        ", userIncome=" + userIncome +
        ", userLocation=" + userLocation +
        "}";
    }
}
