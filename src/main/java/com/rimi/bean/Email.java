package com.rimi.bean;

public class Email {
    // 邮件主题
    private String subject;
    // 发送者
    private String fromAddress;
    // 接收者
    private String toAddress;
    // 发送内容
    private String text;
    //邮箱授权码
    private String licenseCode;

    public Email() {
    }

    public Email(String subject, String fromAddress, String toAddress, String text, String licenseCode) {
        this.subject = subject;
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.text = text;
        this.licenseCode = licenseCode;
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLicenseCode() {
        return licenseCode;
    }

    public void setLicenseCode(String licenseCode) {
        this.licenseCode = licenseCode;
    }

    @Override
    public String toString() {
        return "Email{" +
                "subject='" + subject + '\'' +
                ", fromAddress='" + fromAddress + '\'' +
                ", toAddress='" + toAddress + '\'' +
                ", text='" + text + '\'' +
                ", licenseCode='" + licenseCode + '\'' +
                '}';
    }
}
