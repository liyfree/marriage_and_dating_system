package com.rimi.bean;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class LeisureStyle implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("leisure_name")
    private String leisureName;
    @TableField(exist = false)
    private String checked;

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLeisureName() {
        return leisureName;
    }

    public void setLeisureName(String leisureName) {
        this.leisureName = leisureName;
    }

    @Override
    public String toString() {
        return "LeisureStyle{" +
        "id=" + id +
        ", leisureName=" + leisureName +
        "}";
    }
}
