package com.rimi.bean;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("menu_id")
    private Integer menuId;
    @TableField("menu_code")
    private Integer menuCode;
    @TableField("menu_name")
    private String menuName;
    @TableField("menu_url")
    private String menuUrl;
    @TableField("menu_status")
    private String menuStatus;


    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(Integer menuCode) {
        this.menuCode = menuCode;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuStatus() {
        return menuStatus;
    }

    public void setMenuStatus(String menuStatus) {
        this.menuStatus = menuStatus;
    }

    @Override
    public String toString() {
        return "Menu{" +
        "menuId=" + menuId +
        ", menuCode=" + menuCode +
        ", menuName=" + menuName +
        ", menuUrl=" + menuUrl +
        ", menuStatus=" + menuStatus +
        "}";
    }
}
