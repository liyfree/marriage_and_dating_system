package com.rimi.bean;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class UserInterest implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("user_id")
    private Integer userId;
    @TableField("user_lifestyle")
    private String userLifestyle;
    @TableField("user_pet")
    private String userPet;
    @TableField("user_bodybuliding")
    private String userBodybuliding;
    @TableField("user_outdoor")
    private String userOutdoor;
    @TableField("user_travel")
    private String userTravel;
    @TableField("user_ieisure")
    private String userIeisure;
    @TableField("user_food")
    private String userFood;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserLifestyle() {
        return userLifestyle;
    }

    public void setUserLifestyle(String userLifestyle) {
        this.userLifestyle = userLifestyle;
    }

    public String getUserPet() {
        return userPet;
    }

    public void setUserPet(String userPet) {
        this.userPet = userPet;
    }

    public String getUserBodybuliding() {
        return userBodybuliding;
    }

    public void setUserBodybuliding(String userBodybuliding) {
        this.userBodybuliding = userBodybuliding;
    }

    public String getUserOutdoor() {
        return userOutdoor;
    }

    public void setUserOutdoor(String userOutdoor) {
        this.userOutdoor = userOutdoor;
    }

    public String getUserTravel() {
        return userTravel;
    }

    public void setUserTravel(String userTravel) {
        this.userTravel = userTravel;
    }

    public String getUserIeisure() {
        return userIeisure;
    }

    public void setUserIeisure(String userIeisure) {
        this.userIeisure = userIeisure;
    }

    public String getUserFood() {
        return userFood;
    }

    public void setUserFood(String userFood) {
        this.userFood = userFood;
    }

    @Override
    public String toString() {
        return "UserInterest{" +
        "id=" + id +
        ", userId=" + userId +
        ", userLifestyle=" + userLifestyle +
        ", userPet=" + userPet +
        ", userBodybuliding=" + userBodybuliding +
        ", userOutdoor=" + userOutdoor +
        ", userTravel=" + userTravel +
        ", userIeisure=" + userIeisure +
        ", userFood=" + userFood +
        "}";
    }
}
