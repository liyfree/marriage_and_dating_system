package com.rimi.bean;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.mapper.Wrapper;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class UserFriend  {


    public UserFriend(Integer userId, Integer friendId, Integer relation) {
        this.userId = userId;
        this.friendId = friendId;
        this.relation = relation;
    }

    public UserFriend() {
    }

    @TableField("user_id")
    private Integer userId;
    @TableField("friend_id")
    private Integer friendId;
    private Integer relation;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFriendId() {
        return friendId;
    }

    public void setFriendId(Integer friendId) {
        this.friendId = friendId;
    }

    public Integer getRelation() {
        return relation;
    }

    public void setRelation(Integer relation) {
        this.relation = relation;
    }

    @Override
    public String toString() {
        return "UserFriend{" +
        "userId=" + userId +
        ", friendId=" + friendId +
        ", relation=" + relation +
        "}";
    }
}
