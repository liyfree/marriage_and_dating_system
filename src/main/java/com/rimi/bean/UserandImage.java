package com.rimi.bean;

public class UserandImage {

    public UserandImage() {
    }

    public UserandImage(Integer userid, String username, String imageurl, Integer state) {
        this.userid = userid;
        this.username = username;
        this.imageurl = imageurl;
        this.state = state;
    }

    private Integer userid;
    private String username;
    private String imageurl;
    private Integer state;
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }


    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

}
