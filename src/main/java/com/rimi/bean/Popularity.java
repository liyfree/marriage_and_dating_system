package com.rimi.bean;

import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class Popularity implements Serializable {

    private static final long serialVersionUID = 1L;

    public Popularity(Integer userId, Integer num) {
        this.userId = userId;
        this.num = num;
    }

    public Popularity() {
    }

    @TableField("user_id")
    private Integer userId;
    private Integer num;



    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Popularity{" +
        "userId=" + userId +
        ", num=" + num +
        "}";
    }
}
