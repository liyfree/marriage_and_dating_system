package com.rimi.bean;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class Status implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("status_id")
    private Integer statusId;
    @TableField("status_name")
    private String statusName;
    @TableField("statu_code")
    private String statuCode;


    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatuCode() {
        return statuCode;
    }

    public void setStatuCode(String statuCode) {
        this.statuCode = statuCode;
    }

    @Override
    public String toString() {
        return "Status{" +
        "statusId=" + statusId +
        ", statusName=" + statusName +
        ", statuCode=" + statuCode +
        "}";
    }
}
