package com.rimi.bean;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author ly
 * @since 2018-06-05
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    // 用户id
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;
    // 角色id
    @TableField("role_id")
    private Integer roleId;
    // 账号
    @TableField("user_account")
    private String userAccount;
    // 密码
    @TableField("user_password")
    private String userPassword;
    // 用户名称
    @TableField("user_realname")
    private String userRealname;
    //性别
    @TableField("user_sex")
    private String userSex;
    // 血型
    @TableField("user_blood")
    private String userBlood;
    // 生日
    @TableField("user_birthday")
    private Date userBirthday;
    // 身高
    @TableField("user_height")
    private Double userHeight;
    // 体重
    @TableField("user_weight")
    private Double userWeight;
    // 民族
    @TableField("user_nation")
    private String userNation;
    // 籍贯
    @TableField("user_fromplace")
    private String userFromplace;
    // 学校
    @TableField("user_school")
    private String userSchool;
    // 专业
    @TableField("user_major")
    private String userMajor;
    //  学历
    @TableField("user_enucation")
    private String userEnucation;
    // 是否有房
    @TableField("user_house")
    private String userHouse;
    // 是否有车
    @TableField("user_car")
    private String userCar;
    // 电话
    @TableField("user_phone")
    private String userPhone;
    // 邮箱
    @TableField("user_email")
    private String userEmail;
    // qq号码
    @TableField("user_qq")
    private String userQq;
    // 微信
    @TableField("user_wx")
    private String userWx;
    // 是否喝酒
    @TableField("user_drink")
    private String userDrink;
    // 是否抽烟
    @TableField("user_smoke")
    private String userSmoke;
    // 婚姻状况
    @TableField("user_marry")
    private String userMarry;
    // 子女情况
    @TableField("user_child")
    private String userChild;
    // 是否想要小孩
    @TableField("user_wangchild")
    private String userWangchild;
    // 父母情况
    @TableField("user_parent")
    private String userParent;
    // 月收入
    @TableField("user_income")
    private String userIncome;
    // 所在地
    @TableField("user_location")
    private String userLocation;
    // 添加时间
    @TableField("user_addtime")
    private Date userAddtime;
    // 是否禁用
    @TableField("user_status")
    private String userStatus;
    // 最后登录时间
    @TableField("user_logintime")
    private Date userLogintime;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserRealname() {
        return userRealname;
    }

    public void setUserRealname(String userRealname) {
        this.userRealname = userRealname;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getUserBlood() {
        return userBlood;
    }

    public void setUserBlood(String userBlood) {
        this.userBlood = userBlood;
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public Double getUserHeight() {
        return userHeight;
    }

    public void setUserHeight(Double userHeight) {
        this.userHeight = userHeight;
    }

    public Double getUserWeight() {
        return userWeight;
    }

    public void setUserWeight(Double userWeight) {
        this.userWeight = userWeight;
    }

    public String getUserNation() {
        return userNation;
    }

    public void setUserNation(String userNation) {
        this.userNation = userNation;
    }

    public String getUserFromplace() {
        return userFromplace;
    }

    public void setUserFromplace(String userFromplace) {
        this.userFromplace = userFromplace;
    }

    public String getUserSchool() {
        return userSchool;
    }

    public void setUserSchool(String userSchool) {
        this.userSchool = userSchool;
    }

    public String getUserMajor() {
        return userMajor;
    }

    public void setUserMajor(String userMajor) {
        this.userMajor = userMajor;
    }

    public String getUserEnucation() {
        return userEnucation;
    }

    public void setUserEnucation(String userEnucation) {
        this.userEnucation = userEnucation;
    }

    public String getUserHouse() {
        return userHouse;
    }

    public void setUserHouse(String userHouse) {
        this.userHouse = userHouse;
    }

    public String getUserCar() {
        return userCar;
    }

    public void setUserCar(String userCar) {
        this.userCar = userCar;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserQq() {
        return userQq;
    }

    public void setUserQq(String userQq) {
        this.userQq = userQq;
    }

    public String getUserWx() {
        return userWx;
    }

    public void setUserWx(String userWx) {
        this.userWx = userWx;
    }

    public String getUserDrink() {
        return userDrink;
    }

    public void setUserDrink(String userDrink) {
        this.userDrink = userDrink;
    }

    public String getUserSmoke() {
        return userSmoke;
    }

    public void setUserSmoke(String userSmoke) {
        this.userSmoke = userSmoke;
    }

    public String getUserMarry() {
        return userMarry;
    }

    public void setUserMarry(String userMarry) {
        this.userMarry = userMarry;
    }

    public String getUserChild() {
        return userChild;
    }

    public void setUserChild(String userChild) {
        this.userChild = userChild;
    }

    public String getUserWangchild() {
        return userWangchild;
    }

    public void setUserWangchild(String userWangchild) {
        this.userWangchild = userWangchild;
    }

    public String getUserParent() {
        return userParent;
    }

    public void setUserParent(String userParent) {
        this.userParent = userParent;
    }

    public String getUserIncome() {
        return userIncome;
    }

    public void setUserIncome(String userIncome) {
        this.userIncome = userIncome;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public Date getUserAddtime() {
        return userAddtime;
    }

    public void setUserAddtime(Date userAddtime) {
        this.userAddtime = userAddtime;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Date getUserLogintime() {
        return userLogintime;
    }

    public void setUserLogintime(Date userLogintime) {
        this.userLogintime = userLogintime;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", roleId=" + roleId +
                ", userAccount=" + userAccount +
                ", userPassword=" + userPassword +
                ", userRealname=" + userRealname +
                ", userSex=" + userSex +
                ", userBlood=" + userBlood +
                ", userBirthday=" + userBirthday +
                ", userHeight=" + userHeight +
                ", userWeight=" + userWeight +
                ", userNation=" + userNation +
                ", userFromplace=" + userFromplace +
                ", userSchool=" + userSchool +
                ", userMajor=" + userMajor +
                ", userEnucation=" + userEnucation +
                ", userHouse=" + userHouse +
                ", userCar=" + userCar +
                ", userPhone=" + userPhone +
                ", userEmail=" + userEmail +
                ", userQq=" + userQq +
                ", userWx=" + userWx +
                ", userDrink=" + userDrink +
                ", userSmoke=" + userSmoke +
                ", userMarry=" + userMarry +
                ", userChild=" + userChild +
                ", userWangchild=" + userWangchild +
                ", userParent=" + userParent +
                ", userIncome=" + userIncome +
                ", userLocation=" + userLocation +
                ", userAddtime=" + userAddtime +
                ", userStatus=" + userStatus +
                ", userLogintime=" + userLogintime +
                "}";
    }
}
