package com.rimi.bean;

import java.util.Date;

public class UserPage {
    private String age1;
    private  String age2;
    private Integer height1;
    private Integer height2;
    private String inCome;
    private String marry;
    private  String fromplace;
    private  int  nowPage=1; //当前页
    private  int  pageSize=12;//每页显示多少条记录数
    private  int  totalPage; //总页数  总记录数/分页单位
    private  int  count;     //总记录数
    private  int  startPage; //开始查询数
    private String sex;

    public UserPage(String age1, String age2, Integer height1, Integer height2, String inCome, String marry, String fromplace, int nowPage, int pageSize, int totalPage, int count, int startPage, String sex) {
        this.age1 = age1;
        this.age2 = age2;
        this.height1 = height1;
        this.height2 = height2;
        this.inCome = inCome;
        this.marry = marry;
        this.fromplace = fromplace;
        this.nowPage = nowPage;
        this.pageSize = pageSize;
        this.totalPage = totalPage;
        this.count = count;
        this.startPage = startPage;
        this.sex = sex;
    }

    public Integer getHeight1() {
        return height1;
    }

    public void setHeight1(Integer height1) {
        this.height1 = height1;
    }

    public Integer getHeight2() {
        return height2;
    }

    public void setHeight2(Integer height2) {
        this.height2 = height2;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge1() {
        return age1;
    }

    public void setAge1(String age1) {
        this.age1 = age1;
    }

    public String getAge2() {
        return age2;
    }

    public void setAge2(String age2) {
        this.age2 = age2;
    }

    public String getFromplace() {
        return fromplace;
    }

    public void setFromplace(String fromplace) {
        this.fromplace = fromplace;
    }

    public int getStartPage() {
        if(nowPage>=1&&nowPage<=totalPage){
            return (nowPage-1)*pageSize;
        }else if (nowPage>totalPage){
            return totalPage;
        }else {
            return 0;
        }
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public UserPage() {
    }


    public String getInCome() {
        return inCome;
    }

    public void setInCome(String inCome) {
        this.inCome = inCome;
    }

    public String getMarry() {
        return marry;
    }

    public void setMarry(String marry) {
        this.marry = marry;
    }

    public int getNowPage() {
        return nowPage;
    }

    public void setNowPage(int nowPage) {
        this.nowPage = nowPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return count%pageSize==0?count/pageSize:count/pageSize+1;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
