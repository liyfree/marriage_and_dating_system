package com.rimi.bean;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *  在线用户
 */
public class OnlineUser {

    private List<HttpSession> list = new ArrayList<HttpSession>();

    private User user;

    /**
     *保存在线用户
     */

    public Boolean addOnlineUser(HttpSession session){
        boolean add = list.add(session);
        return add;
    }
    /**
     * 获取在线用户集合
     */
    public List<User> getOnlineUserList(){
        List<User> userList = new ArrayList<User>();
        if (list!=null){
            for (HttpSession session : list ) {
                user = (User) session.getAttribute("user");
                userList.add(user);
            }
        }
        return userList;
    }
}
