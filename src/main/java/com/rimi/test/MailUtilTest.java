package com.rimi.test;

import com.rimi.bean.Email;
import com.rimi.util.CodeUtil;
import com.rimi.util.MailUtil;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

public class MailUtilTest {
    public static void main(String[] args) {

        // 获取验证码
        String code = CodeUtil.getCode();

        System.out.println("生成的验证码是：" + code);

        // 创建邮件对象
        Email email = new Email();
        // 设置发送者邮箱
        email.setFromAddress("liyfree@qq.com");
        // 设置接收者邮箱
        email.setToAddress("724973851@qq.com");
        // 设置邮件主题
        email.setSubject("缘来是你婚恋网验证码");
        // 设置邮件内容，可以使用html标签
        email.setText("<h3>欢迎您注册缘来是你婚恋网，您本次的注册验证码是：<font color='red'>" + code + "</font>  ,5分钟内有效,请尽快完成注册！</h3>");
        // 设置邮箱授权码，需要去qq邮箱获取
        email.setLicenseCode("rmgcmqfewhxcbdbd");

        try {
            // 发送邮件
            MailUtil.sendMail(email);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            System.out.println("邮件发送失败！");
        } catch (MessagingException e) {
            e.printStackTrace();
            System.out.println("邮件发送失败！");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.out.println("邮件发送失败！");
        }

        System.out.println("邮件发送成功！");

    }
}
