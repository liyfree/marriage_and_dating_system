<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>南昌万家喜婚恋交友网</title>
    <meta name="Keywords" content="南昌万家喜婚恋交友网"/>
    <meta name="Description" content="南昌万家喜婚恋交友网"/>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/js/comment.js"></script>
    <script type="text/javascript" src="/js/jquery.collapse.js"></script>
</head>
<body>
<div class="head">
    <div class="top">
        <div class="top-left">
            <a href="javascript:;" class="top-coll" onclick="AddFavorite('', 'http://www.ncwjxhljyw.com')">收藏万家喜</a> |
            <a href="" class="top-att">关注万家喜</a>
        </div>
        <div class="top-right">
            <a href="">注册</a> |
            <a href="">登录</a>
        </div>
    </div>
    <div class="top-ban">
        <div class="top-mid-box">
            <a href="" class="logo"><img src="/images/logo.png"/></a>
            <div class="adv"><img src="/images/adv.png"/></div>
            <div class="tele"><img src="/images/tele.png"/></div>
            <div class="erweima"><img src="/images/erwei.png"/></div>
        </div>
    </div>
</div>
<div class="nav-box">
    <div class="nav">
        <a href="/user/chatindex">网站首页</a>
        <a href="/user/firstUserList">条件搜索</a>
        <a href="/user/userInformation">快速匹配</a>
        <a href="/sotry/index">婚恋故事</a>
        <a href="/userPersonal">个人中心</a>
    </div>
</div>
<div class="main">
    <div class="main-box1">
        <div class="main-right" style="margin-left: 0; margin-right: 14px;"><!--right-->
            <div class="list-main1-title">个人中心</div>
            <div class="col c2"><!--menu-->
                <div class="list-top"><a href="/userPersonal" class="cur">基本资料</a></div>
                <div class="list-top"><a href="/userPersonal/addcontactPage">联系方式</a></div>
                <div class="list-top"><a href="/likeUser/otherHalf">择偶条件</a></div>
                <div class="list-top"><a href="/userImage/imageShow">我的照片</a></div>
                <div class="css3-animated-example">
                    <h3>兴趣爱好</h3>
                    <div>
                        <div class="content">
                            <p><a href="/lifeStyle/getLifeStyleList">生活方式</a></p>
                            <p><a href="/petHobby/getHobbyList">喜欢宠物</a></p>
                            <p><a href="/foodHabit/getFoodHabitList">饮食习惯</a></p>
                            <p><a href="/leisureStyle/getLeisureList">休闲方式</a></p>
                            <p><a href="/bodyBuilding/getBodyBuildList">运动健身</a></p>
                            <p><a href="/outdoorSport/getOutdoorSpotrList">户外运动</a></p>
                            <p><a href="/travel/getTravelList">旅行方式</a></p>
                        </div>
                    </div>
                </div>
                <div class="list-top"><a href="/userImage/getImageList">管理照片</a></div>
            </div><!--menu-->
        </div><!--right-->
        <div class="main-left"><!--left-->
            <div class="s-address">当前位置：<a href="">首页 </a>&gt;个人中心&gt;兴趣爱好</div>
            <div class="safty">
                <div class="tit">休闲方式 >></div>
                <form action="/leisureStyle/addLeiSureStyle">
                    <ul class="condition-list" style="font-size: 16px;">
                        <c:forEach var="leisure" items="${leisureList}">
                            <li style="margin-top: 5px">
                                <input type="checkbox" name="leisure" value="${leisure.id}" ${leisure.checked}/>
                                <span>${leisure.leisureName}</span>
                            </li>
                        </c:forEach>

                    </ul>
                    <div class="btn-box">
                        <input class="save-btn" type="submit" value="保存并继续">
                    </div>
                </form>
            </div>
        </div><!--left-->
    </div>
    <div class="nav-box">
        <div class="nav">
            <a href="/user/chatindex">网站首页</a>
            <a href="/user/firstUserList">条件搜索</a>
            <a href="/user/userInformation">快速匹配</a>
            <a href="/sotry/index">婚恋故事</a>
            <a href="/userPersonal">个人中心</a>
    </div>
    <div class="copy">
        <p>COPYRIGHT 2015  南昌万家囍婚恋交友网  版权所有 赣ICP备15002274号-1</p>
        <p>地址：进贤县香江商业街27号  电话：138-7915-5305  技术支持：<a target="_blank" href="http://www.0791jr.com/">嘉瑞科技</a></p>
    </div>
    <!--在线客服-->
    <div class="chat_jsp">
        <iframe id="divFloatToolsView" src="/userFriend/UpChat"></iframe>
        <div id="floatTools" class="rides-cs" style="height:247px; ">
            <div class="floatL">
                <a id="aFloatTools_Show" class="btnOpen" title="打开聊天系统" style="top:20px;display:block" href="javascript:void(0);">展开</a>
                <a id="aFloatTools_Hide" class="btnCtn" title="关闭聊天系统" style="top:20px;display:none" href="javascript:void(0);">收缩</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(".css3-animated-example").collapse({
        accordion: true,
        open: function () {
            this.addClass("open");
            this.css({height: this.children().outerHeight()});
        },
        close: function () {
            this.css({height: "0px"});
            this.removeClass("open");
        }
    });
</script>
</body>
</html>
