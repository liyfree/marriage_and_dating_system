<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>南昌万家喜婚恋交友网</title>
    <meta name="Keywords" content="南昌万家喜婚恋交友网"/>
    <meta name="Description" content="南昌万家喜婚恋交友网"/>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/js/comment.js"></script>
    <script type="text/javascript" src="/js/jquery.collapse.js"></script>
    <script language="javascript" type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
</head>
<body>
<div class="head">
    <div class="top">
        <div class="top-left">
            <a href="javascript:;" class="top-coll" onclick="AddFavorite('', 'http://www.ncwjxhljyw.com')">收藏万家喜</a> |
            <a href="" class="top-att">关注万家喜</a>
        </div>
        <div class="top-right">
            <a href="">注册</a> |
            <a href="">登录</a>
        </div>
    </div>
    <div class="top-ban">
        <div class="top-mid-box">
            <a href="" class="logo"><img src="/images/logo.png"/></a>
            <div class="adv"><img src="/images/adv.png"/></div>
            <div class="tele"><img src="/images/tele.png"/></div>
            <div class="erweima"><img src="/images/erwei.png"/></div>
        </div>
    </div>
</div>
<div class="nav-box">
    <div class="nav">
        <a href="/user/chatindex">网站首页</a>
        <a href="/user/firstUserList">条件搜索</a>
        <a href="/user/userInformation">快速匹配</a>
        <a href="/sotry/index">婚恋故事</a>
        <a href="/userPersonal">个人中心</a>
    </div>
</div>
<div class="main">
    <div class="main-box1">
        <div class="main-right" style="margin-left: 0; margin-right: 14px;"><!--right-->
            <div class="list-main1-title">个人中心</div>
            <div class="col c2"><!--menu-->
                <div class="list-top"><a href="/userPersonal" class="cur">基本资料</a></div>
                <div class="list-top"><a href="/userPersonal/addcontactPage">联系方式</a></div>
                <div class="list-top"><a href="/likeUser/otherHalf">择偶条件</a></div>
                <div class="list-top"><a href="/userImage/imageShow">我的照片</a></div>
                <div class="css3-animated-example">
                    <h3>兴趣爱好</h3>
                    <div>
                        <div class="content">
                            <p><a href="/lifeStyle/getLifeStyleList">生活方式</a></p>
                            <p><a href="/petHobby/getHobbyList">喜欢宠物</a></p>
                            <p><a href="/foodHabit/getFoodHabitList">饮食习惯</a></p>
                            <p><a href="/leisureStyle/getLeisureList">休闲方式</a></p>
                            <p><a href="/bodyBuilding/getBodyBuildList">运动健身</a></p>
                            <p><a href="/outdoorSport/getOutdoorSpotrList">户外运动</a></p>
                            <p><a href="/travel/getTravelList">旅行方式</a></p>
                        </div>
                    </div>
                </div>
                <div class="list-top"><a href="/userImage/getImageList">管理照片</a></div>
            </div><!--menu-->
        </div><!--right-->
        <div class="main-left"><!--left-->
            <div class="s-address">当前位置：<a href="">首页 </a>&gt;个人中心&gt;择偶条件</div>
            <div class="safty">
                <form action="/likeUser/addOtherHalf" method="post">
                    <p name="userId">
                        <span>会&nbsp;&nbsp;员 &nbsp;号：</span>
                        ${loginUser.userId}
                    </p>
                    <input type="hidden" name="userId" value="${loginUser.userId}">
                    <br>
                    <div class="tit">择偶条件 >></div>
                    <ul class="condition-list">
                        <li>
                            <p>
                                <span>出生年份：</span>
                                <input class="conditon-select" type="text" name="likeUserBir"
                                       onclick="WdatePicker({dateFmt:'yyyy'})"
                                       value="<fm:formatDate value="${likeUser.userBirthday}" type="date"  pattern="yyyy"/>"
                                       placeholder="请选择"/>
                            </p>
                            <p>
                                <span>婚姻状况：</span>
                                <select class="conditon-select" name="userMarry">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userMarry=='已婚'?'selected':''}>已婚</option>
                                    <option ${likeUser.userMarry=='未婚'?'selected':''}>未婚</option>
                                    <option ${likeUser.userMarry=='离婚'?'selected':''}>离婚</option>
                                    <option ${likeUser.userMarry=='丧偶'?'selected':''}>丧偶</option>
                                    <option ${likeUser.userMarry=='不限'?'selected':''}>不限</option>
                                </select>
                            </p>
                            <p>
                                <span>  是否有车： </span>
                                <select class="conditon-select" name="userCar">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userCar=='有，有车贷'?'selected':''}>有，有车贷</option>
                                    <option ${likeUser.userCar=='有，无车贷'?'selected':''}>有，无车贷</option>
                                    <option ${likeUser.userCar=='无'?'selected':''}>无</option>
                                    <option ${likeUser.userCar=='不限'?'selected':''}>不限</option>
                                </select>
                            </p>
                            <p>
                                <span>  是否抽烟： </span>
                                <select class="conditon-select" name="userSmoke">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userSmoke=='是'?'selected':''}>是</option>
                                    <option ${likeUser.userSmoke=='否'?'selected':''}>否</option>
                                    <option ${likeUser.userSmoke=='不限'?'selected':''}>不限</option>
                                </select>
                            </p>
                            <p>
                                <span>  父母情况： </span>
                                <select class="conditon-select" name="userParent">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userParent=='单亲'?'selected':''}>单亲</option>
                                    <option ${likeUser.userParent=='双亲'?'selected':''}>双亲</option>
                                    <option ${likeUser.userParent=='已去世'?'selected':''}>已去世</option>
                                    <option ${likeUser.userParent=='不限'?'selected':''}>不限</option>
                                </select>
                            </p>
                            <p>
                                <span>血&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;型：</span>
                                <select class="conditon-select" name="userBlood">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userBlood=='A'?'selected':''}>A</option>
                                    <option ${likeUser.userBlood=='B'?'selected':''}>B</option>
                                    <option ${likeUser.userBlood=='AB'?'selected':''}>AB</option>
                                    <option ${likeUser.userBlood=='O'?'selected':''}>O</option>
                                    <option ${likeUser.userBlood=='不限'?'selected':''}>不限</option>
                                </select>
                                <span>型</span>
                            </p>

                        </li>

                        <li>
                            <p>
                                <span>民 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;族： </span>
                                <select class="conditon-select" name="userNation">
                                    <option value="">请选择</option>
                                    <option  ${likeUser.userNation=='汉族'?'selected':''}>汉族</option>
                                    <option  ${likeUser.userNation=='其他'?'selected':''}>其他</option>
                                    <option  ${likeUser.userNation=='不限'?'selected':''}>不限</option>
                                </select>
                            </p>
                            <p>
                                <span>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 校： </span>
                                <input class="conditon-select" type="text" name="userSchool" value="${likeUser.userSchool}" placeholder="请输入毕业院校">
                            </p>
                            <p>
                                <span>  住房情况： </span>
                                <select class="conditon-select" name="userHouse">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userHouse=='有，有房贷'?'selected':''}>有，有房贷</option>
                                    <option ${likeUser.userHouse=='有，无房贷'?'selected':''}>有，无房贷</option>
                                    <option ${likeUser.userHouse=='无'?'selected':''}>无</option>
                                    <option ${likeUser.userHouse=='不限'?'selected':''}>不限</option>
                                </select>
                            </p>
                            <p>
                                <span>  是否喝酒： </span>
                                <select class="conditon-select" name="userDrink">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userDrink=='是'?'selected':''}>是</option>
                                    <option ${likeUser.userDrink=='否'?'selected':''}>否</option>
                                    <option ${likeUser.userDrink=='不限'?'selected':''}>不限</option>
                                </select>
                            </p>
                            <p>
                                <span>  子女情况： </span>
                                <select class="conditon-select" name="userChild">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userChild=='无'?'selected':''}>无</option>
                                    <option ${likeUser.userChild=='有，住一起'?'selected':''}>有，住一起</option>
                                    <option ${likeUser.userChild=='有，未住一起'?'selected':''}>有，未住一起</option>
                                    <option ${likeUser.userChild=='不限'?'selected':''}>不限</option>
                                </select>
                            </p>

                        </li>
                        <li>

                            <p>
                                <span>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;历： </span>
                                <select class="conditon-select" name="userEnucation" style="width: 100px">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userEnucation=='高中及以下'?'selected':''}>高中及以下</option>
                                    <option ${likeUser.userEnucation=='大专'?'selected':''}>大专</option>
                                    <option ${likeUser.userEnucation=='本科'?'selected':''}>本科</option>
                                    <option ${likeUser.userEnucation=='研究生'?'selected':''}>研究生</option>
                                    <option ${likeUser.userEnucation=='硕士'?'selected':''}>硕士</option>
                                    <option ${likeUser.userEnucation=='博士及以上'?'selected':''}>博士及以上</option>
                                    <option ${likeUser.userEnucation=='不限'?'selected':''}>不限</option>
                                </select>
                            </p>
                            <p>
                                <span>月&nbsp;&nbsp;&nbsp;收&nbsp;&nbsp; &nbsp;入：</span>
                                <select class="conditon-select" style="width: 100px" name="userIncome">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userIncome=='0-0'?'selected':''} value="0-0">不限</option>
                                    <option ${likeUser.userIncome=='0-3000'?'selected':''} value="0-3000">0-3000</option>
                                    <option ${likeUser.userIncome=='3000-8000'?'selected':''} value="3000-8000">3000-8000</option>
                                    <option ${likeUser.userIncome=='8000-20000'?'selected':''} value="8000-20000">8000-20000</option>
                                    <option ${likeUser.userIncome=='20000-30000'?'selected':''} value="20000-30000">20000以上</option>
                                </select>
                                <span>k</span>
                            </p>
                            <p>
                                <span>身&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;高：</span>
                                <input class="conditon-select" type="text" name="userHeight" style="width: 100px" value="${likeUser.userHeight}"/>
                                <span>cm</span>
                            </p>
                            <p>
                                <span>体&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 重：</span>
                                <input class="conditon-select" type="text" name="userWeight" style="width: 100px" value="${likeUser.userWeight}"/>
                                <span>kg</span>
                            </p>
                            <p>
                                <span>  是否要小孩： </span>
                                <select class="conditon-select" name="userWangchild" style="width: 100px">
                                    <option value="">请选择</option>
                                    <option ${likeUser.userWangchild=='是'?'selected':''}>是</option>
                                    <option ${likeUser.userWangchild=='否'?'selected':''}>否</option>
                                    <option ${likeUser.userWangchild=='是'?'selected':''}>不限</option>
                                </select>
                            </p>


                        </li>
                    </ul>

                    <ul class="address" style="margin-top: 0;">
                        <li>
                            <p>
                                <span> 现居住地：</span>
                                <select class="address-select" name="s_province" id="s_province">
                                </select>
                                省
                                <select class="address-select" name="s_city" id="s_city">
                                </select>
                                市
                            </p>
                            <script src="/js/nownation.js" type="text/javascript"></script>
                            <c:set var="s_province" value="${likeUserLocation.s_province}" scope="session"></c:set>
                            <c:if test="${s_province !='省份' && s_province!=''}">
                                <script>
                                    opt0[0] = "${s_province }"
                                </script>
                            </c:if>
                            <c:set var="s_city" value="${likeUserLocation.s_city }" scope="session"></c:set>
                            <c:if test="${s_city !='地级市' && s_city!=''}">
                                <script>
                                    opt0[1] = "${s_city}"
                                </script>
                            </c:if>
                            <c:set var="s_county" value="${likeUserLocation.s_county}" scope="session"></c:set>
                            <c:if test="${s_county !='县级市' && s_county!=''}">
                                <script>
                                    opt0[2] = "${s_county}"
                                    _init_area();
                                </script>
                            </c:if>
                        </li>
                        <li>
                            <p>
                                <span> 户籍地址：</span>
                                <select class="address-select" name="s_province1" id="s_province1">
                                </select>
                                省
                                <select class="address-select" name="s_city1" id="s_city1">
                                </select>
                                市
                            </p>
                            <script src="/js/nownation1.js" type="text/javascript"></script>
                            <c:set var="s_province1" value="${likeUserFromPlace.s_province}" scope="session"></c:set>
                            <c:if test="${s_province1 !='省份'  && s_province1!=''}">
                                <script>
                                    opt1[0] = "${s_province1}"
                                </script>
                            </c:if>
                            <c:set var="s_city1" value="${likeUserFromPlace.s_city}" scope="session"></c:set>
                            <c:if test="${s_city1 !='地级市' && s_city1!=''}">
                                <script>
                                    opt1[1] = "${s_city1}"
                                    _init_area1();
                                </script>
                            </c:if>
                        </li>
                    </ul>
                    <div class="btn-box">
                        <input type="submit" class="save-btn" value="保存并继续"/>
                    </div>
                </form>
            </div>
        </div><!--left-->
    </div>
    <div class="nav-box">
        <div class="nav">
            <a href="/user/chatindex">网站首页</a>
            <a href="/user/firstUserList">条件搜索</a>
            <a href="/user/userInformation">快速匹配</a>
            <a href="/sotry/index">婚恋故事</a>
            <a href="/userPersonal">个人中心</a>
        </div>
    </div>
    <div class="copy">
        <p>COPYRIGHT 2015  南昌万家囍婚恋交友网  版权所有 赣ICP备15002274号-1</p>
        <p>地址：进贤县香江商业街27号  电话：138-7915-5305  技术支持：<a target="_blank" href="http://www.0791jr.com/">嘉瑞科技</a></p>
    </div>
    <!--在线客服-->
    <div class="chat_jsp">
        <iframe id="divFloatToolsView" src="/userFriend/UpChat"></iframe>
        <div id="floatTools" class="rides-cs" style="height:247px; ">
            <div class="floatL">
                <a id="aFloatTools_Show" class="btnOpen" title="打开聊天系统" style="top:20px;display:block" href="javascript:void(0);">展开</a>
                <a id="aFloatTools_Hide" class="btnCtn" title="关闭聊天系统" style="top:20px;display:none" href="javascript:void(0);">收缩</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(".css3-animated-example").collapse({
        accordion: true,
        open: function () {
            this.addClass("open");
            this.css({height: this.children().outerHeight()});
        },
        close: function () {
            this.css({height: "0px"});
            this.removeClass("open");
        }
    });
</script>

</body>
