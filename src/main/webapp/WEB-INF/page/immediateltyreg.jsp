<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>南昌万家喜婚恋交友网</title>
		<meta name="Keywords" content="南昌万家喜婚恋交友网" />
        <meta name="Description" content="南昌万家喜婚恋交友网"/>
		<link type="image/x-icon" rel=icon href="/images/icon.png" />
		<link type="text/css" rel="stylesheet" href="/css/style.css"/>
		<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="/js/comment.js"></script>
        <script type="text/javascript" src="/js/jquery.collapse.js" ></script>
	</head>
	<body>
		<div class="head">
		  <div class="top">
		  	 <div class="top-left">
		  	 	<a href="javascript:;" class="top-coll" onclick="AddFavorite('', 'http://www.ncwjxhljyw.com')">收藏万家喜</a> |
		  	 	<a href="" class="top-att">关注万家喜</a>
		  	 </div>
		  	 <div class="top-right">
		  	 	<a href="">注册</a> |
		  	 	<a href="">登录</a>
		  	 </div>
		  </div>
		  <div class="top-ban">
		  	<div class="top-mid-box">
		  	  <a href="" class="logo"><img src="/images/logo.png"/></a>
		  	  <div class="adv"><img src="/images/adv.png"/></div>
		  	  <div class="tele"><img src="/images/tele.png"/></div>
		  	  <div class="erweima"><img src="/images/erwei.png"/></div>
		  	</div>
		  </div>
		</div>
		<div class="nav-box">
			<div class="nav">
				<a href="/user/chatindex">网站首页</a>
            <a href="/user/firstUserList">条件搜索</a>
            <a href="/user/userInformation">快速匹配</a>
            <a href="/sotry/index">婚恋故事</a>
            <a href="/userPersonal">个人中心</a>
			</div>
		</div>
		<div class="main">
			<div class="main-box1">
				<div class="m-title">个人资料  >></div>
				<ul class="condition-list">
							<li>
								<p>
									<span>会&nbsp;&nbsp;员 &nbsp;号：</span>
									0001
								</p>
								<p>
									<span>身&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 高：</span>
									<select class="conditon-select">
										<option>165cm</option>
										<option>170cm</option>
									</select>
								</p>
								<p>
									<span>从事工作：</span>
									<select class="conditon-select">
										<option>销售</option>
										<option>计算机</option>
									</select>
								</p>
							</li>
							<li>
								<p>
									<span>昵&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 称：</span>
									<input type="text" class="conditon-select" />
								</p>
								<p>
									<span>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 历： </span>
									<select class="conditon-select">
										<option>高中</option>
										<option>大专</option>
									</select>
								</p>
								<p>
									<span>月&nbsp;&nbsp;收 &nbsp;入： </span>
									<select class="conditon-select">
										<option>1000-3000</option>
										<option>3000-5000</option>
									</select>
								</p>
								
							</li>
							<li>
								<p>
									<span> 性 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别： </span>
									<select class="conditon-select">
										<option>男</option>
										<option>女</option>
									</select>
								</p>
								<p>
									<span> 婚姻状况： </span>
									<select class="conditon-select">
										<option>已婚</option>
										<option>未婚</option>
									</select>
								</p>
								<p>
									<span> 家庭成员： </span>
									<select class="conditon-select">
										<option>1</option>
										<option>2</option>
									</select>
								</p>
							</li>
							<li>
								<p>
									<span> 出生年月： </span>
									<select class="conditon-select">
										<option>1990</option>
										<option>1992</option>
									</select>
								</p>
								<p>
									<span> 健康状况： </span>
									<select class="conditon-select">
										<option>优</option>
										<option>良</option>
									</select>
								</p>
								<p>
									<span> 住房情况： </span>
									<select class="conditon-select">
										<option>有房</option>
										<option>无房</option>
									</select>
								</p>
							</li>
						</ul>
						<ul class="address" style="margin-top: 0;">
							<li>
								<p>
									<span>现居住地：</span>
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									省
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									市
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									县
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									镇
								</p>
							</li>
							<li>
								<p>
									<span> 户 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;籍：</span>
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									省
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									市
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									县
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									镇
								</p>
							</li>
							<li>
								<p>
									<span>Q&nbsp;&nbsp;Q &nbsp;号： </span>
									<input type="text" class="conditon-select" />
								</p>
								<p>
									<span>上传真实照片： </span>
									<input type="file" class="conditon-select" />
								</p>
							</li>
						</ul>
					<div class="m-title">择偶要求  >></div>
					<ul class="condition-list">
							<li>
								<p>
									<span>年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 龄：</span>
									<select class="year-select">
										<option>0</option>
									</select>
									到
									<select class="year-select">
										<option>20</option>
									</select>
								</p>
								<p>
									<span>婚姻状况：</span>
									<select class="conditon-select">
										<option>未婚</option>
										<option>离婚</option>
									</select>
								</p>
								<p>
									<span>住房情况：</span>
									<select class="conditon-select">
										<option>有房</option>
										<option>无房</option>
									</select>
								</p>
							</li>
							<li>
								<p>
									<span>身&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 高：</span>
									<select class="conditon-select">
										<option>165cm</option>
										<option>170cm</option>
									</select>
								</p>
								<p>
									<span>月&nbsp;&nbsp;收 &nbsp;入： </span>
									<select class="conditon-select">
										<option>1000-3000</option>
										<option>3000-5000</option>
									</select>
								</p>
							</li>
							<li>
								<p>
									<span> 学 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;历： </span>
									<select class="conditon-select">
										<option>高中</option>
										<option>大专</option>
									</select>
								</p>
								<p>
									<span> 从事工作： </span>
									<select class="conditon-select">
										<option>销售</option>
										<option>大专</option>
									</select>
								</p>
							</li>
						</ul>
						
						<ul class="address">
							<li>
								<p>
									<span>现居住地：</span>
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									省
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									市
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									县
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									镇
								</p>
							</li>
							<li>
								<p>
									<span> 户 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;籍：</span>
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									省
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									市
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									县
									<select class="address-select" >
										<option>北京</option>
										<option>天津</option>
									</select>
									镇
								</p>
							</li>
						</ul>
						<div class="regwan">
						<button>完成注册</button>
						<p>（请完整的填写好信息！）</p>
						</div>
			</div>
			<div class="nav-box">
			  <div class="nav">
				<a href="/user/chatindex">网站首页</a>
            <a href="/user/firstUserList">条件搜索</a>
            <a href="/user/userInformation">快速匹配</a>
            <a href="/sotry/index">婚恋故事</a>
            <a href="/userPersonal">个人中心</a>
			  </div>
		    </div>
		 <div class="copy">
			<p>COPYRIGHT 2015  南昌万家囍婚恋交友网  版权所有 赣ICP备15002274号-1</p>
			<p>地址：进贤县香江商业街27号  电话：138-7915-5305  技术支持：<a target="_blank" href="http://www.0791jr.com/">嘉瑞科技</a></p>
		</div>
			<!--在线客服-->
			<div class="chat_jsp">
				<iframe id="divFloatToolsView" src="/userFriend/UpChat"></iframe>
				<div id="floatTools" class="rides-cs" style="height:247px; ">
					<div class="floatL">
						<a id="aFloatTools_Show" class="btnOpen" title="打开聊天系统" style="top:20px;display:block" href="javascript:void(0);">展开</a>
						<a id="aFloatTools_Hide" class="btnCtn" title="关闭聊天系统" style="top:20px;display:none" href="javascript:void(0);">收缩</a>
					</div>
				</div>
			</div>
		</div>
		
		  <script>
        $(".css3-animated-example").collapse({
          accordion: true,
          open: function() {
            this.addClass("open");
            this.css({ height: this.children().outerHeight() });
          },
          close: function() {
            this.css({ height: "0px" });
            this.removeClass("open");
          }
        });
      </script>
	</body>
</html>
