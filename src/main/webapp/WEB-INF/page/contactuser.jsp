<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>南昌万家喜婚恋交友网</title>
    <meta name="Keywords" content="南昌万家喜婚恋交友网"/>
    <meta name="Description" content="南昌万家喜婚恋交友网"/>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/js/comment.js"></script>
    <script type="text/javascript" src="/js/jquery.collapse.js"></script>
    <script language="javascript" type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
</head>
<body>
<div class="head">
    <div class="top">
        <div class="top-left">
            <a href="javascript:;" class="top-coll" onclick="AddFavorite('', 'http://www.ncwjxhljyw.com')">收藏万家喜</a> |
            <a href="" class="top-att">关注万家喜</a>
        </div>
        <div class="top-right">
            <a href="">注册</a> |
            <a href="">登录</a>
        </div>
    </div>
    <div class="top-ban">
        <div class="top-mid-box">
            <a href="" class="logo"><img src="/images/logo.png"/></a>
            <div class="adv"><img src="/images/adv.png"/></div>
            <div class="tele"><img src="/images/tele.png"/></div>
            <div class="erweima"><img src="/images/erwei.png"/></div>
        </div>
    </div>
</div>
<div class="nav-box">
    <div class="nav">
        <a href="">网站首页</a>
        <a href="">了解我们</a>
        <a href="">搜索会员 </a>
        <a href="">婚恋课堂 </a>
        <a href="">活动专题 </a>
        <a href="">婚恋故事</a>
        <a href="">联系社区工作人员</a>
        <a href="">个人中心</a>
    </div>
</div>
<div class="main">
    <div class="main-box1">
        <div class="main-right" style="margin-left: 0; margin-right: 14px;"><!--right-->
            <div class="list-main1-title">个人中心</div>
            <div class="col c2"><!--menu-->
                <div class="list-top"><a href="/userPersonal" class="cur">基本资料</a></div>
                <div class="list-top"><a href="/userPersonal/addcontactPage">联系方式</a></div>
                <div class="list-top"><a href="/likeUser/otherHalf">择偶条件</a></div>
                <div class="list-top"><a href="/userImage/imageShow">我的照片</a></div>
                <div class="css3-animated-example">
                    <h3>兴趣爱好</h3>
                    <div>
                        <div class="content">
                            <p><a href="/lifeStyle/getLifeStyleList">生活方式</a></p>
                            <p><a href="/petHobby/getHobbyList">喜欢宠物</a></p>
                            <p><a href="/foodHabit/getFoodHabitList">饮食习惯</a></p>
                            <p><a href="/leisureStyle/getLeisureList">休闲方式</a></p>
                            <p><a href="/bodyBuilding/getBodyBuildList">运动健身</a></p>
                            <p><a href="/outdoorSport/getOutdoorSpotrList">户外运动</a></p>
                            <p><a href="/travel/getTravelList">旅行方式</a></p>
                        </div>
                    </div>
                </div>
                <div class="list-top"><a href="/userImage/getImageList">管理照片</a></div>
            </div><!--menu-->
        </div><!--right-->
        <div class="main-left"><!--left-->
            <div class="s-address">当前位置：<a href="">首页 </a>&gt;个人中心&gt;联系方式</div>
            <div class="safty">
                <form action="/userPersonal/addContact" method="post" onsubmit="return checkInsert()">
                    <p name="userId">
                        <span>会&nbsp;&nbsp;员 &nbsp;号：</span>
                        ${loginUser.userId}
                    </p>
                    <input type="hidden" name="userId" value="${loginUser.userId}">
                    <br>
                    <div class="tit">联系方式 >></div>
                    <ul class="safty-list">
                        <li>
                            <span>手 &nbsp;  机 &nbsp;  号：</span>
                            <input type="text" class="name-input" id="userPhone" onblur="checkPhone()" name="userPhone"
                                   value="${loginUser.userPhone}"/>
                        </li>
                        <li>
                            <span>Q  &nbsp; Q  &nbsp; 号：</span>
                            <input type="text" class="name-input" id="userQq" name="userQq" value="${loginUser.userQq}"
                                   onblur="checkQq()"/>
                        </li>
                        <li>
                            <span>微  &nbsp; 信 &nbsp;  号：</span>
                            <input type="text" class="name-input" id="userWx" onblur="checkWx()" name="userWx"
                                   value="${loginUser.userWx}"/>
                        </li>
                        <li>
                            <span>邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱：</span>
                            <input type="text" class="name-input" name="userEmail" id="userEmail" onblur="checkEmail()"
                                   value="${loginUser.userEmail}"/>
                        </li>
                        <li>
                            <span>现&nbsp;居&nbsp;住&nbsp;地：</span>
                            <select class="address-select" name="s_province" id="s_province">
                            </select>
                            省
                            <select class="address-select" name="s_city" id="s_city">
                            </select>
                            市
                            <select class="address-select" name="s_county" id="s_county">
                            </select>
                            区
                            <script class="resources library" src="/js/nownation.js" type="text/javascript"></script>
                            <c:set var="s_province" value="${userLocation.s_province}" scope="session"></c:set>
                            <c:if test="${s_province !='省份' && s_province!=''}">
                                <script>
                                    opt0[0] = "${s_province }"
                                </script>
                            </c:if>
                            <c:set var="s_city" value="${userLocation.s_city }" scope="session"></c:set>
                            <c:if test="${s_city !='地级市' && s_city!=''}">
                                <script>
                                    opt0[1] = "${s_city}"
                                </script>
                            </c:if>
                            <c:set var="s_county" value="${userLocation.s_county}" scope="session"></c:set>
                            <c:if test="${s_county !='县级市' && s_county!=''}">
                                <script>
                                    opt0[2] = "${s_county}"
                                    _init_area();
                                </script>
                            </c:if>
                        </li>
                    </ul>
                    <div class="btn-box">
                        <input type="submit" class="save-btn" value="保存并继续"/>
                    </div>
                </form>
            </div>
        </div><!--left-->
    </div>
    <div class="nav-box">
        <div class="nav">
            <a href="">网站首页</a>
            <a href="">了解我们</a>
            <a href="">搜索会员 </a>
            <a href="">婚恋课堂 </a>
            <a href="">活动专题 </a>
            <a href="">婚恋故事</a>
            <a href="">联系社区工作人员</a>
            <a href="">个人中心</a>
        </div>
    </div>
    <div class="copy">
        <p>COPYRIGHT 2015  南昌万家囍婚恋交友网  版权所有 赣ICP备15002274号-1</p>
        <p>地址：进贤县香江商业街27号  电话：138-7915-5305  技术支持：<a target="_blank" href="http://www.0791jr.com/">嘉瑞科技</a></p>
    </div>
    <!--在线客服-->
    <div class="chat_jsp">
        <iframe id="divFloatToolsView" src="/userFriend/UpChat"></iframe>
        <div id="floatTools" class="rides-cs" style="height:247px; ">
            <div class="floatL">
                <a id="aFloatTools_Show" class="btnOpen" title="打开聊天系统" style="top:20px;display:block" href="javascript:void(0);">展开</a>
                <a id="aFloatTools_Hide" class="btnCtn" title="关闭聊天系统" style="top:20px;display:none" href="javascript:void(0);">收缩</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(".css3-animated-example").collapse({
        accordion: true,
        open: function () {
            this.addClass("open");
            this.css({height: this.children().outerHeight()});
        },
        close: function () {
            this.css({height: "0px"});
            this.removeClass("open");
        }
    });
</script>

<script>


function checkInsert() {
    var checkPhone = checkPhone();
    var checkQq = checkQq();
    var checkWx = checkWx();
    var checkEmail = checkEmail();
    var isok = false;
    if (checkPhone == true && checkQq == true && checkWx == true && checkEmail == true){
        isok = true;
        alert("请输入正确的内容！")
    }
    return isok;
}



    function checkPhone() {
        var userPhone = $("#userPhone").val();
        var reg = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/;
        var isok = reg.test(userPhone);
        if (isok == false) {
            alert("请输入正确的手机号码！");
            $("#userPhone").val("");
        }
        return isok;
    }

    function checkQq() {
        var userQq = $("#userQq").val();
        var  reg = /[1-9]\d{4,14}/;
        var  isok = reg.test(userQq)
        if (isok == false) {
            alert("请输入正确的qq号！")
            $("#userQq").val("")
            return false;
        } else {
            return true;
        }

    }

    function checkWx() {
        var userWx = $("#userWx").val();
        var reg = /^[a-zA-Z][a-zA-Z0-9_-]{5,19}$/;
        var isok = reg.test(userWx)
        if (isok == false) {
            alert("请输入正确的微信号！")
            $("#userWx").val("")
            return false;
        } else {
            return true;
        }

    }

    function checkEmail() {
        var userEmail = $("#userEmail").val();
        var  reg =/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
        var isok = reg.test(userEmail);
        if (isok == false) {
            alert("请输入正确的邮箱！")
            $("#userEmail").val("")
            return false;
        } else {
            return true;
        }

    }



</script>

</body>