<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.rimi.bean.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>缘来是你婚恋交友网</title>
    <meta name="Keywords" content="缘来是你婚恋交友网"/>
    <meta name="Description" content="缘来是你婚恋交友网"/>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/js/koala.min.1.5.js"></script>
    <script type="text/javascript" src="/js/comment.js"></script>
    <script type="text/javascript" src="/js/jquery.collapse.js"></script>
    <script type="text/javascript" src="/js/chat.js"></script>
    <script type="text/javascript" src="/layer/layer.js"></script>
</head>
<body id="body">
<div class="head">
    <div class="top">
        <div class="top-right">
            <%
                User loginUser = (User) request.getSession().getAttribute("loginUser");
                if (loginUser == null) {
            %>
            <a href="/user/login">登录</a> |
            <a href="/user/reg">注册</a>
            <%
                }
            %>
            <%
                if (loginUser != null) {
            %>
            欢迎您: <font color="#00bfff">${loginUser.userRealname} </font>|
            <a href="/user/logout" onclick="return logout()"><font color="#dc143c">注销</font></a>
            <%
                }
            %>

        </div>

    </div>
</div>
<div class="nav-box">
    <div class="nav">
        <a href="index.jsp">网站首页</a>
        <a href="/user/firstUserList">条件搜索</a>
        <a href="/user/userInformation">快速匹配</a>
        <a href="sharemarry.jsp">婚恋故事</a>
        <a href="/userPersonal">个人中心</a>
    </div>
</div>
<%--<div class="banner"><img src="/images/banner.jpg"/></div>--%>
<div class="main">
    <div class="main-box">
        <div class="main-left"><!--left-->
            <div class="main-l-top">
                <div class="main-title">
                    人气会员<span>只为寻找有缘的你</span>
                </div>
            </div>
            <ul class="main-member">
                <c:if test="${firstUserList==null||firstUserList.size()<=0}">
                    <br>
                    <p>没有查到对应的用户哟，要不您把条件放宽一些再试试！</p>
                </c:if>
                <c:if test="${firstUserList!=null&&firstUserList.size()>0}">
                    <c:forEach var="a" items="${firstUserList}">
                        <li>
                            <a href="/leaguerPage/leaguerInfo?userid=${a.userId}">
                                <img src="${a.userImage}" />
                                <p class="mem-num">会员号：${a.userId}</p>
                                <p class="mem-text">
                                    <c:if test="${a.userBirthday!=null&&!''.equals(a.userBirthday)}">
                                        ${118-a.userBirthday.getYear()}岁
                                    </c:if>
                                    <c:if test="${a.userBirthday==null||''.equals(a.userBirthday)}">
                                        保密
                                    </c:if>
                                    <c:if test="${a.userEnucation!=null&&!''.equals(a.userEnucation)}">
                                        ${a.userEnucation}
                                    </c:if>
                                    <c:if test="${a.userEnucation==null||''.equals(a.userEnucation)}">
                                        保密
                                    </c:if>
                                    |
                                    <c:if test="${a.userHeight!=null&&a.userHeight!=0}">
                                        ${a.userHeight}CM
                                    </c:if>
                                    <c:if test="${a.userHeight==null||a.userHeight==0}">
                                        保密
                                    </c:if>
                                    <br />
                                    <c:if test="${a.userMarry!=null&&!''.equals(a.userMarry)}">
                                        ${a.userMarry}
                                    </c:if>
                                    <c:if test="${a.userMarry==null||''.equals(a.userMarry)}">
                                        保密
                                    </c:if>
                                    |
                                    <c:if test="${a.userHouse!=null&&!''.equals(a.userHouse)}">
                                        ${a.userHouse}
                                    </c:if>
                                    <c:if test="${a.userHouse==null||''.equals(a.userHouse)}">
                                        保密
                                    </c:if>
                                </p>
                            </a>
                        </li>
                    </c:forEach>
                </c:if>
            </ul>
            <div class="main-title">
                <c:if test="${loginUser.userSex=='男'}">
                近期熟女<span>全城为您找到最靠谱的美女</span>
                </c:if>
                <c:if test="${loginUser.userSex=='女'}">
                近期熟男<span>全城为您找到最靠谱的帅哥</span>:
                </c:if>
                <a href="/user/firstUserList">更多</a>
            </div>
            <ul class="main-member">
                <c:if test="${falshUserList==null||falshUserList.size()<=0}">
                    <br>
                    <p>没有查到对应的用户哟，要不您把条件放宽一些再试试！</p>
                </c:if>
                <c:if test="${falshUserList!=null&&falshUserList.size()>0}">
                    <c:forEach var="a" items="${falshUserList}">
                        <li>
                            <a href="/leaguerPage/leaguerInfo?userid=${a.userId}">
                                <img src="${a.userImage}" />
                                <p class="mem-num">会员号：${a.userId}</p>
                                <p class="mem-text">
                                    <c:if test="${a.userBirthday!=null&&!''.equals(a.userBirthday)}">
                                        ${118-a.userBirthday.getYear()}岁
                                    </c:if>
                                    <c:if test="${a.userBirthday==null||''.equals(a.userBirthday)}">
                                        保密
                                    </c:if>
                                    |
                                    <c:if test="${a.userEnucation!=null&&!''.equals(a.userEnucation)}">
                                        ${a.userEnucation}
                                    </c:if>
                                    <c:if test="${a.userEnucation==null||''.equals(a.userEnucation)}">
                                        保密
                                    </c:if>
                                    |
                                    <c:if test="${a.userHeight!=null&&a.userHeight!=0}">
                                        ${a.userHeight}CM
                                    </c:if>
                                    <c:if test="${a.userHeight==null||a.userHeight==0}">
                                        保密
                                    </c:if>
                                    <br />
                                    <c:if test="${a.userMarry!=null&&!''.equals(a.userMarry)}">
                                        ${a.userMarry}
                                    </c:if>
                                    <c:if test="${a.userMarry==null||''.equals(a.userMarry)}">
                                        保密
                                    </c:if>
                                    |
                                    <c:if test="${a.userHouse!=null&&!''.equals(a.userHouse)}">
                                        ${a.userHouse}
                                    </c:if>
                                    <c:if test="${a.userHouse==null||''.equals(a.userHouse)}">
                                        保密
                                    </c:if>
                                </p>
                            </a>
                        </li>
                    </c:forEach>
                </c:if>
            </ul>
            <%--<div class="main-title">--%>
            <%--友情链接<span>以下商家为“万家囍”合作伙伴</span>--%>
            <%--<a href="">更多></a>--%>
            <%--</div>--%>
            <%--<ul class="friend-link">--%>
            <%--<li><a href=""><img src="/images/link3.png"/></a></li>--%>
            <%--<li><a href=""><img src="/images/link1.png"/></a></li>--%>
            <%--<li><a href=""><img src="/images/link4.png"/></a></li>--%>
            <%--<li><a href=""><img src="/images/link2.png"/></a></li>--%>
            <%--</ul>--%>
        </div><!--left-->
        <div class="main-right"><!--right-->
            <%--<div class="main-log">--%>
            <%--<div class="tit">会员登录</div>--%>
            <%--<div class="main-logbox">--%>
            <%--<i class="main-user"></i>--%>
            <%--<input placeholder="账户名" type="text" class="main-user-input" />--%>
            <%--</div>--%>
            <%--<div class="main-logbox">--%>
            <%--<i class="main-password"></i>--%>
            <%--<input placeholder="密码" type="text" class="main-user-input" />--%>
            <%--</div>--%>
            <%--<button type="button"  class="main-btn">立即登录</button>--%>
            <%--<div class="main-pass-text">没有帐号？<a href="reg.jsp">免费注册</a><a href="" class="forget">忘记密码 ></a></div>--%>
            <%--</div>--%>
            <div class="main-radv"><img src="/images/adv2.png"/></div>
            <div class="main-radv"><img src="/images/adv1.png"/></div>
            <div class="main-message">
                <div class="tit">通告栏</div>
                <p>一、请各用电客户尽快与开户银行联系，按照中国人民银行XX分行的统一要求签订《定期借记业务授权委托书》，并于2002年1月31日前将复印件送达所在区供电局。</p>
                <p>二、部分商业银行由于系统升级原因更改了开户银行账号格式，请客户在签订《定期借记业务授权委托书》的同时与贵开户银行确认新的银行账号，并于2002年1月31日前以正式公函形式通知我局，若届时未受到客户的《定期借记业务授权委托书》及新的银行帐号的，客户将无法使用定期借记方式缴交电费，我局将采取现金方式收取电费。</p>
                <p>三、由于定期借记业务系统投运后，银行系统将不再代为传递电费票据，故我局将统一采取邮政递送，客户签收的方式派发系统将不再代为传递电费票据，故我局将统一采取邮政递送，客户签收的方式派发</p>
            </div>
            <%--<div class="main-message1">--%>
            <%--<div class="tit">联系我们</div>--%>
            <%--<div class="main-contact">--%>
            <%--<p>公司名称：</p>--%>
            <%--<p>联系人：</p>--%>
            <%--<p>联系电话：400-400-400</p>--%>
            <%--<p>电子邮箱：</p>--%>
            <%--<p>地   址：成都市高新区</p>--%>
            <%--<p>xxxx婚恋交友官方微信</p>--%>
            <%--<p><img src="/images/weixin.jpg"/></p>--%>
            <%--<p style="text-align: center;">“ 扫一扫与万家喜亲密互动 ”</p>--%>
            <%--</div>--%>
            <%--</div>--%>
        </div><!--right-->
    </div>
</div>
<!--在线客服-->
<div class="chat_jsp">
    <iframe id="divFloatToolsView" src="/userFriend/UpChat"></iframe>
    <div id="floatTools" class="rides-cs" style="height:247px; ">
        <div class="floatL">
            <a id="aFloatTools_Show" class="btnOpen" title="打开聊天系统" style="top:20px;display:block" href="javascript:void(0);">展开</a>
            <a id="aFloatTools_Hide" class="btnCtn" title="关闭聊天系统" style="top:20px;display:none" href="javascript:void(0);">收缩</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    <%--注销--%>
    function logout() {
        if (confirm("是否注销？")) {
            return true;
        } else {
            return false;
        }
    }
    Qfast.add('widgets', { path:"/js/terminator2.2.min.js", type: "js", requires: ['fx'] });
    Qfast(false, 'widgets', function () {
        K.tabs({
            id: 'fsD1',
            conId: "D1pic1",
            tabId:"D1fBt",
            tabTn:"a",
            conCn: '.fcon',
            auto: 1,
            effect: 'fade',
            eType: 'click',
            pageBt:true,
            bns: ['.prev', '.next'],
            interval: 3000
        })
    });
</script>
</body>
</html>
