<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>缘来是你婚恋交友网</title>
    <meta name="Keywords" content="缘来是你婚恋交友网"/>
    <meta name="Description" content="缘来是你婚恋交友网"/>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <style>
        .main-log {
            height: 390px;
            width: 350px;
            margin-bottom: 30px;
            background: #fff;
        }

        .main-logbox {
            width: 300px;
        }

        .main-user-input {
            width: 270px;
        }

        .main-log .tit {
            height: 50px;
            line-height: 50px;
            font-size: 24px;
        }

        .login-left {
            width: 650px;
        }

        .login-left img {
            margin: 90px 0px 0px 200px;
        }
    </style>
</head>
<body>
<div class="head">
    <div class="top">
        <div class="top-right">
            <a href="/user/index">首页</a> |
            <a href="/user/index">帮助</a>
        </div>
    </div>
</div>
<div class="nav-box">
    <div class="nav">
        <%--<a href="index.jsp">网站首页</a>--%>
        <%--<a href="searchmember.jsp">快速匹配</a>--%>
        <%--<a href="sharemarry.jsp">婚恋故事</a>--%>
        <%--<a href="infor.jsp">个人中心</a>--%>
    </div>
</div>
<div class="main">
    <div class="main-box1">
        <div class="login-left"><img src="/images/loginpic.png"/></div>
        <div class="login-right">
            <div class="main-log">

                <div class="tit">免费注册</div>
                <form id="fm" action="/user/reg" method="post" onsubmit="return checkSubmit()">

                    <div class="main-logbox">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        请选择您的性别：<br>
                        &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" id="male" name="userSex" value="男" checked/>男
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" id="female" name="userSex" value="女"/>女
                    </div>

                    <div class="main-logbox">
                        <i class="main-user1"></i>
                        <input placeholder="注册邮箱" id="account" name="userAccount" type="text"
                               class="main-user-input" onblur="checkAccount()">
                    </div>
                    <div class="main-logbox">
                        <i class="main-password"></i>
                        <input placeholder="设置密码" id="password" onblur="checkPs()" name="userPassword" type="password"
                               class="main-user-input" onpaste="return false" oncontextmenu="return false">
                        <%--onpaste="return false" oncontextmenu = "return false" 表示禁用粘贴，且不会弹出粘贴菜单--%>
                    </div>
                    <div class="main-logbox">
                        <i class="main-password"></i>
                        <input placeholder="再次输入密码" id="password2" type="password"
                               class="main-user-input" onblur="checkPs2()"
                               onpaste="return false" oncontextmenu="return false">
                    </div>

                    <div class="main-logbox" style="border: 0;">
                        <input type="text" id="code" name="code" placeholder="请输入验证码" onblur="checkCode()"
                               class="yzm">
                        <button type="button" id="sendcode" onclick="sendMail()">获取验证码</button>
                        <button type="button" id="sendcode2" hidden disabled>60秒后重新发送</button>
                    </div>

                    <%--<div class="remember"></div>--%>
                    <div class="login-btn">
                        <button type="submit">立即注册</button>
                        <a href="/user/login">立即登录 ></a>
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>
</body>

<script src="/js/jquery-3.3.1.js"></script>
<script src="/layer/layer.js"></script>
<script src="/js/test.js"></script>
<script>

    <%--存储系统生成验证码--%>
    var isSendCode = "";
    // 验证码是否相同
    var codeIsEq = "";

    //当前剩余秒数
    var curCount;
    <%--服务器发送验证码到注册邮箱--%>

    function sendMail() {

        //获取输入框账号
        var account = $("#account").val();
        if (checkAccount()) {

            $.ajax({
                    type: "post",
                    url: "/user/getcode",
                    data: "account=" + account,
                    async: false,//同步
                    success: function (msg) {//请求成功返回值
                        isSendCode = msg;
                    }
                }
            );

            if (isSendCode = "1") { //发送验证码成功
                layer.msg("验证码发送成功！");

                curCount = 60;
                // 隐藏发送验证码按钮
                $("#sendcode").hide();
                //显示倒计时按钮
                $("#sendcode2").show();

                //启动计时器，1秒执行一次
                var interval = window.setInterval(SetRemainTime, 1000);

            } else {
                layer.msg("验证码发送失败");
            }
        }
    }


    //timer处理函数
    function SetRemainTime() {
        if (curCount == 0) {
            $("#sendcode").show();  // 启用发送验证码按钮
            $("#sendcode2").hide();// 禁用倒计时按钮
            window.clearInterval(interval);//停止计时器
        } else {
            curCount--;
            // 显示剩余秒数
            $("#sendcode2").text(curCount + "秒后重新发送");
        }
    }


    <%--表单提交时要验证账号密码框--%>

    function checkSubmit() {
        var ps = $("#password").val();
        var ps2 = $("#password2").val();

        if (checkCode() && checkPassword(ps) && checkPassword(ps2)) {
            return true;
        } else {
            return false;
        }
    }


    <%--验证密码框非空和符合邮箱规则--%>

    function checkAccount() {

        // 返回值赋给res
        var res = "";
        var account = $("#account").val();//获得输入框值

        if (account == null || account == "") { // 非空验证
            layer.msg("注册账号不能为空！");
            return false;
        } else {// 进入else说明不为空
            var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
            emailIsMatch = reg.test(account);//判断邮箱是否匹配规则

            if (!emailIsMatch) {
                layer.msg("邮箱格式不正确!");
                return false;
            } else { // 邮箱格式正确通过ajax校验账号是否已经注册

                $.ajax({
                        type: "post",
                        url: "/user/checkAccount",
                        data: "account=" + account,
                        async: false,//同步：使用异步外部获取不到返回值
                        success: function (msg) {//请求成功返回值
                            res = msg;
                        }

                    }
                );

                if (res == "1") {
                    layer.msg("该账号已经注册！");
                    return false;
                } else {
                    return true;
                }

            }
        }

    }


    <%--密码输入框失去焦点事件--%>

    function checkPs() {
        var ps = $("#password").val();//获取密码输入框的值
        checkPassword(ps);
    }

    function checkPs2() {
        var ps = $("#password2").val();//获取确认密码输入框的值
        checkPassword(ps);
    }

    // 验证验证码
    function checkCode() {

        //获取输入框验证码
        var inputCode = $("#code").val();

        if (inputCode == null || inputCode == "") {
            layer.msg("请输入验证码");
            return false;
        }
        if (isSendCode != 1) {
            layer.msg("请发送验证码");
            return false;
        } else {
            // 判断输入验证码是否正确
            $.ajax({
                    type: "post",
                    url: "/user/checkCode",
                    data: "inputCode=" + inputCode,
                    async: false,//同步：使用异步外部获取不到返回值
                    success: function (msg) {//请求成功返回值
                        codeIsEq = msg;
                    }

                }
            );

            if (codeIsEq == 1) {// 返回值为1表示验证码相同
                return true;
            } else if (codeIsEq == -1) {
                layer.msg("验证码已超时，请重新发送验证码！");
                return false;
            } else {
                layer.msg("验证码错误！");
                return false;
            }
        }

    }

</script>

</html>
