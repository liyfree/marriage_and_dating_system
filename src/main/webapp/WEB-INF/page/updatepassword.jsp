<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>缘来是你婚恋交友网</title>
    <meta name="Keywords" content="缘来是你婚恋交友网"/>
    <meta name="Description" content="缘来是你婚恋交友网"/>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/js/comment.js"></script>
    <script type="text/javascript" src="/js/jquery.collapse.js"></script>
</head>

<body>
<div class="head">
    <div class="top">
        <%--<div class="top-left">--%>
        <%--<a href="javascript:;" class="top-coll" onclick="AddFavorite('', 'http://www.ncwjxhljyw.com')">收藏万家喜</a> |--%>
        <%--<a href="" class="top-att">关注万家喜</a>--%>
        <%--</div>--%>
        <div class="top-right">
            <a href="">注册</a> |
            <a href="">登录</a>
        </div>
    </div>

</div>
<div class="nav-box">
    <div class="nav">
        <a href="/user/chatindex">网站首页</a>
        <a href="/user/firstUserList">条件搜索</a>
        <a href="/user/userInformation">快速匹配</a>
        <a href="/sotry/index">婚恋故事</a>
        <a href="/userPersonal">个人中心</a>

    </div>
</div>
<div class="main">

    <div class="main-left"><!--left-->
        <%--<div class="s-address">当前位置：<a href="">首页 </a>&gt;个人中心&gt;安全认证</div>--%>
        <div class="safty">
            <div class="tit">修改密码</div>
            <%--<form action="/user/updatePs" method="post" onsubmit="return checkAll()">--%>
            <%--<ul class="safty-list">--%>
            <%--<li>--%>
            <%--<span><i>*</i>要修改的账号</span>--%>
            <%--<input id="account" name="account" type="text" value="${updateAccount}" readonly--%>
            <%--class="name-input"/>--%>
            <%--</li>--%>
            <%--<li>--%>
            <%--<span><i>*</i>新密码：</span>--%>
            <%--<input id="newPs" name="newPs" type="text" class="name-input" onblur="checkPs()"/>--%>
            <%--</li>--%>
            <%--<li>--%>
            <%--<span><i>*</i>确认新密码：</span>--%>
            <%--<input id="confirmPs" onblur="checkPs2()" name="confirmPs" type="text" class="name-input"/>--%>
            <%--</li>--%>
            <%--</ul>--%>

            <%--<div class="btn-box">--%>
            <%--<button type="submit" class="save-btn">提交修改</button>--%>
            <%--</div>--%>
            <%--</form>--%>
            <ul class="safty-list">
                <li>
                    <span><i>*</i>要修改的账号</span>
                    <input id="account" name="account" type="text" value="${updateAccount}" readonly
                           class="name-input"/>
                </li>
                <li>
                    <span><i>*</i>新密码：</span>
                    <input id="newPs" name="newPs" type="text" class="name-input" onblur="checkPs()"/>
                </li>
                <li>
                    <span><i>*</i>确认新密码：</span>
                    <input id="confirmPs" onblur="checkPs2()" name="confirmPs" type="text" class="name-input"/>
                </li>
            </ul>

            <div class="btn-box">
                <button type="button" onclick="checkAll()" class="save-btn">提交修改</button>
            </div>

        </div>
    </div>
</div><!--left-->
</div>

</div>
<script src="/js/jquery-3.3.1.js"></script>
<script src="/layer/layer.js"></script>
<script src="/js/test.js"></script>
<script>

    <%--提交事件--%>
    function checkAll() {
        var account = $("#account").val();
        var newPs = $("#newPs").val();
        var confirmPs = $("#confirmPs").val();
        if (checkPassword(newPs) && checkPassword(confirmPs)) {
            if (newPs != confirmPs) {
                layer.msg("两次密码不一样");
            } else {
                //执行这里说明两次密码符合要求，异步提交
                $.ajax({
                        type: "post",
                        url: "/user/updatePs",
                    data: "userAccount=" + account + "&userPassword=" + newPs,
                        async: false,//同步
                        success: function (msg) {//请求成功返回值
                            if (msg == 1) {
                                alert("修改成功");
                                //修改成功跳转到登录页面
                                window.location.href = "http://192.168.0.108:8080/user/index";
                            } else {
                                layer.msg("修改失败")
                            }
                        }
                    }
                );
            }
        } 
    }

    //密码框失去焦点事件
    function checkPs() {
        var ps = $("#newPs").val();
        checkPassword(ps);
    }

    function checkPs2() {
        var ps = $("#confirmPs").val();
        checkPassword(ps);
    }

</script>
<script>
    $(".css3-animated-example").collapse({
        accordion: true,
        open: function () {
            this.addClass("open");
            this.css({height: this.children().outerHeight()});
        },
        close: function () {
            this.css({height: "0px"});
            this.removeClass("open");
        }
    });
</script>
</body>
</html>
