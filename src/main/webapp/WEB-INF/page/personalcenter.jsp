<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>南昌万家喜婚恋交友网</title>
    <meta name="Keywords" content="南昌万家喜婚恋交友网"/>
    <meta name="Description" content="南昌万家喜婚恋交友网"/>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/js/comment.js"></script>
    <script type="text/javascript" src="/js/jquery.collapse.js"></script>
    <script language="javascript" type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
</head>
<body>
<div class="head">
    <div class="top">
        <div class="top-left">
            <a href="javascript:;" class="top-coll" onclick="AddFavorite('', 'http://www.ncwjxhljyw.com')">收藏万家喜</a> |
            <a href="" class="top-att">关注万家喜</a>
        </div>
        <div class="top-right">
            <a href="">注册</a> |
            <a href="">登录</a>
        </div>
    </div>
    <div class="top-ban">
        <div class="top-mid-box">
            <a href="" class="logo"><img src="/images/logo.png"/></a>
            <div class="adv"><img src="/images/adv.png"/></div>
            <div class="tele"><img src="/images/tele.png"/></div>
            <div class="erweima"><img src="/images/erwei.png"/></div>
        </div>
    </div>
</div>
<div class="nav-box">
    <div class="nav">
        <a href="/user/chatindex">网站首页</a>
        <a href="/user/firstUserList">条件搜索</a>
        <a href="/user/userInformation">快速匹配</a>
        <a href="/sotry/index">婚恋故事</a>
        <a href="/userPersonal">个人中心</a>
    </div>
</div>
<div class="main">
    <div class="main-box1">
        <div class="main-right" style="margin-left: 0; margin-right: 14px;"><!--right-->
            <div class="list-main1-title">个人中心</div>
            <div class="col c2"><!--menu-->
                <div class="list-top"><a href="/userPersonal" class="cur">基本资料</a></div>
                <div class="list-top"><a href="/userPersonal/addcontactPage">联系方式</a></div>
                <div class="list-top"><a href="/likeUser/otherHalf">择偶条件</a></div>
                <div class="list-top"><a href="/userImage/imageShow">我的照片</a></div>
                <div class="css3-animated-example">
                    <h3>兴趣爱好</h3>
                    <div>
                        <div class="content">
                            <p><a href="/lifeStyle/getLifeStyleList">生活方式</a></p>
                            <p><a href="/petHobby/getHobbyList">喜欢宠物</a></p>
                            <p><a href="/foodHabit/getFoodHabitList">饮食习惯</a></p>
                            <p><a href="/leisureStyle/getLeisureList">休闲方式</a></p>
                            <p><a href="/bodyBuilding/getBodyBuildList">运动健身</a></p>
                            <p><a href="/outdoorSport/getOutdoorSpotrList">户外运动</a></p>
                            <p><a href="/travel/getTravelList">旅行方式</a></p>
                        </div>
                    </div>
                </div>
                <div class="list-top"><a href="/userImage/getImageList">管理照片</a></div>
            </div><!--menu-->
        </div><!--right-->
        <div class="main-left"><!--left-->
            <div class="s-address">当前位置：<a href="">首页 </a>&gt;个人中心&gt;基本资料</div>
            <div class="safty">
                <form action="/userPersonal/addBasicMsg" method="post" onsubmit="return checkSubmit()">
                    <p name="userId">
                        <span>会&nbsp;&nbsp;员 &nbsp;号：</span>
                        ${loginUser.userId}
                    </p>
                    <input type="hidden" name="userId" value="${loginUser.userId}">
                    <br>
                    <div class="tit">个人资料 >></div>
                    <ul class="condition-list">
                        <li>
                            <p>
                                <span>昵&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 称：</span>
                                <input class="conditon-select" type="text" name="userRealname"
                                       value="${loginUser.userRealname}" placeholder="请输入昵称" id="userName"/>
                            </p>
                            <p>
                                <span>出生年月：</span>
                                <input class="conditon-select" type="text" name="birthday"
                                       onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" placeholder="请选择出生年月"
                                       value="<fm:formatDate value="${loginUser.userBirthday}" type="date"  pattern="yyyy-MM-dd"/>"/>
                            </p>

                            <p>
                                <span>婚姻状况：</span>
                                <select class="conditon-select" name="userMarry">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userMarry=='已婚'?'selected':''}>已婚</option>
                                    <option ${loginUser.userMarry=='未婚'?'selected':''}>未婚</option>
                                    <option ${loginUser.userMarry=='离婚'?'selected':''}>离婚</option>
                                    <option ${loginUser.userMarry=='丧偶'?'selected':''}>丧偶</option>
                                </select>
                            </p>
                            <p>
                                <span>  是否有车： </span>
                                <select class="conditon-select" name="userCar">
                                    <option>请选择</option>
                                    <option ${loginUser.userCar=='有，有车贷'?'selected':''}>有，有车贷</option>
                                    <option ${loginUser.userCar=='有，无车贷'?'selected':''}>有，无车贷</option>
                                    <option ${loginUser.userCar=='无'?'selected':''}>无</option>
                                </select>
                            </p>
                            <p>
                                <span>  是否抽烟： </span>
                                <select class="conditon-select" name="userSmoke">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userSmoke=='是'?'selected':''}>是</option>
                                    <option ${loginUser.userSmoke=='否'?'selected':''}>否</option>
                                </select>
                            </p>
                            <p>
                                <span>  父母情况： </span>
                                <select class="conditon-select" name="userParent">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userParent=='单亲'?'selected':''}>单亲</option>
                                    <option ${loginUser.userParent=='双亲'?'selected':''}>双亲</option>
                                    <option ${loginUser.userParent=='已去世'?'selected':''}>已去世</option>
                                </select>
                            </p>
                        </li>

                        <li>
                            <p>
                                <span> 性 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别： </span>
                                <select class="conditon-select" name="userSex">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userSex=='男'?'selected':''}>男</option>
                                    <option ${loginUser.userSex=='女'?'selected':''}>女</option>
                                </select>
                            </p>


                            <p>
                                <span>民 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;族： </span>
                                <select class="conditon-select" name="userNation">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userNation=='汉族'?'selected':''}>汉族</option>
                                    <option ${loginUser.userNation=='其他'?'selected':''}>其他</option>
                                </select>
                            </p>
                            <p>
                                <span>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 校： </span>
                                <input class="conditon-select" type="text" name="userSchool"
                                       value="${loginUser.userSchool}" placeholder="请输入毕业院校">
                            </p>
                            <p>
                                <span>  是否有房： </span>
                                <select class="conditon-select" name="userHouse">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userHouse=='有，有房贷'?'selected':''}>有，有房贷</option>
                                    <option ${loginUser.userHouse=='有，无房贷'?'selected':''}>有，无房贷</option>
                                    <option ${loginUser.userHouse=='无'?'selected':''}>无</option>
                                </select>
                            </p>
                            <p>
                                <span>  是否喝酒： </span>
                                <select class="conditon-select" name="userDrink">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userDrink=='是'?'selected':''}>是</option>
                                    <option ${loginUser.userDrink=='否'?'selected':''}>否</option>
                                </select>
                            </p>
                            <p>
                                <span>  子女情况： </span>
                                <select class="conditon-select" name="userChild">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userChild=='无'?'selected':''}>无</option>
                                    <option ${loginUser.userChild=='有，住一起'?'selected':''}>有，住一起</option>
                                    <option ${loginUser.userChild=='有，未住一起'?'selected':''}>有，未住一起</option>
                                </select>
                            </p>
                        </li>
                        <li>
                            <p>
                                <span>身&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;高：</span>
                                <input class="conditon-select" type="text" id="insertHeight" name="userHeight"
                                       placeholder="请输入身高" style="width: 100px"
                                       value="<fm:formatNumber value='${loginUser.userHeight}' pattern='#' type='number'/>"
                                       onblur="heightIsInt()"/>
                                <span>cm</span>
                            </p>
                            <p>
                                <span>体&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 重：</span>
                                <input class="conditon-select" type="text" id="insertWeight" name="userWeight"
                                       placeholder="请输入体重"
                                       style="width: 100px"
                                       value="<fm:formatNumber value='${loginUser.userWeight}' pattern='#' type='number'/>"
                                       onblur="weightIsInt()"/>
                                <span>kg</span>
                            </p>
                            <p>
                                <span>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 历： </span>
                                <select class="conditon-select" name="userEnucation" style="width: 100px">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userEnucation=='高中及以下'?'selected':''}>高中及以下</option>
                                    <option ${loginUser.userEnucation=='大专'?'selected':''}>大专</option>
                                    <option ${loginUser.userEnucation=='本科'?'selected':''}>本科</option>
                                    <option ${loginUser.userEnucation=='研究生'?'selected':''}>研究生</option>
                                    <option ${loginUser.userEnucation=='硕士'?'selected':''}>硕士</option>
                                    <option ${loginUser.userEnucation=='博士及以上'?'selected':''}>博士及以上</option>
                                </select>
                            </p>
                            <p>
                                <span>月&nbsp;&nbsp;&nbsp;收&nbsp;&nbsp; &nbsp;入：</span>
                                <input class="conditon-select" type="text" name="userIncome" id="insertIncome"
                                       placeholder="请输入月收入"
                                       style="width: 100px" value="${loginUser.userIncome}" onblur="incomeIsInt()"/>
                                <span>k</span>
                            </p>
                            <p>
                                <span>  是否要小孩： </span>
                                <select class="conditon-select" name="userWangchild" style="width: 100px">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userWangchild=='是'?'selected':''}>是</option>
                                    <option ${loginUser.userWangchild=='否'?'selected':''}>否</option>
                                </select>
                            </p>
                            <p>
                                <span>血&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;型：</span>
                                <select class="conditon-select" name="userBlood" style="width: 100px">
                                    <option value="">请选择</option>
                                    <option ${loginUser.userBlood=='A'?'selected':''}>A</option>
                                    <option ${loginUser.userBlood=='B'?'selected':''}>B</option>
                                    <option ${loginUser.userBlood=='AB'?'selected':''}>AB</option>
                                    <option ${loginUser.userBlood=='O'?'selected':''}>O</option>
                                </select>
                                <span>型</span>
                            </p>
                        </li>
                    </ul>

                    <ul class="address" style="margin-top: 0;">
                        <li>
                            <p>
                                <span> 户 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;籍：</span>
                                <select class="address-select" name="s_province" id="s_province">

                                </select>
                                省
                                <select class="address-select" name="s_city" id="s_city">

                                </select>
                                市
                                <select class="address-select" name="s_county" id="s_county">

                                </select>
                                区
                            </p>
                            <script src="/js/nownation.js" type="text/javascript"></script>
                            <c:set var="s_province" value="${loginUserFromPlace.s_province}" scope="session"></c:set>
                            <c:if test="${s_province !='省份'}">
                                <script>
                                    opt0[0] = "${s_province}"
                                </script>
                            </c:if>
                            <c:set var="s_city" value="${loginUserFromPlace.s_city}" scope="session"></c:set>
                            <c:if test="${s_city !='地级市'}">
                                <script>
                                    opt0[1] = "${s_city}"
                                </script>
                            </c:if>
                            <c:set var="s_county" value="${loginUserFromPlace.s_county}" scope="session"></c:set>
                            <c:if test="${s_county !='县级市'}">
                                <script>
                                    opt0[2] = "${s_county}"
                                    _init_area();
                                </script>
                            </c:if>
                        </li>
                    </ul>
                    <div class="btn-box">
                        <input type="submit" class="save-btn" value="保存并继续"/>
                    </div>
                </form>
            </div>
        </div><!--left-->
    </div>
    <div class="nav-box">
        <div class="nav">
            <a href="/user/chatindex">网站首页</a>
            <a href="/user/firstUserList">条件搜索</a>
            <a href="/user/userInformation">快速匹配</a>
            <a href="/sotry/index">婚恋故事</a>
            <a href="/userPersonal">个人中心</a>
        </div>
    </div>
    <div class="copy">
        <p>COPYRIGHT 2015  南昌万家囍婚恋交友网  版权所有 赣ICP备15002274号-1</p>
        <p>地址：进贤县香江商业街27号  电话：138-7915-5305  技术支持：<a target="_blank" href="http://www.0791jr.com/">嘉瑞科技</a></p>
    </div>
    <!--在线客服-->
    <div class="chat_jsp">
        <iframe id="divFloatToolsView" src="/userFriend/UpChat"></iframe>
        <div id="floatTools" class="rides-cs" style="height:247px; ">
            <div class="floatL">
                <a id="aFloatTools_Show" class="btnOpen" title="打开聊天系统" style="top:20px;display:block" href="javascript:void(0);">展开</a>
                <a id="aFloatTools_Hide" class="btnCtn" title="关闭聊天系统" style="top:20px;display:none" href="javascript:void(0);">收缩</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(".css3-animated-example").collapse({
        accordion: true,
        open: function () {
            this.addClass("open");
            this.css({height: this.children().outerHeight()});
        },
        close: function () {
            this.css({height: "0px"});
            // this.removeClass("open");
        }
    });
</script>
<script>

    function checkSubmit() {
        var userName = $("#userName").val();
        var aa = heightIsInt();
        var bb = weightIsInt();
        var cc = incomeIsInt();
        var isok = false;
        if (aa == true && bb == true && cc == true && userName!="") {
            isok = true;
        }
        if (userName == ""){
            alert("请输入用户名！")
        }
        return isok;


    }

    <%--判断输入的是数字--%>

    function isInteger(data) {
        var checkInsert = true;
        var reg = /^[1-9]\d*$/;
        var isok = reg.test(data);
        if (isok == false) {
            alert("请输入正确的数字")
            checkInsert = false;
        }
        return checkInsert;
    }


    function heightIsInt() {
        var height = $("#insertHeight").val();
        return isInteger(height);
    }

    function weightIsInt() {
        var weight = $("#insertWeight").val();
        return isInteger(weight);
    }

    function incomeIsInt() {
        var income = $("#insertIncome").val();
        return isInteger(income);
    }


</script>

</body>