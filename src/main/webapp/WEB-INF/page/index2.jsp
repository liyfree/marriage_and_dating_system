<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 张哥
  Date: 2018/6/15
  Time: 10:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/js/koala.min.1.5.js"></script>
    <script type="text/javascript" src="/js/comment.js"></script>
    <script type="text/javascript" src="/js/jquery.collapse.js"></script>
    <script type="text/javascript" src="/js/chat.js"></script>
</head>
<div>
    <div class="jilu" id="jilu">
        <div style="width: 450px;height: 20px;">
            <div class="guanbi" onclick="su('jilu')"><img src="/images/close.png"></div>
        </div>
        <div class="xiaoxi">
            <ul id="xiaoxi">
            </ul>
        </div>
        <div class="pages" id="pages">
            <input type="hidden" value="1" id="page">
        </div>
    </div>
    <div class="kuang" id="kuangj" style="display: none">
        <div class="admin-mess">
            <div class="tou" id="tou">
            </div>
            <div class="mess" id="mess">
            </div>
            <div class="guanbi" onclick="su('kuangj')"><img src="/images/close.png"></div>
            <div class="somes" id="somes">
            </div>
            <div id="image"></div>
        </div>
        <div class="am-chat">
            <!-- 聊天区 -->
            <div class="am-scrollable-vertical" id="chat-view">
                <ul class="am-comments-list" id="chat">
                </ul>
            </div>
            <!-- 输入区 -->
            <div class="am-form-group">
                <div class="am-form-buton">
                    <button onclick="chatmessagelist()">历史消息</button>
                    <button onclick="send()">发送</button>
                </div>
                <div class="am-from-textare">
                    <textarea class="am-text" id="message" name="message"  rows="5"  onkeydown="huiche()"></textarea>
                </div>
            </div>
        </div>
        <div class="imgs"></div>
    </div>
    <div class="floatR">
        <div class="titZx">
            <div class="tou" >
                <img src="${userimage.imageUrl}"/>
            </div>
            <div class="mess" >
                <p>${loginUser.userRealname}</p>
            </div>
        </div>
        <div class="titZxy">
            <button class="xialas" onclick="su('1')" >最新动态</button>
            <div id="1"  style="display: none">
                <ul id="UserChat">

                </ul>
            </div>
            <button class="xialas" onclick="su('2')">好友列表</button>
            <div id="2" style="display: none">
                <ul id="Userlist">

                </ul>
            </div>
        </div>
    </div>
    <script>
        UserChat(${loginUser.userId});
        Userlist(${loginUser.userId});
        function chatmessagelist() {
            var ToSendUserno = document.getElementById('userid').value;
            var page = document.getElementById('page').value;
            chatmessagelists(${loginUser.userId},ToSendUserno,page)
        }
        function su(a) {
            var div = document.getElementById(a).style.display;
            if (div == 'none') {
                document.getElementById(a).style.display = 'block';
            } else {
                if (a =="jilu") {
                document.getElementById("pages").innerHTML = "<input type='hidden' value='1' id='page'>";
                }
                if (a=="kuangj") {
                    document.getElementById("chat").innerHTML = "";
                }
                document.getElementById(a).style.display = 'none';
            }
        };
        var websocket = null;
        //判断当前浏览器是否支持WebSocket
        if('WebSocket' in window){
            websocket = new WebSocket("ws://192.168.0.106/client/${loginUser.userId}");
        }
        else{
            alert('你的浏览器不支持这个功能')
        }
        //连接发生错误的回调方法
        websocket.onerror = function(){
            setMessageInnerHTML("与聊天服务器断开连接,请刷新页面在进行聊天");
        };

        //连接成功建立的回调方法
        websocket.onopen = function(event){
        // setMessageInnerHTML("open")
        }
        //接收到消息的回调方法
        websocket.onmessage = function(event){
            showChat(event.data)
        }


        //连接关闭的回调方法
        websocket.onclose = function(){
            // setMessageInnerHTML("-_-")
        }


        //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
        window.onbeforeunload = function(){
            websocket.close();
        }


        //将消息显示在网页上
        function setMessageInnerHTML(innerHTML){
            document.getElementById('message').innerHTML += alert(innerHTML);
        }


        //关闭连接
        function closeWebSocket(){
            websocket.close();
        }

        function huiche() {
            if(event.keyCode==13)
            {
                send();
            }
        }
        //发送消息
        function send(){
            var message = document.getElementById('message').value;//要发送的消息内容
            var ToSendUserno = document.getElementById('userid').value; //接收人编号
            var send = new Object();//将要发送的信息和内容拼起来，以便于服务端知道消息要发给谁
            send.msgId = 0;
            send.notry ="消息";
            send.msgSendTime = getNowFormatDate();
            send.msgContext = message;
            send.msgSendId = ${loginUser.userId};
            send.msgReciveId = ToSendUserno;
            var s = JSON.stringify(send)
            websocket.send(s);
            showChat(s);
        }
        /**
         * 展示会话信息
         */
        function showChat(s){
            var message = JSON.parse(s);
            var image = document.getElementById("image").value;
            var notry = message.notry;
            if (notry =="通知"||notry=="推送"){
                showTong(message);
            }else{
                var sendUserno = message.msgReciveId;//获取接收人
                var sendMessage = message.msgContext;//消息
                var now =  message.msgSendTime;//时间
                var isSef = sendUserno==${loginUser.userId}+""?"am-left":"am-right";
                var pic =  sendUserno==${loginUser.userId}?image:"${userimage.imageUrl}";
                var url = "/leaguerPage/leaguerInfo?userid="+sendUserno;
                var html = "<li class='am-comment'>"+
                    "<div class='am-date'>"+now+"</div>"+
                    "<div class='am-comment-main'>"+
                    "<a href='"+url+"'>"+
                    "<img class='am-comment-avatar  "+isSef+"' src=\'"+pic+"'>"+
                    "</a>"+
                    "<div class='am-comment-meta  "+isSef+"'>"+sendMessage+"</div>"+
                    "</div>"+
                    "</li>"+
                    "<li class='am-kong'></li><br/>";
                $("#chat").append(html);
                $("#message").val("");  //清空输入区
                var chat = $("#chat-view");
                chat.scrollTop(chat[0].scrollHeight);   //让聊天区始终滚动到最下面
                // alert("有最新消息");
            }
        }
        //获取当前时间
        function getNowFormatDate() {
            var date = new Date();
            var seperator1 = "-";
            var seperator2 = ":";
            var month = date.getMonth() + 1;
            var strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
                + " " + date.getHours() + seperator2 + date.getMinutes()
                + seperator2 + date.getSeconds();
            return currentdate;
        };
    </script>
</div>
</html>
