<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.rimi.bean.User" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>缘来是你婚恋交友网</title>
	<meta name="Keywords" content="缘来是你婚恋交友网"/>
	<meta name="Description" content="缘来是你婚恋交友网"/>
	<link type="image/x-icon" rel=icon href="/images/icon.png" />
	<link type="text/css" rel="stylesheet" href="/css/style.css"/>
	<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="/js/koala.min.1.5.js"></script>
	<script type="text/javascript" src="/js/comment.js"></script>
	<script type="text/javascript" src="/js/jquery.collapse.js" ></script>

</head>
<body>
<div class="head">
	<div class="top">
		<div class="top-right">
			<%
				User loginUser = (User) request.getSession().getAttribute("loginUser");
				if (loginUser == null) {
			%>
			<a href="/user/login">登录</a> |
			<a href="/user/reg">注册</a>
			<%
				}
			%>
			<%
				if (loginUser != null) {
			%>
			欢迎您:${loginUser.userRealname} |
			<a href="/user/logout">注销</a>
			<%
				}
			%>

		</div>

	</div>
</div>
<div class="nav-box">
	<div class="nav">
		<a href="/user/chatindex">网站首页</a>
		<a href="/user/firstUserList">条件搜索</a>
		<a href="/user/userInformation">快速匹配</a>
		<a href="/sotry/index">婚恋故事</a>
		<a href="/userPersonal">个人中心</a>
	</div>
</div>
<%--<div class="banner"><img src="/images/banner.jpg"/></div>--%>
<div class="main">
	<div class="main-box">
		<div class="main-left"><!--left--><br>


			<div>
				<form action="/user/firstUserList" method="post" >
					年龄：<select class="address-select" name="age1">
							<option value="17">不限</option>
							<c:forEach var="i" begin="18" end="60">
                                <c:if test="${(2018-firstUserCondition.age1.substring(0,4))==i}">
                                    <option value="${i}" selected="selected">${i}岁</option>
                                </c:if>
                                <c:if test="${(2018-firstUserCondition.age1.substring(0,4))!=i}">
                                    <option value="${i}" >${i}岁</option>
                                </c:if>
							</c:forEach>
						</select>-
						<select class="address-select" name="age2">
							<option value="70" selected="selected">不限</option>
							<c:forEach var="a" begin="18" end="60">
                                <c:if test="${(2018-firstUserCondition.age2.substring(0,4))==a}">
                                    <option value="${a}" selected="selected">${a}岁</option>
                                </c:if>
                                <c:if test="${(2018-firstUserCondition.age2.substring(0,4))!=a}">
                                    <option value="${a}">${a}岁</option>
                                </c:if>
							</c:forEach>
						</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					身高：<select class="address-select" name="height1">
							<option value="0" >不限</option>
							<c:forEach var="j" begin="145" end="190">
								<option value="${j}" ${firstUserCondition.height1==j?"selected":""}>${j}CM</option>
							</c:forEach>
						</select>-
						<select class="address-select" name="height2">
							<option value="210" >不限</option>
                            <c:forEach var="z" begin="145" end="190">
                                <option value="${z}" ${firstUserCondition.height2==z?"selected":""}>${z}CM</option>
                            </c:forEach>
						</select><br><br>
					收入：<select class="address-select" name="inCome">
							<option value="0-0" ${"0-0"==firstUserCondition.inCome?"selected":""}>不限</option>
                            <option value="0-3000" ${"0-3000"==firstUserCondition.inCome?"selected":""}>3000以下</option>
							<option value="3000-8000" ${"3000-8000"==firstUserCondition.inCome?"selected":""}>3000-8000</option>
                            <option value="8000-20000" ${"8000-20000"==firstUserCondition.inCome?"selected":""}>8000-20000</option>
							<option value="20000-30000" ${"20000-30000"==firstUserCondition.inCome?"selected":""}>20000以上</option>
						</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					婚姻状况：<select class="address-select" name="marry">
							<option value="不限" ${"不限"==firstUserCondition.marry?"selected":""}>不限</option>
                            <option value="未婚" ${"未婚"==firstUserCondition.marry?"selected":""}>未婚</option>
							<option value="已婚" ${"已婚"==firstUserCondition.marry?"selected":""}>已婚</option>
                            <option value="离婚" ${"离婚"==firstUserCondition.marry?"selected":""}>离婚</option>
							<option value="丧偶" ${"丧偶"==firstUserCondition.marry?"selected":""}>丧偶</option>
						</select><br><br>
					居住地址：<select class="address-select" name="province" id="s_province" >

						</select>(省)&nbsp;
						<select class="address-select" name="city" id="s_city" >

						</select>(市)<br><br>
					<script type="text/javascript" src="/js/area.js"></script>
                <c:if test="${sheng!=null}">
                    <script>
                        opt0[0]="${sheng}"
                        opt0[1]="${sheng}"
                    </script>
                </c:if>
                    <button class="save-btn" name="cha" value="cha">查找</button>
				</form>
			</div>

			<div class="main-l-top">
				<div class="main-title">
					人气会员<span>只为寻找有缘的你</span>
				</div>
			</div>
			<ul class="main-member">
                <c:if test="${firstUserList==null||firstUserList.size()<=0}">
                    <br>
                    <p>没有查到对应的用户哟，要不您把条件放宽一些再试试！</p>
                </c:if>
                <c:if test="${firstUserList!=null&&firstUserList.size()>0}">
                <c:forEach var="a" items="${firstUserList}">
                    <li>
                        <a href="/leaguerPage/leaguerInfo?userid=2">
                            <img src="/images/test.png" />
                            <p class="mem-num">会员号：${a.userId}</p>
                            <p class="mem-text">
                                    <c:if test="${a.userBirthday!=null&&''!=a.userBirthday}">
                                        ${118-a.userBirthday.getYear()}岁
                                    </c:if>
                                    <c:if test="${a.userBirthday==null||''==a.userBirthday}">
                                        保密
                                    </c:if>
                                      |
                                    <c:if test="${a.userEnucation!=null&&''!=a.userEnucation}">
                                        ${a.userEnucation}
                                    </c:if>
                                    <c:if test="${a.userEnucation==null||''==a.userEnucation}">
                                        保密
                                    </c:if>
                                      |
                                    <c:if test="${a.userHeight!=null&&a.userHeight!=0}">
                                        ${a.userHeight}CM
                                    </c:if>
                                    <c:if test="${a.userHeight==null||a.userHeight==0}">
                                        保密
                                    </c:if>
                                      <br />
                                    <c:if test="${a.userMarry!=null&&''!=a.userMarry}">
                                        ${a.userMarry}
                                    </c:if>
                                    <c:if test="${a.userMarry==null||''==a.userMarry}">
                                        保密
                                    </c:if>
                                     |
                                    <c:if test="${a.userHouse!=null&&''!=a.userHouse}">
                                        ${a.userHouse}
                                    </c:if>
                                    <c:if test="${a.userHouse==null||''==a.userHouse}">
                                        保密
                                    </c:if>
                                   </p>
                        </a>
                    </li>
                </c:forEach>
                </c:if>
			</ul>
            <br>
            <div>

                <a href="/user/firstPageChange?nowPage=${firstUserCondition.nowPage-1}">上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/user/firstPageChange?nowPage=${firstUserCondition.nowPage+1}">下一页</a>

            </div>
        </div>
    <%--<div class="main-title">--%>
			<%--友情链接<span>以下商家为“万家囍”合作伙伴</span>--%>
			<%--<a href="">更多></a>--%>
			<%--</div>--%>
			<%--<ul class="friend-link">--%>
			<%--<li><a href=""><img src="/images/link3.png"/></a></li>--%>
			<%--<li><a href=""><img src="/images/link1.png"/></a></li>--%>
			<%--<li><a href=""><img src="/images/link4.png"/></a></li>--%>
			<%--<li><a href=""><img src="/images/link2.png"/></a></li>--%>
			<%--</ul>--%>
		<div class="main-right"><!--right-->
			<%--<div class="main-log">--%>
			<%--<div class="tit">会员登录</div>--%>
			<%--<div class="main-logbox">--%>
			<%--<i class="main-user"></i>--%>
			<%--<input placeholder="账户名" type="text" class="main-user-input" />--%>
			<%--</div>--%>
			<%--<div class="main-logbox">--%>
			<%--<i class="main-password"></i>--%>
			<%--<input placeholder="密码" type="text" class="main-user-input" />--%>
			<%--</div>--%>
			<%--<button type="button"  class="main-btn">立即登录</button>--%>
			<%--<div class="main-pass-text">没有帐号？<a href="reg.jsp">免费注册</a><a href="" class="forget">忘记密码 ></a></div>--%>
			<%--</div>--%>
			<div class="main-radv"><img src="/images/adv2.png"/></div>
			<div class="main-radv"><img src="/images/adv1.png"/></div>
			<div class="main-message">
				<div class="tit">通告栏</div>
				<p>一、请各用电客户尽快与开户银行联系，按照中国人民银行XX分行的统一要求签订《定期借记业务授权委托书》，并于2002年1月31日前将复印件送达所在区供电局。</p>
				<p>二、部分商业银行由于系统升级原因更改了开户银行账号格式，请客户在签订《定期借记业务授权委托书》的同时与贵开户银行确认新的银行账号，并于2002年1月31日前以正式公函形式通知我局，若届时未受到客户的《定期借记业务授权委托书》及新的银行帐号的，客户将无法使用定期借记方式缴交电费，我局将采取现金方式收取电费。</p>
				<p>三、由于定期借记业务系统投运后，银行系统将不再代为传递电费票据，故我局将统一采取邮政递送，客户签收的方式派发系统将不再代为传递电费票据，故我局将统一采取邮政递送，客户签收的方式派发</p>
			</div>
			<%--<div class="main-message1">--%>
			<%--<div class="tit">联系我们</div>--%>
			<%--<div class="main-contact">--%>
			<%--<p>公司名称：</p>--%>
			<%--<p>联系人：</p>--%>
			<%--<p>联系电话：400-400-400</p>--%>
			<%--<p>电子邮箱：</p>--%>
			<%--<p>地   址：成都市高新区</p>--%>
			<%--<p>xxxx婚恋交友官方微信</p>--%>
			<%--<p><img src="/images/weixin.jpg"/></p>--%>
			<%--<p style="text-align: center;">“ 扫一扫与万家喜亲密互动 ”</p>--%>
			<%--</div>--%>
			<%--</div>--%>
		</div><!--right-->
	</div>

	<!--在线客服-->
	<div class="chat_jsp">
		<iframe id="divFloatToolsView" src="/userFriend/UpChat"></iframe>
		<div id="floatTools" class="rides-cs" style="height:247px; ">
			<div class="floatL">
				<a id="aFloatTools_Show" class="btnOpen" title="打开聊天系统" style="top:20px;display:block" href="javascript:void(0);">展开</a>
				<a id="aFloatTools_Hide" class="btnCtn" title="关闭聊天系统" style="top:20px;display:none" href="javascript:void(0);">收缩</a>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
    function blockDiv(a) {
        if (a==1){
            var div = document.getElementById("hiddenDiv").style.display;
            if (div == 'none') {
                document.getElementById("hiddenDiv").style.display = 'block';
            } else {
                document.getElementById("hiddenDiv").style.display = 'none';
            }
        }
        if (a==2){
            var div = document.getElementById("hiddenDiv2").style.display;
            if (div == 'none') {
                document.getElementById("hiddenDiv2").style.display = 'block';
            } else {
                document.getElementById("hiddenDiv2").style.display = 'none';
            }
        }
    }
    function chat(){
        document.getElementById("hiddenDiv").style.display;
    }
    Qfast.add('widgets', { path: "js/terminator2.2.min.js", type: "js", requires: ['fx'] });
    Qfast(false, 'widgets', function () {
        K.tabs({
            id: 'fsD1',
            conId: "D1pic1",
            tabId:"D1fBt",
            tabTn:"a",
            conCn: '.fcon',
            auto: 1,
            effect: 'fade',
            eType: 'click',
            pageBt:true,
            bns: ['.prev', '.next'],
            interval: 3000
        })
    })
</script>
</body>
</html>
