<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>缘来是你婚恋交友网</title>
    <meta name="Keywords" content="缘来是你婚恋交友网"/>
    <meta name="Description" content="缘来是你婚恋交友网"/>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <style>
        .main-log {
            height: 290px;
            width: 350px;
            margin-bottom: 30px;
            background: #fff;
        }

        .main-logbox {
            width: 300px;
        }

        .main-user-input {
            width: 270px;
        }

        .main-log .tit {
            height: 50px;
            line-height: 50px;
            font-size: 24px;
        }

        .login-left {
            width: 650px;
        }

        .login-left img {
            margin: 20px 0px 0px 200px;
        }

        .main-btn, .main-pass-text {
            width: 300px;
        }
    </style>
</head>
<%
    String accValue = "";//账号默认值
    String psValue = "";//密码默认值

    // 获取cookie
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie c : cookies) {
            //获取cookie的key名称
            String keyName = c.getName();
            if ("accCookie".equals(keyName)) {
                accValue = c.getValue();//
            }
            if ("psCookie".equals(keyName)) {
                psValue = c.getValue();
            }
        }
    }
%>
<body>
<div class="head">
    <div class="top">
        <div class="top-right">
            <a href="/user/index">首页</a> |
            <a href="/user/index">帮助</a>
        </div>
    </div>
</div>
<div class="nav-box">
    <div class="nav">
        <%--<a href="index.jsp">网站首页</a>--%>
        <%--<a href="searchmember.jsp">快速匹配</a>--%>
        <%--<a href="sharemarry.jsp">婚恋故事</a>--%>
        <%--<a href="infor.jsp">个人中心</a>--%>
    </div>
</div>
<div class="main">
    <div class="main-box1">
        <div class="login-left"><img src="/images/loginpic.png"/></div>
        <div class="login-right">
            <div class="main-log">
                <form id="form1" action="/user/login" method="post" onsubmit="return checkFun()">
                    <div class="tit">会员登录</div>

                    <div class="main-logbox">
                        <i class="main-user"></i>
                        <input placeholder="账户名" id="account" name="userAccount" type="text" value="<%=accValue%>"
                               class="main-user-input">
                    </div>
                    <div class="main-logbox">
                        <i class="main-password"></i>
                        <input placeholder="密码" id="password" name="userPassword" value="<%=psValue%>"
                               type="password" class="main-user-input">
                    </div>
                    <div class="remember">
                        <input id="remPs" name="remPs" value="y" onclick="rememberMe()" type="checkbox">
                        <label>记住密码</label>
                    </div>

                    <button type="submit" class="main-btn">立即登录</button>
                    <div class="main-pass-text">
                        没有帐号？<a href="/user/register">立即注册</a>
                        <a href="#" id="findps" class="forget" onclick="findPs()">忘记密码？通过邮箱找回 &gt;</a>
                    </div>
                </form>

                <form id="form2" hidden>
                    <div class="tit">找回密码</div>

                    <div class="main-logbox">
                        <i class="main-user"></i>
                        <input placeholder="请输入注册邮箱" id="regEmail" name="regEmail" type="text" class="main-user-input">
                    </div>

                    <div class="remember"></div>
                    <button type="button" onclick="sendEmail()" class="main-btn">发送邮件</button>
                    <div class="remember"></div>
                    <button type="button" onclick="backLogin()" class="main-btn">返回</button>
                </form>


            </div>
        </div>
    </div>

</div>
</body>

<script src="/js/jquery-3.3.1.js"></script>
<script src="/layer/layer.js"></script>
<script>
    <%--页面加载时如果账号密码有值，选中单选框，未选中，取消单选框--%>
    window.onload = function () {
        //获取输入框的值
        var account = $("#account").val();
        if (account != null && account != "") {
            //选中单选框
            $("#remPs").prop('checked', true);
        } else {
            //选中单选框
            $("#remPs").prop('checked', false);
        }
    }

    <%--点击记住密码--%>
    function rememberMe() {
        //获取当前单选框点击后，（点击前未选中，进入函数时变成选中了，反之）
        var checked = $("#remPs").prop('checked');
        if (checked) {//如果单选框未选中，显示提示信息
            if (confirm("请勿在公共电脑上勾选此项，是否确认？")) {
                $("#remPs").prop('checked', true);
            } else {
                $("#remPs").prop('checked', false);
            }
        } else {//单选框已经选中，点击后取消选中
            $("#remPs").prop('checked', false);
        }
    }

    <%--返回登录页面--%>
    function backLogin() {
        //显示登录界面
        $("#form1").show();
        //隐藏找回密码界面
        $("#form2").hide();
        // 清空输入框
        $("#regEmail").val("");
    }

    <%--找回密码--%>

    function findPs() {
        //隐藏登录界面
        $("#form1").hide();
        //显示找回密码界面
        $("#form2").show();
    }

    function sendEmail() {
        // 获取输入的邮箱
        var regEmail = $("#regEmail").val();
        // 需要验证邮箱符合规则
        if (checkAccount()) {
            layer.msg("发送中......");
            $.ajax(
                {
                    type: "post",
                    url: "/user/findPs",
                    data: "regEmail=" + regEmail,
                    success: function (msg) {//请求成功返回值
                        if (msg == 1) {
                            layer.msg("已发送");
                        }
                    }
                }
            );
        }

    }


    <%--提交时异步验证账号密码--%>

    function checkFun() {
        var account = $("#account").val();
        var password = $("#password").val();

        if (account == null || account == "") {
            layer.msg("账号不能为空！")
            return false;
        }
        if (password == null || password == "") {
            layer.msg("密码不能为空！");
            return false;
        }

        $.ajax(
            {
                type: "post",
                url: "/user/loginAjax",
                data: "userAccount=" + account + "&userPassword=" + password,
                async: false,
                success: function (msg) {//请求成功返回值
                    if (msg == "-1") {
                        alert("用户名或密码错误！");
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        );

    }

    <%--验证密码框非空和符合邮箱规则--%>

    function checkAccount() {

        var regEmail = $("#regEmail").val();//获得输入框值

        if (regEmail == null || regEmail == "") { // 非空验证
            layer.msg("注册账号不能为空！");
            return false;
        } else {// 进入else说明不为空
            var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
            emailIsMatch = reg.test(regEmail);//判断邮箱是否匹配规则

            if (!emailIsMatch) {
                layer.msg("邮箱格式不正确!");
                return false;
            } else {
                return true;
            }
        }
    }


</script>
<script language="javascript">
    //防止页面后退
    history.pushState(null, null, document.URL);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, document.URL);
    });
</script>
</html>
