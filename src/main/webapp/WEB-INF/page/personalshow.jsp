<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>南昌万家喜婚恋交友网</title>
    <meta name="Keywords" content="南昌万家喜婚恋交友网"/>
    <meta name="Description" content="南昌万家喜婚恋交友网"/>
    <link type="image/x-icon" rel=icon href="/images/icon.png"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/js/comment.js"></script>
    <script type="text/javascript" src="/js/popup.mini.js"></script>
    <script type="text/javascript" src="/js/chat.js"></script>
</head>
<body>
<div class="head">
    <div class="top">
        <div class="top-left">
            <a href="javascript:;" class="top-coll" onclick="AddFavorite('', 'http://www.ncwjxhljyw.com')">收藏万家喜</a> |
            <a href="" class="top-att">关注万家喜</a>
        </div>
        <div class="top-right">
            <a href="">注册</a> |
            <a href="">登录</a>
        </div>
    </div>
    <div class="top-ban">
        <div class="top-mid-box">
            <a href="" class="logo"><img src="/images/logo.png"/></a>
            <div class="adv"><img src="/images/adv.png"/></div>
            <div class="tele"><img src="/images/tele.png"/></div>
            <div class="erweima"><img src="/images/erwei.png"/></div>
        </div>
    </div>
</div>
<div class="nav-box">
    <div class="nav">
        <a href="/user/chatindex">网站首页</a>
        <a href="/user/firstUserList">条件搜索</a>
        <a href="/user/userInformation">快速匹配</a>
        <a href="/sotry/index">婚恋故事</a>
        <a href="/userPersonal">个人中心</a>
    </div>
</div>

<div class="main">
    <div class="main-box1">
        <div class="main-left"><!--left-->
            <div class="s-address">当前位置：<a href="">首页 </a>>搜索会员>${leaguer.userId}</div>
            <div class="s-top">
                <div class="preview">
                    <div id="vertical" class="bigImg"><img src="/images/bgimg.jpg" width="300" height="300" alt=""
                                                           id="midimg"/>
                    </div>
                    <div class="smallImg">
                        <div class="scrollbutton smallImgUp disabled"></div>
                        <div id="imageMenu">
                            <ul>
                                <li id="onlickImg"><img src="${userImageList.get(0).imageUrl}" width="60" height="60"/></li>
                               <c:forEach var="userImage" items="${userImageList}" begin="1">
                                   <li><img src="${userImage.imageUrl}" width="68" height="68"/></li>
                               </c:forEach>
                        </div>
                        <div class="scrollbutton smallImgDown"></div>
                    </div>
                </div>
                <div class="s-minfor">
                    <div class="tit">
                        <div class="s-mname">${leaguer.userRealname} <span>会员号：${leaguer.userId}</span></div>
                        <div class="m-icon" id="m-icon">
                        </div>
                        <div class="m-icon" id="m-guan">
                        </div>
                    </div>
                    <ul class="infor">
                        <li>
                            <p>性别：<span>${leaguer.userSex}</span></p>
                            <p>学历：<span>${leaguer.userEnucation}</span></p>
                        </li>
                        <li>
                            <p>生日：<span><fm:formatDate value="${leaguer.userBirthday}" type="date"  pattern="yyyy-MM-dd"/></span></p>
                            <p>血型：<span>${leaguer.userBlood}</span></p>
                        </li>
                        <li>
                            <p>身高：<span>${leaguer.userHeight}cm</span></p>
                            <p>体重：<span>${leaguer.userWeight}kg</span></p>
                        </li>
                        <li>
                            <p>民族：<span>${leaguer.userNation}</span></p>
                        </li>
                    </ul>
                    <ul class="infor1">
                        <li>
                            <p>户&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;藉:<span>${leaguer.userFromplace}</span></p>
                            <p>所&nbsp;在&nbsp;&nbsp;地：<span>${leaguer.userLocation}</span></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="s-introduce">
                <div class="tit">择偶要求</div>
            </div>
            <ul class="s-yaoqiu">
                <li>
                    <p>出生年份：<span><fm:formatDate value="${likeUser.userBirthday}" type="date"  pattern="yyyy"/></span></p>
                    <p>月 收 入：<span>${likeUser.userIncome}</span></p>
                    <p>吸&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 烟：<span>${likeUser.userSmoke}</span></p>
                    <p>是否要小孩：<span>${likeUser.userWangchild}</span></p>
                    <p>父母情况：<span>${likeUser.userParent}</span></p>
                </li>
                <li>
                    <p>身&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高：<span>${likeUser.userHeight}</span></p>
                    <p>住&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;房：<span>${likeUser.userHouse}</span></p>
                    <p>饮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 酒：<span>${likeUser.userDrink}</span></p>

                </li>
                <li>
                    <p>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;历：<span>${likeUser.userEnucation}</span></p>
                    <p>购&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;车：<span>${likeUser.userCar}</span></p>
                    <p>月&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薪：<span>${likeUser.userIncome}&nbsp;&nbsp;k</span></p>

                </li>
                <li>
                    <p>婚姻状况：<span>${likeUser.userMarry}</span></p>
                    <p>购&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 房：<span>${likeUser.userHouse}</span></p>
                </li>
            </ul>
            <ul class="infor1">
                <li>
                    <p>户&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;藉：<span>${likeUser.userFromplace}</span></p>
                    <p>现&nbsp;&nbsp;住&nbsp;&nbsp;地：<span>${likeUser.userLocation}</span></p>
                </li>
            </ul>
        </div><!--left-->
        <div class="main-right"><!--right-->
            <div class="main-log">
                <div class="tit">${loginUser.userRealname}</div>
                <div class="mem-pic"><img src="${loginUserImage.imageUrl}"/></div>
                <div class="main-pass-text" style="margin-top: 25px;">
                    <a href="/userPersonal" class="entermem" style="color: #fff;">进入个人中心</a>
                    <a href="" class="forget">忘记密码 ></a>
                </div>
            </div>
            <div class="main-radv"><img src="/images/adv2.png"/></div>
            <div class="main-message1">
                <div class="tit">联系我们</div>
                <div class="main-contact">
                    <p>公司名称：</p>
                    <p>联系人：</p>
                    <p>联系电话：400-400-400</p>
                    <p>电子邮箱：</p>
                    <p>实体店地址：进贤县香江商业街27号</p>
                    <p>南昌万家喜婚恋交友官方微信</p>
                    <p><img src="/images/weixin.jpg"/></p>
                    <p style="text-align: center;">“ 扫一扫与万家喜亲密互动 ”</p>
                </div>
            </div>
        </div><!--right-->
    </div>
    <div class="nav-box">
        <div class="nav">

        </div>
    </div>
    <div class="copy">
        <p>COPYRIGHT 2015  南昌万家囍婚恋交友网  版权所有 赣ICP备15002274号-1</p>
        <p>地址：进贤县香江商业街27号  电话：138-7915-5305  技术支持：<a target="_blank" href="http://www.0791jr.com/">嘉瑞科技</a></p>
    </div>
    <!--在线客服-->
    <div class="chat_jsp">
        <iframe id="divFloatToolsView" src="/userFriend/UpChat"></iframe>
        <div id="floatTools" class="rides-cs" style="height:247px; ">
            <div class="floatL">
                <a id="aFloatTools_Show" class="btnOpen" title="打开聊天系统" style="top:20px;display:block"
                   href="javascript:void(0);">展开</a>
                <a id="aFloatTools_Hide" class="btnCtn" title="关闭聊天系统" style="top:20px;display:none"
                   href="javascript:void(0);">收缩</a>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript'>
    var param = ${leaguer.userId}+"," +${loginUser.userId};
    showFriend(param);
    ChatPopu(param);
</script>
</body>
</html>
