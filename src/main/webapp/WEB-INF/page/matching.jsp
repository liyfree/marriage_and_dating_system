<%@ page import="com.rimi.bean.User" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.rimi.bean.UserInformation" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<%
	Calendar instance = Calendar.getInstance();
	int i = instance.get(Calendar.YEAR);
	UserInformation userInformation = (UserInformation) request.getAttribute("userInformation");
	int likeage=0;
	int age=0;
	if (userInformation.getLikeUser().getUserBirthday()!=null){
		likeage =i-1900- userInformation.getLikeUser().getUserBirthday().getYear();
	}
	if (userInformation.getUser().getUserBirthday()!=null){
		age=i-1900-userInformation.getUser().getUserBirthday().getYear();
	}
%>
	<head>
		<meta charset="utf-8" />
		<title>南昌万家喜婚恋交友网</title>
		<meta name="Keywords" content="南昌万家喜婚恋交友网" />
        <meta name="Description" content="南昌万家喜婚恋交友网"/>
		<link type="image/x-icon" rel=icon href="/images/icon.png" />
		<link type="text/css" rel="stylesheet" href="/css/style.css"/>
		<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="/js/comment.js"></script>
	</head>
	<body>
		<div class="head">
		  <div class="top">
		  	 <div class="top-left">

		  	 </div>
		  	 <div class="top-right">
                 <%
                     User loginUser = (User) request.getSession().getAttribute("loginUser");
                     if (loginUser == null) {
                 %>
                 <a href="/user/login">登录</a> |
                 <a href="/user/reg">注册</a>
                 <%
                     }
                 %>
                 <%
                     if (loginUser != null) {
                 %>
                 欢迎您:${loginUser.userRealname} |
                 <a href="/user/logout">注销</a>
                 <%
                     }
                 %>
		  	 </div>
		  </div>
		  
		</div>
		<div class="nav-box">
			<div class="nav">
				<a href="/user/chatindex">网站首页</a>
				<a href="/user/firstUserList">条件搜索</a>
				<a href="/user/userInformation">快速匹配</a>
				<a href="/sotry/index">婚恋故事</a>
				<a href="/userPersonal">个人中心</a>
			</div>
		</div>
		
		<div class="main">
			<div class="main-box1">
			<div class="main-left"><!--left-->
				<div class="s-address">当前位置：首页 >快速匹配>${userInformation.user.userId}</div><br><br><br>

				<div>
					<div style="width:20%;float:left;">
					&nbsp;
					</div>
				    <div style="width:40%;float:left;">
					<font size="5" color="blue">
					匹配指数：${userInformation.matchingIndex}%
					</font>
					</div>
					<div style="width:30%;float:left;">
						<form action="/user/userInformation">
							<button class="save-btn" name="match" value="notSatisfied">不满意换一个</button>
						</form>
				    </div>
				</div>
				<br><br><br><br>
				
				<div class="s-top">
			    <div class="preview">
	               <div id="vertical" class="bigImg"><img src="/images/bgimg.jpg" width="300" height="300" alt="" id="midimg" /></div>
	               <div class="smallImg">
		              <div class="scrollbutton smallImgUp disabled"></div>
		              <div id="imageMenu">
			            <ul>
				           <li id="onlickImg"><img src="/images/test.png" width="60" height="60" /></li>
				           <li><img src="/images/bgimg.jpg" width="68" height="68" /></li>
				           <li><img src="/images/test.png" width="68" height="68" /></li>
				           <li><img src="/images/test1.png" width="68" height="68" /></li>
			            </ul>
		              </div>
		               <div class="scrollbutton smallImgDown"></div>
	                 </div>
                </div>
                <div class="s-minfor">
                	<div class="tit">
                		<div class="s-mname">${userInformation.user.userRealname} <span>会员号：${userInformation.user.userId}</span></div>
                		<div class="m-icon">
                			<a href="" class="m-icon-email"></a>
                		</div>
                	</div>
                	<ul class="infor">
                	   <li>
                		  <p>性别：<span>${userInformation.user.userSex}</span></p>
						  <p>身高：<span>
							  <c:if test="${userInformation.user.userHeight==null || userInformation.user.userHeight==0}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userHeight!=0&&userInformation.user.userHeight!=null}">
								  ${userInformation.user.userHeight}CM
							  </c:if>
							  </span></p>
                	   </li> 
                	   <li>
                		  <p>年龄：<span>
							  <c:if test="${userInformation.user.userBirthday!=null}">
								  <%= age%>
					  		 </c:if>
						   <c:if test="${userInformation.user.userBirthday==null}">
							   保密
						   </c:if>
						   </span></p>
                		  <p>体重：<span>
							  <c:if test="${userInformation.user.userWeight==null || userInformation.user.userWeight==0}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userWeight!=0&&userInformation.user.userWeight!=null}">
						${userInformation.user.userHeight}CM
						</c:if>
						</span></p>
						</li>
                	   <li>
                		  <p>民族：<span>
							  <c:if test="${userInformation.user.userNation==null || userInformation.user.userNation==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userNation!=null&&userInformation.user.userNation!=''}">
								  ${userInformation.user.userNation}
							  </c:if>
							  </span></p>
                		  <p>学历：<span>
							  <c:if test="${userInformation.user.userEnucation==null || userInformation.user.userEnucation==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userEnucation!=null&&userInformation.user.userEnucation!=''}">
								  ${userInformation.user.userEnucation}
							  </c:if>
							  </span></p>
                	   </li>
                	   <li>
                		  <p>血型：<span>
							  <c:if test="${userInformation.user.userBlood==null || userInformation.user.userBlood==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userBlood!=null&&userInformation.user.userBlood!=''}">
								  ${userInformation.user.userBlood}
							  </c:if>
							  </span></p>
						  <p>收入：<span>
							  <c:if test="${userInformation.user.userIncome==null || userInformation.user.userIncome==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userIncome!=null&&userInformation.user.userIncome!=''}">
								  ${userInformation.user.userIncome}
							  </c:if>
							  </span></p>
                	   </li>  
                    </ul>
					
					<ul class="infor">
                	   <li>
						  <p>是否喝酒：<span>
							  <c:if test="${userInformation.user.userDrink==null || userInformation.user.userDrink==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userDrink!=null&&userInformation.user.userDrink!=''}">
								  ${userInformation.user.userDrink}
							  </c:if>
							  </span></p>
						  <p>婚姻情况：<span>
							  <c:if test="${userInformation.user.userMarry==null || userInformation.user.userMarry==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userMarry!=null&&userInformation.user.userMarry!=''}">
								  ${userInformation.user.userMarry}
							  </c:if>
							  </span></p>
					   </li>  
					   <li>
						 <p>是否抽烟：<span>
							 <c:if test="${userInformation.user.userSmoke==null || userInformation.user.userSmoke==''}">
								 保密
							 </c:if>
							  <c:if test="${userInformation.user.userSmoke!=null&&userInformation.user.userSmoke!=''}">
								  ${userInformation.user.userSmoke}
							  </c:if>
							</span></p>
						  <p>子女情况：<span>
							  <c:if test="${userInformation.user.userChild==null || userInformation.user.userChild==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userChild!=null&&userInformation.user.userChild!=''}">
								  ${userInformation.user.userChild}
							  </c:if>
							  </span></p>
					   </li> 
					   <li>
						 <p>是否有车：</p>
						  <p><span>
							  <c:if test="${userInformation.user.userCar==null || userInformation.user.userCar==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userCar!=null&&userInformation.user.userCar!=''}">
								  ${userInformation.user.userCar}
							  </c:if>
							 </span></p>
					   </li> 
					   <li>
						 <p>是否有房：</p>
						  <p><span>
							  <c:if test="${userInformation.user.userHouse==null || userInformation.user.userHouse==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userHouse!=null&&userInformation.user.userHouse!=''}">
								  ${userInformation.user.userHouse}
							  </c:if>
							  </span></p>
					   </li> 
                    </ul>
					<ul class="infor">
                	   <li>
						  <p>毕业院校：</p>
						  <p><span>
							  <c:if test="${userInformation.user.userSchool==null || userInformation.user.userSchool==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userSchool!=null&&userInformation.user.userSchool!=''}">
								  ${userInformation.user.userSchool}
							  </c:if>
							 </span></p>
					   </li>  
					   <li>
						  <p>是否想要小孩：</p>
						  <p><span>
							  <c:if test="${userInformation.user.userWangchild==null || userInformation.user.userWangchild==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.user.userWangchild!=null&&userInformation.user.userWangchild!=''}">
								  ${userInformation.user.userWangchild}
							  </c:if>
							  </span></p>
					   </li> 
					   <li>
						   <p>所在地：</p>
						   <p><span>
							   <c:if test="${userInformation.user.userLocation==null || userInformation.user.userLocation==''}">
								   保密
							   </c:if>
							  <c:if test="${userInformation.user.userLocation!=null&&userInformation.user.userLocation!=''}">
								  ${userInformation.user.userLocation}
							  </c:if>
						   		</span></p>
					   </li>
						<li>
							<p>父母情况：<span>
								<c:if test="${userInformation.user.userParent==null || userInformation.user.userParent==''}">
									保密
								</c:if>
							  <c:if test="${userInformation.user.userParent!=null&&userInformation.user.userParent!=''}">
								  ${userInformation.user.userParent}
							  </c:if>
								</span></p>
							<p>职业：<span>
								<c:if test="${userInformation.user.userHeight==null || userInformation.user.userHeight==''}">
									保密
								</c:if>
							  <c:if test="${userInformation.user.userHeight!=null&&userInformation.user.userHeight!=''}">
								  ${userInformation.user.userHeight}
							  </c:if>
								</span></p>
						</li>
                    </ul><br>
                    <ul class="infor1">
                       <li>
                       	<p>最后登录时间：<span>
							   <c:set var="t" value="${userInformation.user.userLogintime}"/>
								   <fmt:formatDate type="both"
												   dateStyle="long" timeStyle="long"
												   value="${t}" />
							   </span>
                       	<p>现住居地：<span>
                           <c:if test="${userInformation.user.userLocation==null || userInformation.user.userLocation==''}">
                               保密
                           </c:if>
							  <c:if test="${userInformation.user.userLocation!=null&&userInformation.user.userLocation!=''}">
                                  ${userInformation.user.userLocation}
                              </c:if>
                          </span> </p>
                       </li>
                    </ul>
                </div>
                </div>
          
             <div id="">
             	<hr>
             	<div class="s-top">
			    <div class="preview">
			    	 <ul class="infor">
	              	
	             	 </ul>
	              <ul class="infor">
	              	
                	  
	              </ul>
                </div>
                <div class="s-minfor">
                	<ul class="infor">
                		<p align="center"><font color="#00A0E9" size="4">择偶意向</font></p><br>
                	   <li>
                		  <p>所在地：<span></span></p>
						  <p>身高：<span>
							  <c:if test="${userInformation.likeUser.userHeight==null || userInformation.likeUser.userHeight==0}">
							  保密
						 	 </c:if>
							  <c:if test="${userInformation.likeUser.userHeight!=0&&userInformation.likeUser.userHeight!=null}">
							  ${userInformation.likeUser.userHeight}CM
							  </c:if></span></p>
                	   </li>
                	   <li>
                		  <p>年龄：<span>
							   <c:if test="${userInformation.user.userBirthday!=null}">
								   <%= likeage%>
							   </c:if>
							   <c:if test="${userInformation.user.userBirthday==null}">
								   保密
							   </c:if>
						  </span></p>
                		  <p>体重：<span>
							  <c:if test="${userInformation.likeUser.userWeight==null || userInformation.likeUser.userWeight==0}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.likeUser.userWeight!=0&&userInformation.likeUser.userWeight!=null}">
								  ${userInformation.likeUser.userWeight}CM
							  </c:if>
							 </span>
						  </p>
                	   </li>
                	   <li>
                		  <p>民族：<span>
							  <c:if test="${userInformation.likeUser.userNation==null || userInformation.likeUser.userNation==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.likeUser.userNation!=null&&userInformation.likeUser.userNation!=''}">
								  ${userInformation.likeUser.userNation}
							  </c:if>
							  </span></p>
                		  <p>学历：<span>
							   <c:if test="${userInformation.likeUser.userEnucation==null || userInformation.likeUser.userEnucation==''}">
								   保密
							   </c:if>
							  <c:if test="${userInformation.likeUser.userEnucation!=null&&userInformation.likeUser.userEnucation!=''}">
								  ${userInformation.likeUser.userEnucation}
							  </c:if>
							  </span></p>
                	   </li>
                	   <li>
                		  <p>血型：<span>
							  <c:if test="${userInformation.likeUser.userBlood==null || userInformation.likeUser.userBlood==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.likeUser.userBlood!=null&&userInformation.likeUser.userBlood!=''}">
								  ${userInformation.likeUser.userBlood}
							  </c:if>
							  </span></p>
						  <p>收入：<span>
							   <c:if test="${userInformation.likeUser.userIncome==null || userInformation.likeUser.userIncome==''}">
								   保密
							   </c:if>
							  <c:if test="${userInformation.likeUser.userIncome!=null&&userInformation.likeUser.userIncome!=''}">
								  ${userInformation.likeUser.userIncome}
							  </c:if>
							 </span></p>
                	   </li>  
                    </ul>		
                    <br> <br> <br>
					<ul class="infor">
                	   <li>
						  <p>是否喝酒：<span>
							  <c:if test="${userInformation.likeUser.userDrink==null || userInformation.likeUser.userDrink==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.likeUser.userDrink!=null&&userInformation.likeUser.userDrink!=''}">
								  ${userInformation.likeUser.userDrink}
							  </c:if>
							  </span></p>
						  <p>婚姻情况：<span>
							  <c:if test="${userInformation.likeUser.userMarry==null || userInformation.likeUser.userMarry==''}">
								  保密
							  </c:if>
							  <c:if test="${userInformation.likeUser.userMarry!=null&&userInformation.likeUser.userMarry!=''}">
								  ${userInformation.likeUser.userMarry}
							  </c:if>
							  </span></p>
					   </li>  
					   <li>
						 <p>是否抽烟：<span>
							 <c:if test="${userInformation.likeUser.userSmoke==null || userInformation.likeUser.userSmoke==''}">
								 保密
							 </c:if>
							 <c:if test="${userInformation.likeUser.userSmoke!=null&&userInformation.likeUser.userSmoke!=''}">
								  ${userInformation.likeUser.userSmoke}
							 </c:if>
						 	 </span></p>
						  <p>子女情况：<span>
							  <c:if test="${userInformation.likeUser.userChild==null || userInformation.likeUser.userChild==''}">
								  保密
							  </c:if>
							 <c:if test="${userInformation.likeUser.userChild!=null&&userInformation.likeUser.userChild!=''}">
								 ${userInformation.likeUser.userChild}
							 </c:if>
							  </span></p>
					   </li> 
					   <li>
						 <p>是否有车：</p>
						  <p><span>
							   <c:if test="${userInformation.likeUser.userCar==null || userInformation.likeUser.userCar==''}">
								   保密
							   </c:if>
							 <c:if test="${userInformation.likeUser.userCar!=null&&userInformation.likeUser.userCar!=''}">
								 ${userInformation.likeUser.userCar}
							 </c:if>
							 </span></p>
					   </li> 
					   <li>
						   <p>父母情况：<span>
							   <c:if test="${userInformation.likeUser.userParent==null || userInformation.likeUser.userParent==''}">
								   保密
							   </c:if>
							 <c:if test="${userInformation.likeUser.userParent!=null&&userInformation.likeUser.userParent!=''}">
								 ${userInformation.likeUser.userParent}
							 </c:if>
							 </span></p>
						  <p>职业：<span>
							    <c:if test="${userInformation.likeUser.userMajor==null || userInformation.likeUser.userMajor==''}">
									保密
								</c:if>
							 <c:if test="${userInformation.likeUser.userMajor!=null&&userInformation.likeUser.userMajor!=''}">
								 ${userInformation.likeUser.userMajor}
							 </c:if>
							 </span></p>
					   </li> 
                   </ul>
					<ul class="infor">
						<li>
							<p>毕业院校：</p>
							<p><span>
								 <c:if test="${userInformation.likeUser.userSchool==null || userInformation.likeUser.userSchool==''}">
									 保密
								 </c:if>
							 	<c:if test="${userInformation.likeUser.userSchool!=null&&userInformation.likeUser.userSchool!=''}">
								 ${userInformation.likeUser.userSchool}
								 </c:if>
							</span></p>
						</li>
						<li>
							<p>是否想要小孩：</p>
							<p><span>
								<c:if test="${userInformation.likeUser.userWangchild==null || userInformation.likeUser.userWangchild==''}">
									保密
								</c:if>
							 	<c:if test="${userInformation.likeUser.userWangchild!=null&&userInformation.likeUser.userWangchild!=''}">
									${userInformation.likeUser.userWangchild}
								</c:if>
								</span></p>
						</li>
						<li>
							<p>是否有房：</p>
							<p><span>
								<c:if test="${userInformation.likeUser.userHouse==null || userInformation.likeUser.userHouse==''}">
									保密
								</c:if>
							 	<c:if test="${userInformation.likeUser.userHouse!=null&&userInformation.likeUser.userHouse!=''}">
									${userInformation.likeUser.userHouse}
								</c:if>
								</span></p>
						</li>

					</ul><br>
                </div>
                </div>
                <hr />
                <br>
                <div class="preview"></div>
                <div class="s-minfor">
                <p align="center"><font color="#00A0E9" size="4">个人爱好</font></p><br>
					<font color="#dc143c" size="3">
					<c:forEach items="${userInterestMap}" var="a">
						<p> ${a.key}：&nbsp;&nbsp;&nbsp;&nbsp;
							<c:forEach items="${a.value}" var="v">
								${v} &nbsp;&nbsp;&nbsp;&nbsp;
							</c:forEach>
						</p><br>
					</c:forEach>
					</font>
                 </div>
             </div>
          
               
               
			</div><!--left-->
			<div class="main-right"><!--right-->
				
				<div class="main-radv"><img src="/images/adv2.png"/></div>
                <div class="main-radv"><img src="/images/adv1.png"/></div>
				<div class="main-message1">
					<div class="tit">联系我们</div>
					<div class="main-contact">
						<p>公司名称：瑞丰婚恋中心</p>
						<p>联系人：李阳</p>
						<p>联系电话：110</p>
						<p>电子邮箱：10101012@qq.com</p>
						<p>实体店地址：成都市天府软件园B5座</p>
						<p>睿峰婚恋微信公众号</p>
						<p><img src="/images/weixin.jpg"/></p>
						<p style="text-align: center;">“ 扫一扫与我们亲密互动 ”</p>
					</div>
				</div>
			</div><!--right-->
			</div>
			<div class="nav-box">
			  <div class="nav">
				  <a href="/user/chatindex">网站首页</a>
				  <a href="/user/firstUserList">条件搜索</a>
				  <a href="/user/userInformation">快速匹配</a>
				  <a href="/sotry/index">婚恋故事</a>
				  <a href="/userPersonal">个人中心</a>
			  </div>
		    </div>
		 <div class="copy">
			<p>COPYRIGHT 2015 成都睿峰  版权所有 赣ICP备15002274号-1</p>
			<p>地址：成都市天府软件园B5  电话：123-456-789  技术支持：<a target="_blank" href="http://www.0791jr.com/">睿峰科技</a></p>
		</div>
			<!--在线客服-->
			<div class="chat_jsp">
				<iframe id="divFloatToolsView" src="/userFriend/UpChat"></iframe>
				<div id="floatTools" class="rides-cs" style="height:247px; ">
					<div class="floatL">
						<a id="aFloatTools_Show" class="btnOpen" title="打开聊天系统" style="top:20px;display:block" href="javascript:void(0);">展开</a>
						<a id="aFloatTools_Hide" class="btnCtn" title="关闭聊天系统" style="top:20px;display:none" href="javascript:void(0);">收缩</a>
					</div>
				</div>
			</div>
</div>


</body>
</html>
