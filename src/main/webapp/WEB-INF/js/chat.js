function Userlist(userid){
    document.getElementById("Userlist").innerHTML = "";
    $.ajax({
        url: "/userFriend/FriendList",
        type: "post",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: "userId="+userid,
        success:function(userList){
            if (userList!= null){
                $.each(userList,function(index,obj){
                    var li = obj['state']==1?"<li  alt='点击这里开始聊天' title='点击这里开始聊天'  onclick='chat("+obj['userid']+")'>":"<li  alt='点击这里开始聊天' title='点击这里开始聊天'>";
                    var onln = obj['state']==1?"<font style='color:green'>"+obj['username']+"</font>":"<font style='color:dimgray'>"+obj['username']+"</font>";
                    var html = li+
                        "<div class=\"imgss\">"+
                        "<img border=\"0\" src='"+obj['imageurl']+"\' />"+
                        "</div>"+
                        "<div class=\"spans\">"+
                        onln+
                        "</div>"+
                        "</li>";
                    document.getElementById("Userlist").innerHTML += html;
                });
            }else{
                var html = "<li>"+
                    "<div class='imgss'>"+
                    "<img border=\"0\" src='/images/chatss.jpg' />"+
                    "</div>"+
                    "<div class='spans'>你没有好友，快去添加好友吧</div>"+
                    "</li>";
                document.getElementById("Userlist").innerHTML = html;
            }
        }
    })
};
function UserChat(userid){
    document.getElementById("UserChat").innerHTML = "";
    $.ajax({
        url: "/userFriend/FriendChat",
        type: "post",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: "userId="+userid,
        success:function(userList){
            if (userList!= null){
                $.each(userList,function(index,obj){
                    var html = "<li >"+
                        "<div class='imgss'>"+
                        "<img border=\"0\" src='"+obj['imageurl']+"\'/>"+
                        "</div>"+
                        "<div class=\"spans\">"+
                        "<a>"+obj['username']+"</a>发来好友申请是否同意"+
                        "</div>"+
                        "<div class=\"buttons\">"+
                        "<button class=\"button1\" onclick=\"addfriend("+obj['userid']+","+userid+","+1+")\"></button>"+
                        "<button class=\"button2\" onclick=\"addfriend("+obj['userid']+","+userid+","+2+")\"></button>"+
                        "</div>"+
                        "</li>";
                    document.getElementById("UserChat").innerHTML += html;
                });
            }else{
                var html = "<li >"+
                    "<div class='imgss'>"+
                    "<img border=\"0\" src='/images/chatss.jpg'/>"+
                    "</div>"+
                    "<div class=\"spans\">"+
                    "<p>没有最新消息</p>"+
                    "</div>"+
                    "</li>";
                document.getElementById("UserChat").innerHTML = html;
            }
        }
    })
};
function addfriend(friendid,userid,state){
    var param = friendid+","+userid+","+state;
    $.ajax({
        url: "/userFriend/FriendAnd",
        type: "post",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: "s="+param,
        success:function(state) {
            switch (state) {
                case "1":
                    alert("添加好友成功");
                    break;
                case "2":
                    alert("添加好友失败");
                    break;
                case "3":
                    alert("成功拒绝好友申请");
                    break;
                case "4":
                    alert("拒绝好友申请失败");
                    break;
            }
            UserChat(userid);
            Userlist(userid);
        }
    })
};

function chatmessagelists(sendid,userid,start){
    document.getElementById("xiaoxi").innerHTML = "";
    var param = sendid+","+userid+","+start;
    $.ajax({
        url: "/message/MessageList",
        type: "post",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: "userid="+param,
        success:function(userList){
            $.each(userList,function(index,obj){
                var pic = obj['imageurl'];
                var isSef = obj['msgSendId']==sendid+""?"am-right1":"am-left1";
                var spage = obj['page']+1;
                var xpage = obj['page']-1;
                var html = "<li class='am-comment1'>"+
                    "<div class='am-date1'>"+obj['msgSendTime']+"</div>"+
                    "<div class='am-comment-main1'>"+
                    "<img class='am-comment-avatar1  "+isSef+"' src=\'"+pic+"'>"+
                    "<div class='am-name1 "+isSef+"'>"+obj['username']+"</div>"+"<br>"+
                    "<div class='am-comment-meta1  "+isSef+"'>"+obj['msgContext']+"</div>"+
                    "</div>"+
                    "</li>"+
                    "<li class='am-kong1'></li><br/>";
                document.getElementById("xiaoxi").innerHTML += html;
                document.getElementById("pages").innerHTML = "<button   onclick='chatmessagelists("+obj['msgSendId']+","+obj['msgReciveId']+","+spage+")'>上一页</button>"+
                    "<button   onclick='chatmessagelists("+obj['msgSendId']+","+obj['msgReciveId']+","+xpage+")'>下一页</button>";
            });
        }
    })
    document.getElementById("jilu").style.display = 'block';
};
function showFriend(param) {
    $.ajax({
        url: "/userFriend/FriendHave",
        type: "post",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: "userid=" + param,
        success: function (state) {
            var html;
            switch (state){
                case 1:
                    html = "<a href=\"#\"><img  class=\"m-icon-add\" src='../images/button/load.png'  alt=\"你正在添加他为好友，等待对方接受\" title=\"添加他为好友，等待对方接受\" /></a>";
                    break;
                case 2:
                    html = "<a href=\"#\"><img  class=\"m-icon-add\" src='../images/button/micon.png'  alt=\"你们已是好友啦去好友列表和她（他）开始聊天\" title=\"你们已是好友啦去好友列表和她（他）开始聊天\" /></a>";
                    break;
                case 3:
                    html = "<a href=\"#\"><img  class=\"m-icon-add\" src='../images/button/woshou.png'  alt=\"对方正在添加你为好友，快去最新消息里查看吧！\" title=\"对方正在添加你为好友，快去最新消息里查看吧！\" /></a>";
                    break;
                case 4:
                    html = "<a href=\"#\" onclick=\"addfriends()\"><img  class=\"m-icon-add\" src='../images/button/addfriend.jpg'  alt=\"点击添加好友\" title=\"点击添加好友\"/></a>";
                    break;
                case 5:
                    html = "<a></a>";
                    break;
                default:
                    break;
            }
            document.getElementById("m-icon").innerHTML = html;
        }
    })
};
function ChatPopu(param){
    $.ajax({
        url: "/visit/addGaun",
        type: "post",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: "userid=" + param,
        success: function (state) {
            var html;
            switch (state){
                case 2:
                    html = "<a href=\"#\"><img  class=\"m-icon-add\" src='../images/button/nos.png'  alt=\"不能再点了！\" title=\"不能再点了！\" /></a>";
                    break;
                case 1:
                    html = "<a href=\"#\" onclick=\"addPopu()\"><img  class=\"m-icon-add\" src='../images/button/oks.png'  alt=\"点击加关注度\" title=\"点击加关注度\"/></a>";
                    break;
                case 5:
                    html = "<a></a>";
                    break;
                default:
                    break;
            }
            document.getElementById("m-guan").innerHTML = html;
        }
    })
}

function addPopu(){
    $.ajax({
        url: "/visit/addGaunj",
        type: "post",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: "userid="+param,
        success:function(state){
            if (state == 2){
                ChatPopu(param);
            }
        }
    })
};
// function chatopen() {
//     layer.open({
//         type: 2,
//         title: '缘来是你聊天系统',
//         closeBtn: 1, //不显示关闭按钮
//         shade: [0],
//         area: ['1200px', '600px'],
//         maxmin: true,
//         offset: 'rb', //右下角弹出
//         time: 0, //2秒后自动关闭
//         anim: 2,
//         content: ['/userFriend/UpChat', 'no'], //iframe的url，no代表不显示滚动条
//         display: 0,
//     });
// }

function chat(userid){
    $.ajax({
        url: "/message/ChatIndex",
        type: "post",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: "userid="+userid,
        success:function(userList){
            var name = "<p>"+userList.username+"</p>";
            var id = "<input type='hidden' id='userid' value='"+userList.userid+"'>";
            var somes = "<input type='hidden' id='image' value='"+userList.imageurl+"'>";
            var image =  "<img src='"+userList.imageurl+"'>";
            document.getElementById("image").innerHTML = id;
            document.getElementById("tou").innerHTML = image;
            document.getElementById("mess").innerHTML = name;
            document.getElementById("somes").innerHTML = somes;
        }
    })
    document.getElementById("kuangj").style.display = 'block';
};
function addfriends() {
    $.ajax({
        url: "/userFriend/AddFriend",
        type: "post",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: "userid="+param,
        success:function(state){
            if (state == 2){
                alert("申请成功！请等待对方接受吧！");
                showFriend(param);
            }
        }
    })
};
function showTong(message) {
    var sendMessage = message.msgContext;//消息
    var userid = message.msgReciveId;
    var msgSendId = message.msgSendId;
    UserChat(userid);
    Userlist(userid);
    showFriend(userid+","+msgSendId);
    alert(sendMessage);
};