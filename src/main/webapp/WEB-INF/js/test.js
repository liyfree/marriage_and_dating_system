function checkPassword(ps) {
    // 获取密码长度
    var ps_length = ps.length;

    if (ps_length == 0) {
        layer.msg("密码不能为空");
        return false;
    } else {// 密码不为空验证密码位数
        if (ps_length < 6 || ps_length > 20) {// 密码长度验证
            layer.msg("密码长度不能小于6或大于20");
            return false;
        } else {//密码长度正确验证是否否和密码规则
            var reg = /^([a-zA-z_]{1})([\w]*)$/g;// 密码验证规则：由字母和数字，下划线,点号组成.且开头的只能是下划线和字母
            var psMatch = reg.test(ps); // 验证密码是否符合规则，匹配完正则表达式会记录上一次匹配结束的位置，所以需要两个正则分别匹配，或者reg.lastIndex=0;

            if (!psMatch) {// 如果密码不符合规则
                layer.msg("密码只能为字母、数字、下划线、点号，且只能以字母、下划线开头！");
                return false;
            } else { //执行这里说明密码符合规则
                return true;
            }
        }
    }
}